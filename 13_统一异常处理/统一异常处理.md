# 统一异常处理

## 1. 前言

在给前端返回返回调用失败的时候，应该明确失败的原因，不能笼统的说明，以免造成用户体验不佳、造成用户流失。



## 2. 前后端交互的数据格式

前后端交互返回的数据类型：JSON

前后端交互返回的数据格式：

```json
{
    "flag": true,
    "code": "200",
    "message": "查询成功",
    "data":{}
}
```

- flag:  响应是否成功
- code: 响应状态码
- message: 响应消息
- data: 响应的数据



## 3. 后端返回数据POJO

接口：

```java
public interface IResult {

    boolean isFlag();

    String getCode();

    String getMessage();
    
}
```



Result:

```java
public class Result<Object> implements  IResult {

    private boolean flag;
    private String code;
    private String message;
    private java.lang.Object data;


    public Result(boolean flag, String code, String message, java.lang.Object data) {
        this.flag = flag;
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public Result(boolean flag, String code, String message) {
        this.flag = flag;
        this.code = code;
        this.message = message;
    }

    //构造方法，允许接收接口的实现类
    public Result(IResult iResult){
        this.flag = iResult.isFlag();
        this.code = iResult.getCode();
        this.message = iResult.getMessage();
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public java.lang.Object getData() {
        return data;
    }

    public void setData(java.lang.Object data) {
        this.data = data;
    }
}

```



异常信息的枚举：

```java
public enum EnumBrandCode implements IResult {

    BRAND_EXISTS(false,"20000","商品已经存在")
     ;

    private boolean flag;
    private String code;
    private String message;

    EnumBrandCode(boolean flag, String code, String message) {
        this.flag = flag;
        this.code = code;
        this.message = message;
    }

    @Override
    public boolean isFlag() {
        return false;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
```



## 4. 自定义异常

```java
public class MyException extends RuntimeException{

    private IResult iResult;

    public MyException(IResult iResult) {
        this.iResult = iResult;
    }

    public IResult getiResult() {
        return iResult;
    }
}
```



## 5. 自定义异常处理类

```java
@ControllerAdvice  //标识为一个异常处理类
public class BaseExceptionHandler {

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Result myException(Exception e){
        if(e instanceof MyException){  //判断异常e是否是自定义异常的实现类
            MyException myException = (MyException) e;  //如果是就强转
            return new Result(myException.getiResult());  //获取自定义异常类中的属性值
        }

        return new Result(false,"30000","系统繁忙");
    }

}
```



