# SSM整合

### 1 需求描述

本案例基于maven构建 SSM（Spring+SpringMVC+Mybatis）工程，通过maven坐标进行依赖管理。最终实现根据 id 查询商品信息的功能。

### 2 构建maven工程

### 2.1 数据库环境搭建 

#### 2.1.1 创建数据库ssmtest

​		![](E:\01_notes\04_SSM框架\05_SSM整合\img\1数据库建立.png)

####   2.2.3 创建商品表item

```sql
-- 创建数据库表
CREATE TABLE `item` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) default NULL,
  `price` float default NULL,
  `createtime` datetime default NULL,
  `detail` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

-- 插入一条数据
INSERT INTO item VALUES(1,"电视机",4500,"2019-01-10 11:30:18","电视机");
```

### 2.2 maven项目构建 

- ①创建maven web项目

- ②配置pom.xml文件

- ③实现spring+mybatis整合
  - 创建POJO类
  - 持久层DAO接口编写
  - Mapper映射文件编写
  - 业务层Service编写
  - spring配置文件applicationContext-dao.xml编写
  - spring配置文件applicationContext-service.xml编写

- ④加入springmvc相关配置
  - 表现层Controller编写
  - springmvc.xml文件编写
  - jsp页面编写

#### 2.2.1 创建maven web项目

- 1.补全项目的目录结构(webapp和WEB-INF)

- 2.在pom.xml文件中设置打包方式为war包

  ```xml
  <packaging>war</packaging>
  ```

#### 2.2.2 配置pom.xml文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.example</groupId>
    <artifactId>ssm</artifactId>
    <version>1.0-SNAPSHOT</version>
    <packaging>war</packaging>

    <!--properties定义了一些属性，编译的版本、spring、springmvc、mybatis的版本-->
    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
        <spring.version>5.0.5.RELEASE</spring.version>
        <springmvc.version>5.0.5.RELEASE</springmvc.version>
        <mybatis.version>3.4.5</mybatis.version>
    </properties>

    <!--锁定jar版本，一般是锁定容易发生冲突的jar包，不容易产生冲突的就不用锁定，下面直接引用就好 -->
    <dependencyManagement>
        <dependencies>
            <!-- Mybatis -->
            <dependency>
                <groupId>org.mybatis</groupId>
                <artifactId>mybatis</artifactId>
                <version>${mybatis.version}</version>
            </dependency>
            <!-- springMVC -->
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-webmvc</artifactId>
                <version>${springmvc.version}</version>
            </dependency>
            <!-- spring -->
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-context</artifactId>
                <version>${spring.version}</version>
            </dependency>
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-core</artifactId>
                <version>${spring.version}</version>
            </dependency>
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-aop</artifactId>
                <version>${spring.version}</version>
            </dependency>
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-web</artifactId>
                <version>${spring.version}</version>
            </dependency>
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-expression</artifactId>
                <version>${spring.version}</version>
            </dependency>
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-beans</artifactId>
                <version>${spring.version}</version>
            </dependency>
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-aspects</artifactId>
                <version>${spring.version}</version>
            </dependency>
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-context-support</artifactId>
                <version>${spring.version}</version>
            </dependency>
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-test</artifactId>
                <version>${spring.version}</version>
            </dependency>
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-jdbc</artifactId>
                <version>${spring.version}</version>
            </dependency>
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-tx</artifactId>
                <version>${spring.version}</version>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <dependencies>
        <!-- Mybatis和mybatis与spring的整合 -->
        <dependency>
            <groupId>org.mybatis</groupId>
            <artifactId>mybatis</artifactId>
        </dependency>
        <dependency>
            <groupId>org.mybatis</groupId>
            <artifactId>mybatis-spring</artifactId>
            <version>1.3.1</version>
        </dependency>
        <!-- MySql驱动 -->
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>5.1.32</version>
        </dependency>
        <!-- druid数据库连接池 -->
        <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>druid</artifactId>
            <version>1.0.9</version>
        </dependency>
        <!-- springMVC核心-->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-webmvc</artifactId>
        </dependency>
        <!-- spring相关 -->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-core</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-aop</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-web</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-expression</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-beans</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-aspects</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context-support</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-test</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-jdbc</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-tx</artifactId>
        </dependency>
        <!-- junit测试 -->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.12</version>
        </dependency>
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>servlet-api</artifactId>
            <version>2.5</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>jsp-api</artifactId>
            <version>2.0</version>
            <scope>provided</scope>
        </dependency>
        <!-- jstl -->
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>jstl</artifactId>
            <version>1.2</version>
        </dependency>
    </dependencies>

    <!--maven编译的插件-->
    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.1</version>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                    <encoding>UTF-8</encoding>
                </configuration>
            </plugin>
        </plugins>
    </build>
</project>
```

#### 2.2.3 创建POJO类

```java
package com.example.ssm.pojo;

import java.util.Date;

/**
 * 订单实体
 */
public class Item {
    private int id;
    private String name;
    private float price;
    private Date createtime;
    private String detail;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", createtime=" + createtime +
                ", detail='" + detail + '\'' +
                '}';
    }
}
```

#### 2.2.4 持久层DAO接口编写

只需要定义接口，不需要编写其实现类，我们使用的是接口动态代理的方式，在程序运行期间由框架创建其代理对象

```java
package com.example.ssm.dao;

import com.example.ssm.pojo.Item;


public interface ItemMapper {
    /**
     * 根据商品id查询商品详情
     * @param id
     * @return
     */
    public Item findById(int id);
}
```

#### 2.2.5 Mapper映射文件

ItemMapper.xml编写

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="com.example.ssm.dao.ItemMapper">
    <!--根据商品id查询商品信息-->
    <select id="findById" parameterType="int" resultType="com.example.ssm.pojo.Item">
        select * from item where id = #{id};
    </select>
</mapper>
```

#### 2.2.6 业务层Service

```java
package com.example.ssm.service;

import com.example.ssm.pojo.Item;

public interface ItemService {
    /**
     * 根据id查询商品信息
     * @param id
     * @return
     */
    public Item findById(int id);
}

```

```java
package com.example.ssm.service.impl;

import com.example.ssm.service.ItemService;
import com.example.ssm.dao.ItemMapper;
import com.example.ssm.pojo.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ItemServiceImpl implements ItemService {

    @Autowired
    private ItemMapper itemMapper;

    @Override
    public Item findById(int id) {
        return itemMapper.findById(id);
    }
}

```

#### 2.2.7 spring配置文件applicationContext-dao.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">

    <!--持久层需要连接数据库，所以需要配置数据源相关的信息-->
    <!--配置数据源信息,使用druid连接池-->
    <bean id="dataSource" class="com.alibaba.druid.pool.DruidDataSource">
        <property name="driverClassName" value="com.mysql.jdbc.Driver"/>
        <property name="url" value="jdbc:mysql://localhost:3306/ssmtest"/>
        <property name="username" value="root"/>
        <property name="password" value="root"/>
    </bean>

    <!--配置spring整合mybatis框架的SQLSessionFactoryBean-->
    <bean id="sqlSessionFactory" class="org.mybatis.spring.SqlSessionFactoryBean">
        <!--通过属性注入，注入数据源-->
        <property name="dataSource" ref="dataSource"/>
        <!--mabatis扫描pojo包，为实体类创建别名-->
        <property name="typeAliasesPackage" value="com.example.ssm.pojo"/>
    </bean>

    <!--mapper扫描器，用于产生代理对象-->
    <bean class="org.mybatis.spring.mapper.MapperScannerConfigurer">
        <property name="basePackage" value="com.example.ssm.dao"/>
    </bean>
</beans>
```

#### 2.2.8 spring配置文件applicationContext-service.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context" xmlns:tx="http://www.springframework.org/schema/tx"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
                           http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd
                           http://www.springframework.org/schema/tx http://www.springframework.org/schema/tx/spring-tx.xsd">

    <!--service层都是使用的注解的方式，所以我们需要去扫描service类-->
    <!--配置扫描器，扫描Service-->
    <context:component-scan base-package="com.example.ssm.service"/>

    <!--service层都是使用的注解的方式，所以我们需要去扫描service类-->
    <!--事务管理器-->
    <bean id="transactionManager" class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
        <!--注入数据源  此处报红是因为我们对项目进行了拆分，idea检测报红-->
        <property name="dataSource" ref="dataSource"/>
    </bean>

    <!--配置事务注解驱动  关联事务管理器-->
    <tx:annotation-driven transaction-manager="transactionManager"/>
</beans>
```

#### 2.2.9 加入springmvc相关配置

##### 2.2.9.1表现层Controller

```java
package com.example.ssm.controller;

import com.example.ssm.pojo.Item;
import com.example.ssm.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/item")
public class ItemController {

    @Autowired
    private ItemService itemService;

    @RequestMapping("/showItem/{id}")
    public String findById(@PathVariable("id") int id, Model model){
        Item item = itemService.findById(id);
        model.addAttribute("item",item);
        return "item";
    }
}
```

##### 2.2.9.2 springmvc.xml文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd 
                           http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd">

    <!--配置扫描器，扫描controller-->
    <context:component-scan base-package="com.example.ssm.controller"/>

    <!--配置内部资源解析器-->
    <bean id="viewResolver" class="org.springframework.web.servlet.view.InternalResourceViewResolver">
        <property name="prefix" value="/WEB-INF/jsp"></property>
        <property name="suffix" value=".jsp"></property>
    </bean>
    
</beans>
```

##### 2.2.9.3 jsp页面

```jsp
<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
    ${item}  <%--使用el表达式取值  jsp页面的el表达式默认是关闭的，需要开启 isELIgnored="false"--%>
</body>
</html>
```

##### 2.2.9.4 配置web.xml文件

```xml
<!DOCTYPE web-app PUBLIC
 "-//Sun Microsystems, Inc.//DTD Web Application 2.3//EN" "http://java.sun.com/dtd/web-app_2_3.dtd" >

<web-app>

  <!--指定Spring配置文件位置-->
  <!--
      classpath*表示我们可以加载jar包里面的配置文件，我们现在配置文件都在resources下，加不加*都可以
      applicationContext*:通配符的写法，表示applicationContext开头的
  -->
  <context-param>
      <param-name>contextConfigLocation</param-name>
      <param-value>classpath*:applicationContext*.xml</param-value>
  </context-param>

  <!--配置Spring框架启动时使用的监听器-->
  <listener>
      <listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>
  </listener>

  <!--配置SpringMVC的前端控制器-->
  <servlet>
      <servlet-name>dispatcherServlet</servlet-name>
      <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
      <!--指定springmvc的配置文件-->
      <init-param>
          <param-name>contextConfigLocation</param-name>
          <param-value>classpath:springmvc.xml</param-value>
      </init-param>

      <load-on-startup>1</load-on-startup>
  </servlet>
  <servlet-mapping>
      <servlet-name>dispatcherServlet</servlet-name>
      <url-pattern>*.do</url-pattern>
  </servlet-mapping>
</web-app>
```

如果在xml文件中不配置加载springmvc的配置文件，它会默认到WEB-INF下去找dispatcherServlet-servlet.xml这个配置文件。其路径如下：

```xml
/WEB-INF/dispatcherServlet-servlet.xml
```



## 3 配置文件总结

### 3.1 redis相关配置

spring-redis.xml相关配置(使用jedis客户端操作redis)如下:

```xml
<!--Jedis连接池的相关配置-->
<bean id="jedisPoolConfig" class="redis.clients.jedis.JedisPoolConfig">
    <property name="maxTotal">
        <value>200</value>
    </property>
    <property name="maxIdle">
        <value>50</value>
    </property>
    <property name="testOnBorrow" value="true"/>
    <property name="testOnReturn" value="true"/>
</bean>
<bean id="jedisPool" class="redis.clients.jedis.JedisPool">
    <constructor-arg name="poolConfig" ref="jedisPoolConfig" />
    <constructor-arg name="host" value="127.0.0.1" />
    <constructor-arg name="port" value="6379" type="int" />
    <constructor-arg name="timeout" value="30000" type="int" />
</bean>
```



```xml
 <dependency>
 	<groupId>redis.clients</groupId>
 	<artifactId>jedis</artifactId>
 </dependency>
```

### 3.2 事务相关配置

spring框架事务相关配置，spring-tx.xml

注解形式的事务配置：

```xml
<!-- 事务管理器  -->
<bean id="transactionManager" class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
    <!--平台事务关系器控制事务的时候是通过Connection，Connection又源于数据库连接池，所以这里也需要指明参数数据库连接池-->
    <property name="dataSource" ref="dataSource"/>
</bean>

<!--
      开启事务控制的注解支持
      注意：此处必须加入proxy-target-class="true"，
           需要进行事务控制，会由Spring框架产生代理对象，
           Dubbo需要将Service发布为服务，要求必须使用cglib创建代理对象。
-->
<tx:annotation-driven transaction-manager="transactionManager" proxy-target-class="true"/>

<!--事务的注解驱动 transaction-manager的值为平台事务管理器的id  只有配置了事务的注解驱动，类和方法上的@Transactional才能生效-->
<tx:annotation-driven transaction-manager="transactionManager"/>
```

非注解形式的事务配置：

```xml
<!-- 事务管理器  -->
<bean id="transactionManager" class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
    <!--平台事务关系器控制事务的时候是通过Connection，Connection又源于数据库连接池，所以这里也需要指明参数数据库连接池-->
    <property name="dataSource" ref="dataSource"/>
</bean>

<!--通知  事务增强配置-->
<tx:advice id="txAdvice" transaction-manager="transactionManager">
    <!--配置事务的属性，也就是事务的定义对象，这里配置了事务的属性或者说是事务的参数-->
    <tx:attributes> 
        <!--这里被增强的切点是方法 这里name="*"表示任意方法 后面还可以配置isolation隔离级别 propagation传播行为 超时时间、是否只读  如下-->
        <!-- <tx:method name="*" isolation="DEFAULT" propagation="REQUIRED"/> -->
        <!-- <tx:attributes> 这个标签里面可以有很多tx:method，可以对不同的方法进行设置隔离级别 传播行为 超时时间、是否只读等-->
        <!--这里的name值是方法名字，如果是*这是任意方法，如果是update*则表示所有以update开头的方法-->
        <!--isolation隔离级别 propagation传播行为 超时时间、是否只读如果不设置，都有默认值-->
        <tx:method name="*"/>
    </tx:attributes>
</tx:advice>

<!--事务的aop增强 只有配置了事务AOP织入，才能将通知和切点结合-->
<aop:config>
    <aop:pointcut id="myPointcut" expression="execution(* com.itheima.service.impl.*.*(..))"/>
    <!--学习aop的时候，使用的aop:aspect  但是这里不使用它，因为spring为事务的增强提供了aop:advisor  aop:advisor也叫作切面，一个通知的切面-->
    <!--通知要和切点结合起来才能增强，所以这里需要配置切点-->
    <aop:advisor advice-ref="txAdvice" pointcut-ref="myPointcut"></aop:advisor>
</aop:config>
```

### 3.3 Dubbo的相关配置

Dubbo在服务提供者端的配置：

```xml
<!-- 指定应用名称 -->
<dubbo:application name="health_service_provider"/>

<!--指定暴露服务的端口，如果不指定默认为20880-->
<dubbo:protocol name="dubbo" port="20880"/>

<!--指定服务注册中心地址-->
<dubbo:registry address="zookeeper://122.51.83.231:2181"/>

<!--批量扫描，发布服务-->
<!--
      扫描指定的包，如果类上面有dubbo的service注解，就会将服务发布到服务注册中心
-->
<dubbo:annotation package="com.example.service"/>
```

Dubbo在消费者端的配置：

```xml
<!-- 指定应用名称 -->
<dubbo:application name="health_mobile" />

<!--指定服务注册中心地址-->
<dubbo:registry address="zookeeper://122.51.83.231:2181"/>

<!--批量扫描-->
<!--
    扫描Controller,如果我们需要消费哪个服务，就通过reference注解，注入服务的代理对象
 -->
<dubbo:annotation package="com.itheima.controller" />

<!--
    超时全局设置 10分钟
    check=false 不检查服务提供方，开发阶段建议设置为false
    check=true 启动时检查服务提供方，如果服务提供方没有启动则报错
-->
<dubbo:consumer timeout="600000" check="false"/>
```



### 3.4 Spring整合mybatis的相关配置

Spring框架整合mybatis后，Dao层相关的配置，spring-dao.xml

```xml
<!--在spring的配置文件中加载外部的properties文件-->
<context:property-placeholder location="classpath:jdbc.properties" />

<!--数据源  Druid数据库连接池-->
<bean id="dataSource" class="com.alibaba.druid.pool.DruidDataSource" destroy-method="close">
    <property name="username" value="root" />
    <property name="password" value="he512616" />
    <property name="driverClassName" value="com.mysql.jdbc.Driver" />
    <property name="url" value="jdbc:mysql://122.51.83.231:3306/health" />
</bean>

<!--数据源  Druid数据库连接池 jdbc.properties抽取配置-->
<bean id="dataSource" class="com.alibaba.druid.pool.DruidDataSource" destroy-method="close">
    <property name="username" value="${jdbc.username}" />
    <property name="password" value="${jdbc.password}" />
    <property name="driverClassName" value="${jdbc.driver}" />
    <property name="url" value="${jdbc.url}" />
</bean>

<!--数据源  C3P0数据库连接池-->
<bean id="dataSource" class="com.mchange.v2.c3p0.ComboPooledDataSource">
    <property name="driverClass" value="com.mysql.jdbc.Driver"/>
    <property name="jdbcUrl" value="jdbc:mysql://localhost:3306/test"/>
    <property name="user" value="root"/>
    <property name="password" value="root"/>
</bean>

<!--数据源  C3P0数据库连接池  jdbc.properties抽取配置-->
<bean id="dataSource" class="com.mchange.v2.c3p0.ComboPooledDataSource">
    <property name="driverClass" value="${jdbc.driver}"/>
    <property name="jdbcUrl" value="${jdbc.url}"/>
    <property name="user" value="${jdbc.username}"/>
    <property name="password" value="${jdbc.password}"/>
</bean>

<!--spring和mybatis整合的工厂bean-->
<bean id="sqlSessionFactory" class="org.mybatis.spring.SqlSessionFactoryBean">
    <property name="dataSource" ref="dataSource" />
    <property name="configLocation" value="classpath:SqlMapConfig.xml" />
</bean>

<!--批量扫描接口生成代理对象-->
<bean class="org.mybatis.spring.mapper.MapperScannerConfigurer">
    <!--指定接口所在的包-->
    <property name="basePackage" value="com.itheima.dao" />
</bean>
```

```xml
<!--加载外部properties-->
<context:property-placeholder location="xx.properties"/>
<!--使用外部properties中的数据-->
<property name="" value="${key}"/>
```

### 3.5 mybatis的核心配置文件

mybatis核心配置文件中配置分页插件。SqlMapConfig.xml

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration PUBLIC "-//mybatis.org//DTD Config 3.0//EN" "http://mybatis.org/dtd/mybatis-3-config.dtd">

<configuration>
    
    <!--配置Mybatis的分页插件-->
    <plugins>
        <!-- com.github.pagehelper 为PageHelper类所在包名 -->
        <plugin interceptor="com.github.pagehelper.PageHelper">
            <!-- 设置方言  value为数据库类型 Oracle,Mysql,MariaDB,SQLite,Hsqldb,PostgreSQL六种数据库 不同的数据库分页不一样-->
            <property name="dialect" value="mysql"/>
        </plugin>
    </plugins>
    
</configuration>
```

### springmvc的相关配置

```xml
<!--注解的组件扫描-->
<context:component-scan base-package="com.example"></context:component-scan>

<!--mvc的注解驱动-->
<mvc:annotation-driven>
    <mvc:message-converters register-defaults="true">
        <bean class="com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter">
            <property name="supportedMediaTypes" value="application/json"/>
            <property name="features">
                <list>
                    <value>WriteMapNullValue</value>
                    <value>WriteDateUseDateFormat</value>
                </list>
            </property>
        </bean>
    </mvc:message-converters>
</mvc:annotation-driven>

<!--在spring的主配置文件中加载其它的spring配置文件-->
<import resource="spring-redis.xml"></import>

<!--在spring的配置文件中加载外部的properties文件-->
<context:property-placeholder location="classpath:redis.properties" />

<!--配置文件上传解析器 文件上传组件-->
<bean id="multipartResolver" class="org.springframework.web.multipart.commons.CommonsMultipartResolver">
    <!--上传文件总大小-->
    <property name="maxUploadSize" value="5242800"/>
    <!--上传单个文件的大小-->
    <property name="maxUploadSizePerFile" value="5242800"/>
    <!--上传文件的编码类型-->
    <property name="defaultEncoding" value="UTF-8"/>
</bean>




```

配置自定义类型转化器

```xml
<!--声明转换器-->
<bean id="conversionService" class="org.springframework.context.support.ConversionServiceFactoryBean">
    <!--在转换器服务工厂对象中配置自定的转换器-->
    <property name="converters">
        <list>
        	<bean class="com.example.converter.DateConverter"></bean>
        </list>
    </property>
</bean>

<!--在注解驱动里面引用转换器服务工厂，让转换器工厂去产生日期转换器-->
<mvc:annotation-driven conversion-service="conversionService"/>
```

```xml
<!--配置RequestMappingHandlerAdapter处理器适配器，并指定Json转换器-->
<bean class="org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter">
    <property name="messageConverters">
        <list>
            <bean class="org.springframework.http.converter.json.MappingJackson2HttpMessageConverter"/>
        </list>
    </property>
</bean>

<!--
	上面的配置比较麻烦，我们也可以使用方法或者类上加@ResponseBody，然后在springmvc配置文件中配置注解驱动达到相同效果
	 mvc注解驱动默认底层就会集成jackson进行对象或集合的json格式字符串的转换
-->
<mvc:annotation-driven/>
```

```xml
<!--mapping的值是找谁，找资源的地址？ location的值是哪个目录下的资源是对外开放的-->
<!--开放资源的访问-->
<mvc:resources mapping="/js/**"location="/js/"/>  
<!--如果还有img的文件也要放行，可以如下设置-->
<mvc:resources mapping="/img/**"location="/img/"/>  

<!--第一种方式是前端过滤器检测到是该路径下的资源就放行，第二种方法是前端控制器去找，如果找不到就让tomcat去找-->
<mvc:default-servlet-handler/>
```

配置拦截器

```xml
<!--配置拦截器-->
<mvc:interceptors>
    <!--interceptors里面可以配置很多个interceptor-->
    <mvc:interceptor>
        <!--对哪些资源执行拦截操作 有点类似urlpatten  /**表示对所有的目标方法都执行该操作-->
        <mvc:mapping path="/**"/>
        <!--配置哪些资源排除拦截操作-->
        <mvc:exclude-mapping path="/user/login"/>
        <!--告诉springmvc框架我们编写的拦截器是哪个-->
        <bean class="com.itheima.interceptor.MyInterceptor1"/>
    </mvc:interceptor>
</mvc:interceptors>
```

简单异常处理器的配置

```xml
<!--配置简单映射异常处理器-->
<bean class=“org.springframework.web.servlet.handler.SimpleMappingExceptionResolver”> 
    <!--配置默认错误视图 如果和下面配置的异常都不匹配，就走默认错误视图 后面的value是视图的名称 你错误视图的名字叫什么这里就是什么  
		因为配置了视图解析器，这里只需要配置视图名称即可-->
    <property name=“defaultErrorView” value=“error”/> 
    <!--里面可以配置多个异常类型和错误视图-->
    <property name=“exceptionMappings”>
        <map>	
            <!--异常类型  错误视图的名字-->
            <entry key="com.itheima.exception.MyException" value="error"/>   
            <!--异常类型  错误视图的名字-->
            <entry key="java.lang.ClassCastException" value="error"/>
        </map>
    </property>
</bean>
```

自定义异常处理器的配置

```xml
<!--因为异常的处理和视图跳转的配置已经在上面代码中进行了处理，我们这里只需要配置自定义异常处理的对象bean即可，即将这个bean放到spring容器中-->
<bean id="exceptionResolver"  class="com.itheima.exception.MyExceptionResolver"/>
```



### aop的配置

```xml
<aop:config>
    <!--引用myAspect的Bean为切面对象-->
    <aop:aspect ref="myAspect">
        <!--配置Target的method方法执行时要进行myAspect的before方法前置增强-->
        <aop:before method="before" pointcut="execution(public void com.example.aop.Target.method())"></aop:before>
    </aop:aspect>
</aop:config>


<!--组件扫描-->
<context:component-scan base-package="com.example.aop"/>

<!--aop的自动代理-->
<aop:aspectj-autoproxy></aop:aspectj-autoproxy>
```



### jdbc连接参数

jdbc.properties配置文件

```properties
jdbc.driver=com.mysql.jdbc.Driver
jdbc.url=jdbc:mysql://localhost:3306/test
jdbc.username=root
jdbc.password=root
```



### SpringSecurity的相关配置

```xml
<!--配置哪些资源匿名可以访问（不登录也可以访问）-->
<!--<security:http security="none" pattern="/pages/a.html"></security:http>
    <security:http security="none" pattern="/pages/b.html"></security:http>-->
<!--<security:http security="none" pattern="/pages/**"></security:http>-->
<security:http security="none" pattern="/login.html"></security:http>
<security:http security="none" pattern="/css/**"></security:http>
<security:http security="none" pattern="/img/**"></security:http>
<security:http security="none" pattern="/js/**"></security:http>
<security:http security="none" pattern="/plugins/**"></security:http>

<!--
    auto-config:自动配置，如果设置为true，表示自动应用一些默认配置，比如框架会提供一个默认的登录页面
    use-expressions:是否使用spring security提供的表达式来描述权限
-->
<security:http auto-config="true" use-expressions="true">
    
    <security:headers>
        <!--设置在页面可以通过iframe访问受保护的页面，默认为不允许访问-->
        <security:frame-options policy="SAMEORIGIN"></security:frame-options>
    </security:headers>
    
    
    <!--配置拦截规则，/** 表示拦截所有请求-->
    <!--
            pattern:描述拦截规则
            asscess:指定所需的访问角色或者访问权限
	-->
    <!--只要认证通过就可以访问-->
    <security:intercept-url pattern="/pages/**"  access="isAuthenticated()" />

    
    <!--如果我们要使用自己指定的页面作为登录页面，必须配置登录表单.页面提交的登录表单请求是由框架负责处理-->
    <!--
            login-page:指定登录页面访问URL
    -->
    <security:form-login
                         login-page="/login.html"
                         username-parameter="username"
                         password-parameter="password"
                         login-processing-url="/login.do"
                         default-target-url="/pages/main.html"
                         authentication-failure-url="/login.html"></security:form-login>

    <!--
          csrf：对应CsrfFilter过滤器
          disabled：是否启用CsrfFilter过滤器，如果使用自定义登录页面需要关闭此项，否则登录操作会被禁用（403）
        -->
    <security:csrf disabled="true"></security:csrf>

    <!--
          logout：退出登录
          logout-url：退出登录操作对应的请求路径
          logout-success-url：退出登录后的跳转页面
        -->
    <security:logout logout-url="/logout.do" logout-success-url="/login.html" invalidate-session="true"/>
</security:http>


<!--配置认证管理器-->
<security:authentication-manager>
    <!--配置认证提供者-->
    <security:authentication-provider user-service-ref="springSecurityUserService">
        <!--
            配置一个具体的用户，后期需要从数据库查询用户
            <security:user-service>
                <security:user name="admin" password="{noop}1234" authorities="ROLE_ADMIN"/>
            </security:user-service>
            -->
        <!--指定度密码进行加密的对象-->
        <security:password-encoder ref="passwordEncoder"></security:password-encoder>
    </security:authentication-provider>
</security:authentication-manager>


<!--配置密码加密对象-->
<bean id="passwordEncoder" class="org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder" />


<!--开启注解方式权限控制-->
<security:global-method-security pre-post-annotations="enabled" />
```

### web.xml配置

```xml
<!--解决tomcat8post请求乱码的问题-->
<!--配置全局过滤的filter-->
<filter>
    <filter-name>CharacterEncodingFilter</filter-name>
    <filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
    <!--指定编码-->
    <init-param>
        <param-name>encoding</param-name>
        <param-value>UTF-8</param-value>
    </init-param>
</filter>

<filter-mapping>
    <filter-name>CharacterEncodingFilter</filter-name>
    <!--对所有的资源都进行过滤-->
    <url-pattern>/*</url-pattern>
</filter-mapping>


<!--配置SpringMVC的前端控制器-->
<servlet>
    <servlet-name>DispatcherServlet</servlet-name>
    <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>  
    <!--Servlet的初始化参数-->
    <init-param>
        <param-name>contextConfigLocation</param-name>
        <param-value>classpath:spring-mvc.xml</param-value>
    </init-param>
    <!--配置1表示服务器启动就加载Servlet创建对象，不配置的话就第一次访问的时候创建对象-->
	<load-on-startup>1</load-on-startup>
</servlet>

<!--配置前端控制器的映射地址-->
<servlet-mapping>   
    <servlet-name>DispatcherServlet</servlet-name>
    <!--配置/表示所有请求都要去找前端过滤器、配置*.xxx表示资源的扩展名为xxx时才走Servlet  一般配置*.do-->
    <url-pattern>/</url-pattern>
</servlet-mapping>
```



### 定时任务框架quartz

```xml
<dependency>
    <groupId>org.quartz-scheduler</groupId>
    <artifactId>quartz</artifactId>
</dependency>
<dependency>
    <groupId>org.quartz-scheduler</groupId>
    <artifactId>quartz-jobs</artifactId>
</dependency>
```



### 3.6 日志相关的配置

日志文件log4j.properties

```properties
### direct log messages to stdout ###
log4j.appender.stdout=org.apache.log4j.ConsoleAppender
log4j.appender.stdout.Target=System.err
log4j.appender.stdout.layout=org.apache.log4j.PatternLayout
log4j.appender.stdout.layout.ConversionPattern=%d{ABSOLUTE} %5p %c{1}:%L - %m%n

### direct messages to file mylog.log ###
log4j.appender.file=org.apache.log4j.FileAppender
log4j.appender.file.File=c:\\mylog.log
log4j.appender.file.layout=org.apache.log4j.PatternLayout
log4j.appender.file.layout.ConversionPattern=%d{ABSOLUTE} %5p %c{1}:%L - %m%n

### set log levels - for more verbose logging change 'info' to 'debug' ###

log4j.rootLogger=debug, stdout
```



pom.xml中的相关配置

```xml
<!--maven的jdk编译版本-->
<properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <maven.compiler.source>1.8</maven.compiler.source>
    <maven.compiler.target>1.8</maven.compiler.target>
</properties>


<!--maven的插件启动-->
<build>
    <plugins>
        <plugin>
            <groupId>org.apache.tomcat.maven</groupId>
            <artifactId>tomcat7-maven-plugin</artifactId>
            <configuration>
                <!-- 指定端口 -->
                <port>80</port>
                <!-- 请求路径 -->
                <path>/</path>
            </configuration>
        </plugin>
    </plugins>
</build>
```

