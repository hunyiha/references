package com.itheima.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value="/user", method= RequestMethod.POST)
public class UserController {

    // 请求地址  http://localhost:8080/user/quick
    // <url-pattern>/aaa</url-pattern>
    @RequestMapping(value="/quick",method = RequestMethod.GET,params = {"username"})
    public String save(){
        System.out.println("Controller save running....");
        return "success";
    }

    @RequestMapping(value="/update",method = RequestMethod.GET,params = {"username"})
    public String update(){
        System.out.println("Controller save running....");
        return "success";
    }

}
