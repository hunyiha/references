# Spring框架

## 1. SpringIoC和DI

### 1.1Spring是什么

Spring是分层的 Java SE/EE应用 full-stack 轻量级开源框架

以 IoC（Inverse Of Control：反转控制）和AOP（Aspect Oriented Programming：面向切面编程）为核心思想

LoC的本质就是通过配置文件配置，将对象的创建权交给Spring(由Spring在运行阶段实例化、组装对象)

AOP其思想是在执行某些代码前执行另外的代码，使程序更灵活、扩展性更好，可以随便地添加、删除某些功能

Spring提供了展现层 SpringMVC和持久层 Spring JDBCTemplate以及业务层事务管理等众多的企业级应用技术，还能整合开源世界众多著名的第三方框架和类库，逐渐成为使用最多的Java EE 企业应用开源框架。

Spring同时也是一个“一站式”框架，即Spring在JavaEE的三层架构[表现层（Web层）、业务逻辑层（Service层）、数据访问层（DAO层）]中，每一层均提供了不同的解决技术。

- 表现层（Web层）：Spring MVC
- 业务逻辑层（Service层）：Spring的IoC
- 数据访问层（DAO层）：Spring的jdbcTemplate

### 1.2 Spring发展历程

Rod Johnson （ Spring 之父）

2017  年9 月份发布了 Spring 的最新版本 Spring5.0通用版（GA）

### 1.3 Spring的优势

1. 方便解耦，简化开发
2. AOP 编程的支持
3. 声明式事务的支持
4. 方便程序的测试

### 1.4 Spring的体系结构

![](E:\01_notes\04_SSM框架\01_spring\00_spring笔记\img\Spring的体系结构.png)



## 2. spring快速入门

### 2.1 Spring程序开发步骤 

①导入 Spring 开发的基本包坐标

②编写 Dao 接口和实现类

③创建 Spring 核心配置文件

④在 Spring 配置文件中配置 UserDaoImpl

⑤使用 Spring 的 API 获得 Bean 实例

### 2.2 Spring程序开发步骤详情

#### 2.2.1导入Spring开发的基本包坐标：spring-context

```xml
<properties>
	<spring.version>5.0.5.RELEASE</spring.version>
</properties>

<!--导入spring的context坐标，context依赖core、beans、expression、aop-->
<dependencies> 
    <dependency>  
        <groupId>org.springframework</groupId> 
        <artifactId>spring-context</artifactId> 
        <version>${spring.version}</version>
    </dependency>
</dependencies>
```

#### 2.2.2编写Dao接口和实现类

```java
public interface UserDao {
    public void save();
}
```

```java
public class UserDaoImpl implements UserDao {
    public void save() {
        System.out.println("保存用户");
    }
}
```

#### 2.2.3创建Spring核心配置文件

在类路径下（resources）创建applicationContext.xml(名字可以随意，但是我们约定俗称为applicationContext.xml)配置文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">
</beans>
```

#### 2.2.4在Spring配置文件中配置UserDaoImpl

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">
   
    <!--配置UserDaoImpl  id和类的全路径-->
    <bean id="userDao" class="com.example.spring.dao.impl.UserDaoImpl"></bean>
</beans>
```

#### 2.2.5使用Spring的API获得Bean实例

```java
@Test
public void test1(){
    //ApplicationContext是接口   ClassPathXmlApplicationContext是它的一个实现类  下面构造方法的参数配置文件的文件名的字符串
    ApplicationContext app = new  ClassPathXmlApplicationContext("applicationContext.xml");
    //使用getBean()方法，从容器中或者id为userDao的对象
    UserDao userDao = (UserDao) app.getBean("userDao");
    //对象调用方法，完成保存操作
    userDao.save();
 }
```



## 3.Spring配置文件

### 3.1 Bean标签基本配置 

用于配置对象,将对象的创建劝交由Spring 。

默认情况下它调用的是类中的无参构造函数，如果没有无参构造函数则不能创建成功。

基本属性：

​	id：Bean实例在Spring容器中的唯一标识（不能重复）

​	class：Bean的全限定名称

### 3.2 Bean标签范围配置 

scope:指对象的作用范围，取值如下： 

| 取值范围         | 说明                                                         |
| ---------------- | ------------------------------------------------------------ |
| singleton        | 默认值，单例的，单列模式                                     |
| prototype        | 多例的，原型模式                                             |
| request          | WEB项目中，Spring创建一个Bean 的对象，将对象存入到 request 域中 |
| session          | WEB项目中，Spring创建一个 Bean 的对象，将对象存入到 session 域中 |
| global   session | WEB项目中，应用在Portlet 环境，如果没有 Portlet 环境那么globalSession 相当于session |

1）当scope的取值为singleton时

​      Bean的实例化个数：1个

​      Bean的实例化时机：当Spring核心配置文件被加载时，实例化配置的Bean实例

​      Bean的生命周期：

​			对象创建：当应用加载，创建容器时，对象就被创建了

​			对象运行：只要容器在，对象一直活着

​			对象销毁：当应用卸载，销毁容器时，对象就被销毁了

2）当scope的取值为prototype时

​      Bean的实例化个数：多个

​      Bean的实例化时机：当调用getBean()方法时实例化Bean

对象创建：当使用对象时，创建新的对象实例

对象运行：只要对象在使用中，就一直活着

对象销毁：当对象长时间不用时，被 Java 的垃圾回收器回收了

### 3.3 Bean生命周期配置

init-method：指定类中的初始化方法名称

destroy-method：指定类中销毁方法名称

初始化方法和销毁方法的配置：

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">
    <!--配置UserDaoImpl、配置初始化方法和销毁方法-->
    <bean id="userDao" class="com.example.spring.dao.impl.UserDaoImpl" init-method="init" destroy-method="destory"></bean>
</beans>
```

如果只是在类中写了初始化方法和销毁方法，是不会被调用的，因为没有告诉spring，必须通过配置文件进行配置。

init和destory这两个方法的方法名可以是任意的，这里只是为了可读性强。

初始化和销毁方法是通过对象去调用，所以先执行无参构造，然后执行初始化方法。

配置了销毁方法(假如销毁方法为打印一句话)，但是却没有看见打印在控制台的的原因？

- 因为这句话还没有打印，程序就执行完了，所以看不见
- 如果想要看见，可以让程序手动关闭，程序意识到自己要关闭的时候，它就会去调用销毁方法。
- 上述的具体做法是，ClassPathXmlApplicationContext这个类中有一个close()方法，可以去手动关闭容器

### 3.4 Bean实例化三种方式

1） 使用无参构造方法实例化

​      它会根据默认无参构造方法来创建类对象，如果bean中没有默认无参构造函数，将会创建失败

```xml
<bean id="userDao" class="com.example.dao.impl.UserDaoImpl"/>
```

2） 工厂静态方法实例化

​      工厂的静态方法返回Bean实例

```java
public class StaticFactoryBean {
    public static UserDao createUserDao(){    
    	return new UserDaoImpl();
    }
}
```

```xml
<bean id="userDao" class="com.example.factory.StaticFactoryBean" factory-method="createUserDao" />
```

3） 工厂实例方法实例化

​      工厂的非静态方法返回Bean实例

```java
public class DynamicFactoryBean {  
	public UserDao createUserDao(){        
		return new UserDaoImpl(); 
	}
}
```

```xml
<bean id="factoryBean" class="com.example.factory.DynamicFactoryBean"/>
<bean id="userDao" factory-bean="factoryBean" factory-method="createUserDao"/>
```

### 3.5 Bean的依赖注入入门

①创建 UserService，UserService 内部在调用 UserDao的save() 方法

```java
public class UserServiceImpl implements UserService {
	@Override
	public void save() {
         ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");           	           
         UserDao userDao = (UserDao) applicationContext.getBean("userDao");	
         userDao.save();
 	}
 }
```

②将 UserServiceImpl 的创建权交给 Spring

```xml
<bean id="userService" class="com.example.service.impl.UserServiceImpl"/>
```

③从 Spring 容器中获得 UserService 进行操作

```java
ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
UserService userService = (UserService) applicationContext.getBean("userService");
userService.save();
```

### 3.6 Bean的依赖注入概念

①构造方法

​      创建有参构造

```java
public class UserServiceImpl implements UserService {
	
    //成员变量 userDao
    private UserDao userDao;

    //通过有参构造方法给成员变量赋值
    public UserServiceImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    //业务方法
    public void save() {
        userDao.save();
    }
}
```

​      配置Spring容器调用有参构造时进行注入

```xml
<bean id="userDao" class="com.example.dao.impl.UserDaoImpl"/>
<bean id="userService" class="com.example.service.impl.UserServiceImpl">   
    <!--
		1.name的值是构造方法的参数名
		2.对象的引用使用ref
		3.ref的值表示所需要注入的Bean的id
	-->
    <constructor-arg name="userDao" ref="userDao"></constructor-arg>
</bean>
```

②set方法

​      在UserServiceImpl中添加setUserDao方法

```java
public class UserServiceImpl implements UserService {
    
    //成员变量 userDao
    private UserDao userDao;
    
    //通过set方法给成员变量赋值  其中set后面的单词首字母小写为属性名。成员变量名不是属性名
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;  
    } 
    
    @Override    
    public void save() {      
   		userDao.save();
	}
}
```

​      配置Spring容器调用set方法进行注入

```xml
<bean id="userDao" class="com.example.dao.impl.UserDaoImpl"/>
<bean id="userService" class="com.example.service.impl.UserServiceImpl">
    <!--
		1.name的值是属性名，也就是set方法后面的字母，然后将首字母变为大写，而不是变量名。
		2.对象的引用使用ref
		3.ref的值表示所需要注入的Bean的id
    -->
	<property name="userDao" ref="userDao"/>
</bean>
```

不用从容器中获取Dao，而是在容器内部就将Dao注入到了Service中



set方法:P命名空间注入

​      P命名空间注入本质也是set方法注入，但比起上述的set方法注入更加方便，主要体现在配置文件中(将properties子标签给省略了)，如下：

​      首先，需要引入P命名空间：

```xml
xmlns:p="http://www.springframework.org/schema/p"
```

其次，需要修改注入方式

```xml
 <bean id="userService" class="com.example.spring.service.impl.UserServiceImpl" p:userDao-ref="userDao"></bean>
```

###    3.8 Bean的依赖注入的数据类型

上面的操作，都是注入的引用Bean，除了对象的引用可以注入，普通数据类型，集合等都可以在容器中进行注入。

注入数据的三种数据类型 

普通数据类型

引用数据类型

集合数据类型

其中引用数据类型，此处就不再赘述了，之前的操作都是对UserDao对象的引用进行注入的，下面将以set方法注入为例，演示普通数据类型和集合数据类型的注入。

**Bean的依赖注入的数据类型**

（1）普通数据类型的注入

```java
public class UserDaoImpl implements UserDao {
	private String company;
    private int age;
    
    public void setCompany(String company) {
        this.company = company;
    }
    public void setAge(int age) {
        this.age = age;
    }
    
    public void save() {
        System.out.println(company+"==="+age);
        System.out.println("UserDao save method running....");   
    }
}

```

```xml
<bean id="userDao" class="com.example.dao.impl.UserDaoImpl">
    <property name="company" value="公司"></property>
    <property name="age" value="15"></property>
</bean>
```

（2）集合数据类型（List<String>）的注入

```java
public class UserDaoImpl implements UserDao {
	private List<String> strList;
    
	public void setStrList(List<String> strList) {
		this.strList = strList;
	}
    
	public void save() {
        System.out.println(strList);
        System.out.println("UserDao save method running....");
	}
}
```

```xml
<bean id="userDao" class="com.example.dao.impl.UserDaoImpl">
    <property name="strList">
        <list>
            <!--普通值用value-->
            <value>aaa</value>
            <value>bbb</value>
            <value>ccc</value>
        </list>
    </property>
</bean>
```

（3）集合数据类型（List<User>）的注入

```java
public class UserDaoImpl implements UserDao {
	private List<User> userList;
    
	public void setUserList(List<User> userList) {
		this.userList = userList;  
 	}
    
    public void save() {
        System.out.println(userList);
        System.out.println("UserDao save method running....");
    }
}
```

```xml
<bean id="u1" class="com.example.domain.User"/>
<bean id="u2" class="com.example.domain.User"/>
<bean id="userDao" class="com.example.dao.impl.UserDaoImpl">
    <property name="userList">
        <list>
            <bean class="com.example.domain.User"/>
            <bean class="com.example.domain.User"/>
            <ref bean="u1"/>
            <ref bean="u2"/>       
        </list>
    </property>
</bean>
```

（4）集合数据类型（ Map<String,User> ）的注入

```java
public class UserDaoImpl implements UserDao {
    private Map<String,User> userMap;
    
    public void setUserMap(Map<String, User> userMap) {
    	this.userMap = userMap;
    }  
    
    public void save() {      
        System.out.println(userMap);
        System.out.println("UserDao save method running....");
    }
}
```

```xml
<bean id="u1" class="com.example.domain.User"/>
<bean id="u2" class="com.example.domain.User"/>
<bean id="userDao" class="com.example.dao.impl.UserDaoImpl">
    <property name="userMap">
        <map>            
            <entry key="user1" value-ref="u1"/>
            <entry key="user2" value-ref="u2"/>
        </map>
    </property>
</bean>
```

（5）集合数据类型（Properties）的注入

```java
public class UserDaoImpl implements UserDao {
    private Properties properties;
    
    public void setProperties(Properties properties) {
        this.properties = properties;
    }
    
	public void save() {
		System.out.println(properties);
		System.out.println("UserDao save method running....");
	}
}
```

```xml
<bean id="userDao" class="com.example.dao.impl.UserDaoImpl">
    <property name="properties">
        <props>
            <prop key="p1">aaa</prop>
            <prop key="p2">bbb</prop> 
            <prop key="p3">ccc</prop>
        </props>
    </property>
</bean>
```

### 3.9 引入其他配置文件（分模块开发）

实际开发中，Spring的配置内容非常多，这就导致Spring配置很繁杂且体积很大，不容易维护。所以，可以将部分配置拆解到其他配置文件中(例如：按照业务模块拆分、按照层进行拆分[dao、service等])，而在Spring主配置文件通过import标签进行加载

```xml
<import resource="applicationContext-xxx.xml"/>
```



## 4. spring相关API

### 4.1 ApplicationContext的继承体系

ApplicationContext：接口类型，代表应用上下文，可以通过其实例获得 Spring 容器中的 Bean 对象

### 4.2 ApplicationContext的实现类

1）ClassPathXmlApplicationContext 

​      它是从类的根路径下加载配置文件 推荐使用这种

2）FileSystemXmlApplicationContext 

​      它是从磁盘路径上加载配置文件，配置文件可以在磁盘的任意位置。

3）AnnotationConfigApplicationContext

​      当使用注解配置容器对象时，需要使用此类来创建 spring 容器。它用来读取注解。

### 4.3 getBean()方法使用

getBean()方法的源码

```java
public Object getBean(String name) throws BeansException {  
	assertBeanFactoryActive();   
	return getBeanFactory().getBean(name);
}

public <T> T getBean(Class<T> requiredType) throws BeansException {   			    	
    assertBeanFactoryActive();
	return getBeanFactory().getBean(requiredType);
}
```

其中，当参数的数据类型是字符串(bean实例的id)时，表示根据Bean的id从容器中获得Bean实例，返回是Object，需要强转。

当参数的数据类型是Class类型(字节码类型)时，表示根据类型从容器中匹配Bean实例，当容器中相同类型的Bean有多个时，则此方法会报错

**getBean()方法使用**

```java
//读取spring核心配置文件
ApplicationContext app = new  ClassPathXmlApplicationContext("applicationContext.xml");
//根据id标识获取全限定名，根据反射获取对象，然后将对象返回
UserService userService1 = (UserService) app.getBean("userService");
UserService userService2 = app.getBean(UserService.class);
```



## 5. Spring配置数据源

### 5.1 数据源（连接池）的作用 

数据源(连接池)是提高程序性能如出现的

数据源的使用过程：

1. 事先实例化数据源，初始化部分连接资源
2. 使用连接资源时从数据源中获取
3. 使用完毕后将连接资源归还给数据源


常见的数据源(连接池)：DBCP、C3P0、BoneCP、Druid等

**数据源的开发步骤**

​		①导入数据源的坐标和数据库驱动坐标(数据源一般是第三方提供的，所以要导坐标)

​		②创建数据源对象(没有设置基本连接参数的时候是不能够使用的)

​		③设置数据源的基本连接数据(驱动，地址，用户名，密码)

​		④使用数据源获取连接资源和归还连接资源

### 5.2 数据源的手动创建

①导入c3p0和druid的坐标

```xml
<!-- C3P0连接池 -->
<dependency>
    <groupId>c3p0</groupId>
    <artifactId>c3p0</artifactId>
    <version>0.9.1.2</version>
</dependency>

<!-- Druid连接池 -->
<dependency>
    <groupId>com.alibaba</groupId>
    <artifactId>druid</artifactId>
    <version>1.1.10</version>
</dependency>
```

①导入mysql数据库驱动坐标

```xml
<!-- mysql驱动 -->
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <version>5.1.39</version>
</dependency>
```

②创建C3P0连接池

```java
@Test
public void testC3P0() throws Exception {
    
	//创建数据源
	ComboPooledDataSource dataSource = new ComboPooledDataSource();
    
	//设置数据库连接参数
    dataSource.setDriverClass("com.mysql.jdbc.Driver");    	               	               
	dataSource.setJdbcUrl("jdbc:mysql://localhost:3306/test");
    dataSource.setUser("root");
    dataSource.setPassword("root");
    
	//获得连接对象
	Connection connection = dataSource.getConnection();
	System.out.println(connection);
}

```

②创建Druid连接池

```java
@Test
public void testDruid() throws Exception {
    
    //创建数据源  还能体现出DruidDataSource有无参构造
    DruidDataSource dataSource = new DruidDataSource();
    
    //设置数据库连接参数  能够体现该类还有set方法
    dataSource.setDriverClassName("com.mysql.jdbc.Driver"); 
    dataSource.setUrl("jdbc:mysql://localhost:3306/test");   
    dataSource.setUsername("root");
    dataSource.setPassword("root");
    
    //获得连接对象
    Connection connection = dataSource.getConnection();    
    System.out.println(connection);
}
```

③提取jdbc.properties配置文件

​	抽取配置文件的原因:

​		解耦合,代码中设置的数据库连接参数属于数据库的一些参数,目前写到代码中,会和代码耦合死了,如果需要修改(更换数据库等)就会修改源码,造成不便,处理上面的问题常见的处理方式就是抽取配置文件

```properties
jdbc.driver=com.mysql.jdbc.Driver
jdbc.url=jdbc:mysql://localhost:3306/test
jdbc.username=root
jdbc.password=root
```

④读取jdbc.properties配置文件创建连接池

```java
@Test
public void testC3P0ByProperties() throws Exception {
    
    //专门用来加载类路径下的jdbc.properties
    ResourceBundle rb = ResourceBundle.getBundle("jdbc");
    
    ComboPooledDataSource dataSource = new ComboPooledDataSource(); 
    
    //设置数据库连接参数
    dataSource.setDriverClass(rb.getString("jdbc.driver"));   
    dataSource.setJdbcUrl(rb.getString("jdbc.url")); 
    dataSource.setUser(rb.getString("jdbc.username")); 
    dataSource.setPassword(rb.getString("jdbc.password"));
    
    Connection connection = dataSource.getConnection();   
    System.out.println(connection);
}
```

关于ResourceBundle的一些知识:

  1. ResourceBundle这是一个抽象类,这个抽象类里面提供一个静态方法getBundle()可以获取ResourceBundle的一个实现类

  2. getBundle(basename )的参数basename基本名称,简称基名  getBundle()里面的地址是相对于类加载路径的地址,换句话说就是开发环境下resource下的路径

  3. 因为是专门读取properties文件的,所有文件名称不需要后缀名

  4. ResourceBundle是java.util包下的一个工具类,获取该工具类的方式是该类提供的getBundle()方法

  5. getString()方法中的参数应该和properties配置文件中的key相对应,可以任意改动

     

### 5.3 Spring配置数据源

可以将DataSource的创建权交由Spring容器去完成

DataSource有无参构造方法，而Spring默认就是通过无参构造方法实例化对象的

DataSource要想使用需要通过set方法设置数据库连接信息，而Spring可以通过set方法进行字符串注入

DataSource这个类要使用满足Spring创建对象,注入数据的基本要求:1.有无参构造 (创建Bean的基本要求)  2.有set方法(数据注入)

```xml
<bean id="dataSource" class="com.mchange.v2.c3p0.ComboPooledDataSource">
    <property name="driverClass" value="com.mysql.jdbc.Driver"/>
    <property name="jdbcUrl" value="jdbc:mysql://localhost:3306/test"/>
    <property name="user" value="root"/>
    <property name="password" value="root"/>
</bean>
```

测试从容器当中获取数据源

```java
ApplicationContext applicationContext = new  ClassPathXmlApplicationContext("applicationContext.xml");
DataSource dataSource = (DataSource) applicationContext.getBean("dataSource");
Connection conn = dataSource.getConnection();
System.out.println(conn);
```

### 5.4 抽取jdbc配置文件

使用spring创建数据源对象的时候,虽然代码和配置文件是解耦的(实际代码中并没有数据源的参数,是在xml配置文件中),但实际开发中一般是将spring的核心配置文件和数据库的配置文件分来(利于查找,修改,测试等),但是这样就会带来在xml文件中加载properties文件

applicationContext.xml加载jdbc.properties配置文件获得连接信息。

首先，需要引入context命名空间和约束路径：

命名空间：xmlns:context="http://www.springframework.org/schema/context"

约束路径：http://www.springframework.org/schema/context     

​                   http://www.springframework.org/schema/context/spring-context.xsd

```xml
<!--加载外部的properties配置文件-->
<context:property-placeholder location="classpath:jdbc.properties"/>

<bean id="dataSource" class="com.mchange.v2.c3p0.ComboPooledDataSource">
    <property name="driverClass" value="${jdbc.driver}"/>
    <property name="jdbcUrl" value="${jdbc.url}"/>
    <property name="user" value="${jdbc.username}"/>
    <property name="password" value="${jdbc.password}"/>
</bean>
```

### 5.5 知识要点 

Spring容器加载properties文件

```xml
<context:property-placeholder location="xx.properties"/>
<property name="" value="${key}"/>
```



## 6. Spring注解开发

### 6.1 Spring原始注解

Spring是**轻代码**而**重配置**的框架，配置比较繁重，影响开发效率，所以注解开发是一种趋势，注解代替xml配置文件可以简化配置，提高开发效率。 

Spring原始注解主要是替代<Bean>的配置

| 注解           | 说明                                                         |
| -------------- | ------------------------------------------------------------ |
| @Component     | 使用在类上用于实例化Bean       组件                          |
| @Controller    | 使用在web层类上用于实例化Bean    控制器   具有语义化功能     |
| @Service       | 使用在service层类上用于实例化Bean     服务  具有语义化功能   |
| @Repository    | 使用在dao层类上用于实例化Bean   贮藏室，仓库  具有语义化功能 |
| @Autowired     | 使用在字段上用于根据类型依赖注入                             |
| @Qualifier     | 结合@Autowired一起使用用于根据名称进行依赖注入               |
| @Resource      | 相当于@Autowired+@Qualifier，按照名称进行注入                |
| @Value         | 注入普通属性                                                 |
| @Scope         | 标注Bean的作用范围                                           |
| @PostConstruct | 使用在方法上标注该方法是Bean的初始化方法                     |
| @PreDestroy    | 使用在方法上标注该方法是Bean的销毁方法                       |

注意：

使用注解进行开发时，需要在applicationContext.xml中配置组件扫描，作用是指定哪个包及其子包下的Bean需要进行扫描以便识别使用注解配置的类、字段和方法。

如果不加组件扫描会出现NoSuchBeanDefinitionException异常

```xml
<!--注解的组件扫描-->
<context:component-scan base-package="com.example"></context:component-scan>
```

使用@Compont或@Repository标识UserDaoImpl需要Spring进行实例化。

```java
@Repository("userDao")
public class UserDaoImpl implements UserDao {
    @Override
    public void save() {
    	System.out.println("save running... ...");
    }
}
```

使用@Compont或@Service标识UserServiceImpl需要Spring进行实例化

使用@Autowired或者@Autowired+@Qulifier或者@Resource进行userDao的注入

```java
@Service("userService")
public class UserServiceImpl implements UserService {
    /*@Autowired
    @Qualifier("userDao")*/
    @Resource(name="userDao")	
    private UserDao userDao;
    
    @Override
    public void save() {       
   		userDao.save();
    }
}
```

使用@Value进行字符串的注入

```java
@Repository("userDao")
public class UserDaoImpl implements UserDao {
    @Value("注入普通数据")
    private String str;
    
    @Value("${jdbc.driver}")
    private String driver;
    
    @Override
    public void save() {
        System.out.println(str);
        System.out.println(driver);
        System.out.println("save running... ...");
    }
}
```

使用@Scope标注Bean的范围

```java
//@Scope("prototype")
@Scope("singleton")
public class UserDaoImpl implements UserDao {
   //此处省略代码
}
```

使用@PostConstruct标注初始化方法，使用@PreDestroy标注销毁方法

```java
@PostConstruct
public void init(){
	System.out.println("初始化方法....");
}

@PreDestroy
public void destroy(){
	System.out.println("销毁方法.....");
}
```

### 6.2 Spring新注解

使用上面的注解还不能全部替代xml配置文件，还需要使用注解替代的配置如下：

非自定义的Bean的配置：<bean>

加载properties文件的配置：<context:property-placeholder>

组件扫描的配置：<context:component-scan>

引入其他文件：<import>

| 注解            | 说明                                                         |
| --------------- | ------------------------------------------------------------ |
| @Configuration  | 用于指定当前类是一个 Spring   配置类，当创建容器时会从该类上加载注解 |
| @ComponentScan  | 用于指定 Spring 在初始化容器时要扫描的包                                                                                                                                                                                                   作用和在 Spring  的 xml 配置文件中的   <context:component-scan   base-package="com.example"/>一样 |
| @Bean           | 使用在方法上，标注将该方法的返回值存储到 Spring 容器中       |
| @PropertySource | 用于加载.properties   文件中的配置                           |
| @Import         | 用于导入其他配置类                                           |

@Configuration、@ComponentScan、@Import

```java
@Configuration
@ComponentScan("com.example")
@Import({DataSourceConfiguration.class})
public class SpringConfiguration {
}
```

@PropertySource、@value

```java
@PropertySource("classpath:jdbc.properties")
public class DataSourceConfiguration {
    @Value("${jdbc.driver}")
    private String driver;
    @Value("${jdbc.url}")
    private String url;
    @Value("${jdbc.username}")
    private String username;
    @Value("${jdbc.password}")
    private String password;
```

@Bean

```java
@Bean(name="dataSource")
public DataSource getDataSource() throws PropertyVetoException { 
    ComboPooledDataSource dataSource = new ComboPooledDataSource(); 
    dataSource.setDriverClass(driver);
    dataSource.setJdbcUrl(url);
    dataSource.setUser(username);
    dataSource.setPassword(password);
    return dataSource;
} 
```

测试加载核心配置类创建Spring容器

```java
@Test
public void testAnnoConfiguration() throws Exception {
    
    //通过核心配置类加载
    ApplicationContext applicationContext = new  AnnotationConfigApplicationContext(SpringConfiguration.class);  
    
    UserService userService = (UserService)applicationContext.getBean("userService");
    userService.save();

    DataSource dataSource = (DataSource)applicationContext.getBean("dataSource");
    Connection connection = dataSource.getConnection(); 

    System.out.println(connection);
}
```

@Configuration、@PropertySource、@ComponentScan、@Import、@Bean、@Value总结

```java
@Configuration   //用于指定当前类是一个 Spring 配置类
@PropertySource("classpath:jdbc.properties")  //相当于<context:property-placeholder location="classpath:jdbc.properties"/>
@ComponentScan("com.example.spring")   //相当于<context:component-scan base-package="com.example.spring"></context:component-scan>
@Import(DataSourceConfig.class)  //相当于<import resource="applicationContext-user.xml"/>
public class SpringConfiguration {

    @Bean("dataSource") //spring会将当前方法的返回值以指定名称存储到Spring容器中
    public DataSource getDataSource() throws PropertyVetoException {
        //创建数据源
        ComboPooledDataSource dataSource = new ComboPooledDataSource();

        //设置数据库连接参数
        dataSource.setDriverClass("com.mysql.jdbc.Driver");
        dataSource.setJdbcUrl("jdbc:mysql://localhost:3306/test");
        dataSource.setUser("root");
        dataSource.setPassword("root");

        return dataSource;
    }
}
```



## 7. Spring整合Junit

### 7.1 原始Junit测试Spring的问题

在测试类中，每个测试方法都有以下两行代码：

```java
 ApplicationContext ac = new ClassPathXmlApplicationContext("bean.xml");
 IAccountService as = ac.getBean("accountService",IAccountService.class);
```

这两行代码的作用是获取容器，如果不写的话，直接会提示空指针异常。所以又不能轻易删掉。

### 7.2 上述问题解决思路

让SpringJunit负责创建Spring容器，但是需要将配置文件的名称告诉它

将需要进行测试Bean直接在测试类中进行注入

### 7.3 Spring集成Junit步骤

①导入spring集成Junit的坐标(spring集成junit的jar包没有在spring的基本包里面,需要额外导入)

②使用@Runwith注解替换原来的运行期(指定谁去测试)

③使用@ContextConfiguration指定配置文件或配置类

④使用@Autowired注入需要测试的对象

⑤创建测试方法进行测试

### 7.4 Spring集成Junit代码实现

①导入spring集成Junit的坐标

```xml
<!--此处需要注意的是，spring5 及以上版本要求 junit 的版本必须是 4.12 及以上-->

<!--spring集成junit的坐标-->
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-test</artifactId>
    <version>5.0.2.RELEASE</version>
</dependency>

<!--junit的坐标-->
<dependency>
    <groupId>junit</groupId>
    <artifactId>junit</artifactId>
    <version>4.12</version>
    <scope>test</scope>
</dependency>
```

②使用@Runwith注解替换原来的运行期(意思是没有集成的时候我们是直接使用junit，集成后我们去找spring，然后spring去找junit)

spring去找junit之前会完成配置文件的加载、容器的创建等等工作

```java
@RunWith(SpringJUnit4ClassRunner.class)
public class SpringJunitTest {

}
```

③使用@ContextConfiguration指定配置文件或配置类

```java
@RunWith(SpringJUnit4ClassRunner.class)
//加载spring核心配置文件
//@ContextConfiguration(value = {"classpath:applicationContext.xml"})
//加载spring核心配置类
@ContextConfiguration(classes = {SpringConfiguration.class})
public class SpringJunitTest {
}
```

④使用@Autowired注入需要测试的对象

```java
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SpringConfiguration.class})
public class SpringJunitTest {
    @Autowired
    private UserService userService;
}
```

⑤创建测试方法进行测试

```java
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SpringConfiguration.class})
public class SpringJunitTest {
    @Autowired
    private UserService userService;
    @Test
    public void testUserService(){
   		userService.save();
    }
}
```



## 8. Spring 的 AOP 简介

#### 8.1 什么是 AOP 

AOP 为 Aspect Oriented Programming 的缩写，意思为面向切面编程，是通过预编译方式和运行期动态代理实现程序功能的统一维护的一种技术。

AOP 是 OOP 的延续，是软件开发中的一个热点，也是Spring框架中的一个重要内容，是函数式编程的一种衍生范型。利用AOP可以对业务逻辑的各个部分进行隔离(松耦合)，从而使得业务逻辑各部分之间的耦合度降低，提高程序的可重用性，同时提高了开发的效率。

#### 8.2 AOP 的作用及其优势

作用：在程序运行期间，在不修改源码的情况下对方法进行功能增强

优势：减少重复代码，提高开发效率，并且便于维护

#### 8.3 AOP 的底层实现

实际上，AOP 的底层是通过 Spring 提供的的动态代理技术实现的。在运行期间，Spring通过动态代理技术动态的生成代理对象，代理对象方法执行时进行增强功能的介入，在去调用目标对象的方法，从而完成功能的增强。

#### 8.4 AOP 的动态代理技术

常用的动态代理技术

JDK 代理 : 基于接口的动态代理技术

cglib 代理：基于父类的动态代理技术(目标对象和代理对象之间就是子父类的关系，视频有误)

![](E:\01_notes\04_SSM框架\01_spring\00_spring笔记\img\动态代理.png)

#### 8.5 JDK 的动态代理

①目标类接口

 ```java
public interface TargetInterface {
    public void method();
}
 ```

②目标类

```java
public class Target implements TargetInterface {
    @Override
    public void method() {
        System.out.println("Target running....");
    }
}
```

③动态代理代码

```java
Target target = new Target(); //创建目标对象
//创建代理对象
TargetInterface proxy = (TargetInterface) Proxy.newProxyInstance(target.getClass()
.getClassLoader(),target.getClass().getInterfaces(),new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) 
            throws Throwable {
                System.out.println("前置增强代码...");
                Object invoke = method.invoke(target, args);
                System.out.println("后置增强代码...");
                return invoke;
            }
        }
);
```

④  调用代理对象的方法测试

```java
// 测试,当调用接口的任何方法时，代理对象的代码都无序修改
proxy.method();
```

![](E:\01_notes\04_SSM框架\01_spring\00_spring笔记\img\测试1.png)

#### 8.6 cglib 的动态代理

①目标类

  ```java
public class Target {
    public void method() {
        System.out.println("Target running....");
    }
}
  ```

②动态代理代码

```java
Target target = new Target(); //创建目标对象
Enhancer enhancer = new Enhancer();   //创建增强器
enhancer.setSuperclass(Target.class); //设置父类
enhancer.setCallback(new MethodInterceptor() { //设置回调
    @Override
    public Object intercept(Object o, Method method, Object[] objects, 
    MethodProxy methodProxy) throws Throwable {
        System.out.println("前置代码增强....");
        Object invoke = method.invoke(target, objects);
        System.out.println("后置代码增强....");
        return invoke;
    }
});
Target proxy = (Target) enhancer.create(); //创建代理对象

```

③调用代理对象的方法测试

```java
//测试,当调用接口的任何方法时，代理对象的代码都无序修改
proxy.method();
```

![](E:\01_notes\04_SSM框架\01_spring\00_spring笔记\img\测试2.png)

#### 8.7 AOP 相关概念

Spring 的 AOP 实现底层就是对上面的动态代理的代码进行了封装，封装后我们只需要对需要关注的部分进行代码编写，并通过配置的方式完成指定目标的方法增强。

在正式讲解 AOP 的操作之前，我们必须理解 AOP 的相关术语，常用的术语如下：

- Target（目标对象）：代理的目标对象(需要被增强的对象)
- Proxy （代理）：一个类被 AOP 织入增强后，就产生一个结果代理类
- Joinpoint（连接点）：所谓连接点是指那些被拦截到的点(点可以被认为是方法)。在spring中,这些点指的是方法，因为spring只支持方法类型的连接点（可以被增强的方法）
- Pointcut（切入点/切点）：所谓切入点是指我们要对哪些 Joinpoint 进行拦截的定义（真正被增强的方法）
- Advice（通知/ 增强）：所谓通知是指拦截到 Joinpoint 之后所要做的事情就是通知（怎么去做增强）
- Aspect（切面）：是切入点和通知（引介）的结合  切点+通知
- Weaving（织入）：是指把增强应用到目标对象来创建新的代理对象的过程。spring采用动态代理织入，而AspectJ采用编译期织入和类装载期织入

#### 8.8 AOP 开发明确的事项

##### 1)需要编写的内容

- 编写核心业务代码（目标类的目标方法）

- 编写切面类，切面类中有通知(增强功能方法)

- 在配置文件中，配置织入关系，即将哪些通知与哪些连接点进行结合

##### 2）AOP 技术实现的内容

Spring 框架监控切入点方法的执行。一旦监控到切入点方法被运行，使用代理机制，动态创建目标对象的代理对象，根据通知类别，在代理对象的对应位置，将通知对应的功能织入，完成完整的代码逻辑运行。

##### 3）AOP 底层使用哪种代理方式

在 spring 中，框架会根据目标类是否实现了接口来决定采用哪种动态代理的方式。

#### 8.9 知识要点

- aop：面向切面编程

- aop底层实现：基于JDK的动态代理 和 基于Cglib的动态代理

- aop的重点概念：

      Pointcut（切入点）：被增强的方法
      
      Advice（通知/ 增强）：封装增强业务逻辑的方法
      
      Aspect（切面）：切点+通知
      
      Weaving（织入）：将切点与通知结合的过程

- 开发明确事项：

      谁是切点（切点表达式配置）
      
      谁是通知（切面类中的增强方法）
      
      将切点和通知进行织入配置

### 9. 基于 XML 的 AOP 开发

#### 9.1 快速入门

①导入 AOP 相关坐标

②创建目标接口和目标类（内部有切点）

③创建切面类（内部有增强方法）

④将目标类和切面类的对象创建权交给 spring

⑤在 applicationContext.xml 中配置织入关系

⑥测试代码



①导入 AOP 相关坐标

```xml
<!--导入spring的context坐标，context依赖aop-->
<dependency>
  <groupId>org.springframework</groupId>
  <artifactId>spring-context</artifactId>
  <version>5.0.5.RELEASE</version>
</dependency>

<!-- aspectj的织入 -->
<dependency>
  <groupId>org.aspectj</groupId>
  <artifactId>aspectjweaver</artifactId>
  <version>1.8.13</version>
</dependency>
```

②创建目标接口和目标类（内部有切点）

```java
public interface TargetInterface {
    public void method();
}

public class Target implements TargetInterface {
    @Override
    public void method() {
        System.out.println("Target running....");
    }
}
```

③创建切面类（内部有增强方法）

```java
public class MyAspect {
    //前置增强方法
    public void before(){
        System.out.println("前置代码增强.....");
    }
}
```

④将目标类和切面类的对象创建权交给 spring

```xml
<!--配置目标类-->
<bean id="target" class="com.example.aop.Target"></bean>
<!--配置切面类-->
<bean id="myAspect" class="com.example.aop.MyAspect"></bean>

```

⑤在 applicationContext.xml 中配置织入关系

导入aop命名空间

```xml
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xsi:schemaLocation="
        http://www.springframework.org/schema/context
        http://www.springframework.org/schema/context/spring-context.xsd
        http://www.springframework.org/schema/aop
        http://www.springframework.org/schema/aop/spring-aop.xsd
        http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans.xsd">

```

⑤在 applicationContext.xml 中配置织入关系

配置切点表达式和前置增强的织入关系

```xml
<aop:config>
    <!--引用myAspect的Bean为切面对象-->
    <aop:aspect ref="myAspect">
        <!--配置Target的method方法执行时要进行myAspect的before方法前置增强-->
        <aop:before method="before" pointcut="execution(public void com.example.aop.Target.method())"></aop:before>
    </aop:aspect>
</aop:config>
```

⑥测试代码

```java
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class AopTest {
    @Autowired
    private TargetInterface target;
    @Test
    public void test1(){
        target.method();
    }
}
```

⑦测试结果

![](E:\01_notes\04_SSM框架\01_spring\00_spring笔记\img\测试3.png)

#### 9.2 XML 配置 AOP 详解

##### 1) 切点表达式的写法

表达式语法：

```java
execution([修饰符] 返回值类型 包名.类名.方法名(参数))
```

- 访问修饰符可以省略

- 返回值类型、包名、类名、方法名可以使用星号*  代表任意

- 包名与类名之间一个点 . 代表当前包下的类，两个点 .. 表示当前包及其子包下的类

- 参数列表可以使用两个点 .. 表示任意个数，任意类型的参数列表

例如：

```xml
execution(public void com.example.aop.Target.method())	
execution(void com.example.aop.Target.*(..))
execution(* com.example.aop.*.*(..))
execution(* com.example.aop..*.*(..))
execution(* *..*.*(..))
```

##### 2) 通知的类型

通知的配置语法：

```xml
<aop:通知类型 method=“切面类中方法名” pointcut=“切点表达式"></aop:通知类型>
```

![](E:\01_notes\04_SSM框架\01_spring\00_spring笔记\img\通知分类.png)

##### 3) 切点表达式的抽取

当多个增强的切点表达式相同时，可以将切点表达式进行抽取，在增强中使用 pointcut-ref 属性代替 pointcut 属性来引用抽取后的切点表达式。

切点表达式抽取的好处: 容易维护

```xml
<aop:config>
    <!--引用myAspect的Bean为切面对象-->
    <aop:aspect ref="myAspect">
        <aop:pointcut id="myPointcut" expression="execution(* com.example.aop.*.*(..))"/>
        <aop:before method="before" pointcut-ref="myPointcut"></aop:before>
    </aop:aspect>
</aop:config>
```

#### 9.3 知识要点

- aop织入的配置

```xml
<aop:config>
    <aop:aspect ref=“切面类”>
        <aop:before method=“通知方法名称” pointcut=“切点表达式"></aop:before>
    </aop:aspect>
</aop:config>
```

- 通知的类型：前置通知、后置通知、环绕通知、异常抛出通知、最终通知
- 切点表达式的写法：

```xml
execution([修饰符] 返回值类型 包名.类名.方法名(参数))
```

### 10.基于注解的 AOP 开发

#### 10.1 快速入门

基于注解的aop开发步骤：

①创建目标接口和目标类（内部有切点）

②创建切面类（内部有增强方法）

③将目标类和切面类的对象创建权交给 spring

④在切面类中使用注解配置织入关系

⑤在配置文件中开启组件扫描和 AOP 的自动代理

⑥测试



①创建目标接口和目标类（内部有切点）

```java
public interface TargetInterface {
    public void method();
}

public class Target implements TargetInterface {
    @Override
    public void method() {
        System.out.println("Target running....");
    }
}
```

②创建切面类（内部有增强方法)

```java
public class MyAspect {
    //前置增强方法
    public void before(){
        System.out.println("前置代码增强.....");
    }
}
```

③将目标类和切面类的对象创建权交给 spring

```java
@Component("target")
public class Target implements TargetInterface {
    @Override
    public void method() {
        System.out.println("Target running....");
    }
}
@Component("myAspect")
public class MyAspect {
    public void before(){
        System.out.println("前置代码增强.....");
    }
}
```

④在切面类中使用注解配置织入关系

```java
@Component("myAspect")
@Aspect
public class MyAspect {
    @Before("execution(* com.example.aop.*.*(..))")
    public void before(){
        System.out.println("前置代码增强.....");
    }
}
```

⑤在配置文件中开启组件扫描和 AOP 的自动代理

```xml
<!--组件扫描-->
<context:component-scan base-package="com.example.aop"/>

<!--aop的自动代理-->
<aop:aspectj-autoproxy></aop:aspectj-autoproxy>

```

⑥测试代码

```java
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class AopTest {
    @Autowired
    private TargetInterface target;
    @Test
    public void test1(){
        target.method();
    }
}
```

⑦测试结果

![](E:\01_notes\04_SSM框架\01_spring\00_spring笔记\img\测试4.png)

#### 10.2 注解配置 AOP 详解

##### 1) 注解通知的类型

通知的配置语法：@通知注解(“切点表达式")

![](E:\01_notes\04_SSM框架\01_spring\00_spring笔记\img\通知注解.png)

##### 2) 切点表达式的抽取

同 xml配置
aop 一样，我们可以将切点表达式抽取。抽取方式是在切面内定义方法，在该方法上使用@Pointcut注解定义切点表达式，然后在在增强注解中进行引用。具体如下：

```java
@@Component("myAspect")
@Aspect
public class MyAspect {
    @Before("MyAspect.myPoint()")
    public void before(){
        System.out.println("前置代码增强.....");
    }
    @Pointcut("execution(* com.example.aop.*.*(..))")
    public void myPoint(){}
}
```

#### 10.3 知识要点

- 注解aop开发步骤

①使用@Aspect标注切面类

②使用@通知注解标注通知方法

③在配置文件中配置aop自动代理<aop:aspectj-autoproxy/>



- 通知注解类型

![](E:\01_notes\04_SSM框架\01_spring\00_spring笔记\img\通知注解.png)



aspectj也是一种aop思想的框架,因为它的配置比较轻量级,所以spring这个框架引入了aspectj这个框架来配置



## 12. JdbcTemplate基本使用

### 12.1 JdbcTemplate基本概述

JdbcTemplate是spring框架中提供的一个对象，是对原始繁琐的Jdbc API对象的简单封装。spring框架为我们提供了很多的操作模板类。例如：操作关系型数据的JdbcTemplate和HibernateTemplate，操作nosql数据库的RedisTemplate，操作消息队列的JmsTemplate等等。

spring可以简化开发,spring和各种技术集成之后,可能这种技术的客户端代码比较繁琐,然后spring会对这些技术进行封装,封装后的技术不叫util,叫*Template

### 12.2 JdbcTemplate基本使用

①导入spring-jdbc和spring-tx坐标(spring-jdbc中封装了JdbcTemplate对象、spring-jdbc底层默认使用了spring-tx事务)

②创建数据库表和实体

③创建JdbcTemplate对象

④执行数据库操作

### 12.3 JdbcTemplate基本使用

  导入spring-jdbc和spring-tx坐标

```xml
<dependencies>
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>5.1.46</version>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-jdbc</artifactId>
            <version>5.0.5.RELEASE</version>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
            <version>5.0.5.RELEASE</version>
        </dependency>

        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.12</version>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-test</artifactId>
            <version>5.0.5.RELEASE</version>
        </dependency>
    </dependencies>
```

创建数据库表和实体

![](E:\01_notes\04_SSM框架\01_spring\00_spring笔记\img\mysql表.png)

```java
package com.example.domain;

public class Account {

    private String name;
    private double money;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    @Override
    public String toString() {
        return "Account{" +
                "name='" + name + '\'' +
                ", money=" + money +
                '}';
    }
}
```

创建JdbcTemplate对象

执行数据库操作

```java
@Test
//测试JdbcTemplate开发步骤
public void test1() throws PropertyVetoException {
    //创建数据源对象
    ComboPooledDataSource dataSource = new ComboPooledDataSource();
    dataSource.setDriverClass("com.mysql.jdbc.Driver");
    dataSource.setJdbcUrl("jdbc:mysql://localhost:3306/test");
    dataSource.setUser("root");
    dataSource.setPassword("root");

    JdbcTemplate jdbcTemplate = new JdbcTemplate();
    //设置数据源对象  知道数据库在哪
    jdbcTemplate.setDataSource(dataSource);
    //执行操作
    int row = jdbcTemplate.update("insert into account values(?,?)", "tom", 5000);
    System.out.println(row);

}
```



### 12.4 JdbcTemplate基本使用-spring产生模板对象分析

我们可以将JdbcTemplate的创建权交给Spring，将数据源DataSource的创建权也交给Spring，在Spring容器内部将数据源DataSource注入到JdbcTemplate模版对象中,然后通过Spring容器获得JdbcTemplate对象来执行操作。



### 12.5 JdbcTemplate基本使用-spring产生模板对象代码实现

配置如下：

```xml
<!--数据源对象-->
<bean id="dataSource" class="com.mchange.v2.c3p0.ComboPooledDataSource">
    <property name="driverClass" value="com.mysql.jdbc.Driver"></property>
    <property name="jdbcUrl" value="jdbc:mysql:///test"></property>
    <property name="user" value="root"></property>
    <property name="password" value="root"></property>
</bean>

<!--jdbc模板对象-->
<bean id="jdbcTemplate" class="org.springframework.jdbc.core.JdbcTemplate">
    <property name="dataSource" ref="dataSource"/>
</bean>
```

测试代码

```java
 @Test
//测试Spring产生jdbcTemplate对象
public void test2() throws PropertyVetoException {
    ApplicationContext app = new ClassPathXmlApplicationContext("applicationContext.xml");
    JdbcTemplate jdbcTemplate = app.getBean(JdbcTemplate.class);
    int row = jdbcTemplate.update("insert into account values(?,?)", "lisi", 5000);
    System.out.println(row);
}
```

### 12.6 JdbcTemplate基本使用-spring产生模板对象代码实现

将数据库的连接信息抽取到外部配置文件中，和spring的配置文件分离开，有利于后期维护

```properties
jdbc.driver=com.mysql.jdbc.Driver
jdbc.url=jdbc:mysql://localhost:3306/test
jdbc.username=root
jdbc.password=root
```

配置文件修改为:

```xml
<!--加载jdbc.properties-->
<context:property-placeholder location="classpath:jdbc.properties"/>

<!--数据源对象-->
<bean id="dataSource" class="com.mchange.v2.c3p0.ComboPooledDataSource">
    <property name="driverClass" value="${jdbc.driver}"/>
    <property name="jdbcUrl" value="${jdbc.url}"/>
    <property name="user" value="${jdbc.username}"/>
    <property name="password" value="${jdbc.password}"/>
</bean>

<!--jdbc模板对象-->
<bean id="jdbcTemplate" class="org.springframework.jdbc.core.JdbcTemplate">
    <property name="dataSource" ref="dataSource"/>
</bean>
```

### 12.7 JdbcTemplate基本使用-常用操作-更新操作

```java
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class JdbcTemplateCRUDTest {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    
	//修改更新
    @Test
    public void testUpdate(){
        jdbcTemplate.update("update account set money=? where name=?",10000,"tom");
    }
	//删除
    @Test
    public void testDelete(){
        jdbcTemplate.update("delete from account where name=?","tom");
    }

}
```

### 12.8 JdbcTemplate基本使用-常用操作-查询操作

```java
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class JdbcTemplateCRUDTest {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    
	//聚合查询
    @Test
    public void testQueryCount(){
        Long count = jdbcTemplate.queryForObject("select count(*) from account", Long.class);
        System.out.println(count);
    }
	//查询一个
    @Test
    public void testQueryOne(){
        String sql = "select * from account where name=?";
        Account account = jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<Account>(Account.class), "tom");
        System.out.println(account);
    }
	//查询所有
    @Test
    public void testQueryAll(){
        List<Account> accountList = jdbcTemplate.query("select * from account", new BeanPropertyRowMapper<Account>(Account.class));
        System.out.println(accountList);
    }

}
```

### 12.9 JdbcTemplate基本使用

①导入spring-jdbc和spring-tx坐标

②创建数据库表和实体

③创建JdbcTemplate对象

```java
JdbcTemplate jdbcTemplate = newJdbcTemplate();
jdbcTemplate.setDataSource(dataSource);
```

④执行数据库操作

    更新操作：
    
        jdbcTemplate.update (sql,params)
    
    查询操作：
    
        jdbcTemplate.query (sql,Mapper,params)
    
    	jdbcTemplate.queryForObject(sql,Mapper,params)



## 13 编程式事务控制相关对象

### 13.1 PlatformTransactionManager 

PlatformTransactionManager 接口是 spring 的事务管理器，它里面提供了我们常用的操作事务的方法。

![](E:\01_notes\04_SSM框架\01_spring\00_spring笔记\img\spring中的平台事务管理器中的方法.png)

注意：

PlatformTransactionManager 是接口类型，不同的 Dao 层技术则有不同的实现类，例如：Dao 层技术是jdbc 或 mybatis 时：org.springframework.jdbc.datasource.DataSourceTransactionManager 

Dao 层技术是hibernate时：org.springframework.orm.hibernate5.HibernateTransactionManager

### 13.2 TransactionDefinition

TransactionDefinition 是事务的定义信息对象(封装事务的一些参数)，里面有如下方法：

![](E:\01_notes\04_SSM框架\01_spring\00_spring笔记\img\事务的定义对象.png)

#### 1. 事务隔离级别

设置隔离级别，可以解决事务并发产生的问题，如脏读、不可重复读和虚读。

- ISOLATION_DEFAULT  （当前数据库默认的）

- ISOLATION_READ_UNCOMMITTED

- ISOLATION_READ_COMMITTED

- ISOLATION_REPEATABLE_READ

- ISOLATION_SERIALIZABLE

#### 2. 事务传播行为

事务的传播行为是为了解决业务方法去调用业务方法的时候，业务方法之间事务统一性的问题。

- **REQUIRED：如果当前没有事务，就新建一个事务，如果已经存在一个事务中，加入到这个事务中。一般的选择（默认值）**

- **SUPPORTS：支持当前事务，如果当前没有事务，就以非事务方式执行（没有事务）**

- MANDATORY：使用当前的事务，如果当前没有事务，就抛出异常

- REQUERS_NEW：新建事务，如果当前在事务中，把当前事务挂起。

- NOT_SUPPORTED：以非事务方式执行操作，如果当前存在事务，就把当前事务挂起

- NEVER：以非事务方式运行，如果当前存在事务，抛出异常

- NESTED：如果当前存在事务，则在嵌套事务内执行。如果当前没有事务，则执行 REQUIRED 类似的操作

  

- 超时时间：默认值是-1，没有超时限制。如果有，以秒为单位进行设置

- 是否只读：建议查询时设置为只读

### 13.3 TransactionStatus

TransactionStatus 接口提供的是事务具体的运行状态（这个对象是为了维护不同时间点事务的状态信息的），方法介绍如下。

![](E:\01_notes\04_SSM框架\01_spring\00_spring笔记\img\事务状态对象.png)

状态随着程序的运行会改变，这是一个被动的信息，不是我们主动设置的，不需要我们自己维护

### 13.4 编程式事务控制三大对象

编程式事务控制三大对象

- PlatformTransactionManager（接口，根据不同的Dao层而定的【技术不同，内部控制事务的方式不同，api不同】，指明事务是怎么控制的）

- TransactionDefinition（维护事务的一些参数信息的）

- TransactionStatus(封装在事务进行中的状态信息)

  在声明式事务中我们需要通过配置文件告诉spring。



## 14 基于 XML 的声明式事务控制

### 14.1 什么是声明式事务控制

Spring 的声明式事务顾名思义就是采用声明的方式来处理事务。这里所说的声明，就是指在配置文件中声明，用在 Spring 配置文件中声明式的处理事务来代替代码式的处理事务。

**声明式事务处理的作用**

- 事务管理不侵入开发的组件。具体来说，业务逻辑对象就不会意识到正在事务管理之中，事实上也应该如此，因为事务管理是属于系统层面的服务，而不是业务逻辑的一部分，如果想要改变事务管理策划的话，也只需要在定义文件中重新配置即可

- 在不需要事务管理的时候，只要在设定文件上修改一下，即可移去事务管理服务，无需改变代码重新编译，这样维护起来极其方便

**注意：Spring 声明式事务控制底层就是AOP。**

### 14.2 声明式事务控制的实现

声明式事务控制明确事项：

- 谁是切点？就是被增强的方法，也就是业务方法，也就是转账方法

- 谁是通知？就是增强，转账过程中就是一个事务控制（aop学习的时候，增强方法需要我们自己写，但是事务增强不需要，spring已经提供了，我们只需要引入对应的命名空间就好tx:）

- 配置切面？

①引入tx命名空间

```xml
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xmlns:tx="http://www.springframework.org/schema/tx"
       xsi:schemaLocation="
        http://www.springframework.org/schema/context
        http://www.springframework.org/schema/context/spring-context.xsd
        http://www.springframework.org/schema/aop
        http://www.springframework.org/schema/aop/spring-aop.xsd
        http://www.springframework.org/schema/tx 
        http://www.springframework.org/schema/tx/spring-tx.xsd
        http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans.xsd">


```

②配置事务增强

```xml
<!--平台事务管理器-->
<bean id="transactionManager" class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
    <!--平台事务关系器控制事务的时候是通过Connection，Connection又源于数据库连接池，所以这里也需要指明参数数据库连接池-->
    <property name="dataSource" ref="dataSource"></property>
</bean>

<!--通知  事务增强配置-->
<tx:advice id="txAdvice" transaction-manager="transactionManager">
    <!--配置事务的属性，也就是事务的定义对象，这里配置了事务的属性或者说是事务的参数-->
    <tx:attributes> 
        <!--这里被增强的切点是方法 这里name="*"表示任意方法 后面还可以配置isolation隔离级别 propagation传播行为 超时时间、是否只读  如下-->
        <!-- <tx:method name="*" isolation="DEFAULT" propagation="REQUIRED"/> -->
        <!-- <tx:attributes> 这个标签里面可以有很多tx:method，可以对不同的方法进行设置隔离级别 传播行为 超时时间、是否只读等-->
        <!--这里的name值是方法名字，如果是*这是任意方法，如果是update*则表示所有以update开头的方法-->
        <!--isolation隔离级别 propagation传播行为 超时时间、是否只读如果不设置，都有默认值-->
        <tx:method name="*"/>
    </tx:attributes>
</tx:advice>
```

③配置事务 AOP 织入

```xml
<!--事务的aop增强 只有配置了事务AOP织入，才能将通知和切点结合-->
<aop:config>
    <aop:pointcut id="myPointcut" expression="execution(* com.itheima.service.impl.*.*(..))"/>
    <!--学习aop的时候，使用的aop:aspect  但是这里不使用它，因为spring为事务的增强提供了aop:advisor  aop:advisor也叫作切面，一个通知的切面-->
    <!--通知要和切点结合起来才能增强，所以这里需要配置切点-->
    <aop:advisor advice-ref="txAdvice" pointcut-ref="myPointcut"></aop:advisor>
</aop:config>
```

④测试事务控制转账业务代码

```java
@Override
public void transfer(String outMan, String inMan, double money) {
    accountDao.out(outMan,money);
    int i = 1/0;
    accountDao.in(inMan,money);
}
```

### 14.3 切点方法的事务参数的配置

```xml
<!--事务增强配置-->
<tx:advice id="txAdvice" transaction-manager="transactionManager">
    <tx:attributes>
        <tx:method name="*"/>
    </tx:attributes>
</tx:advice>
```

其中，<tx:method> 代表切点方法的事务参数的配置，例如：

```xml
<tx:method name="transfer" isolation="REPEATABLE_READ" propagation="REQUIRED" timeout="-1" read-only="false"/>
```

- name：切点方法名称

- isolation:事务的隔离级别

- propogation：事务的传播行为

- timeout：超时时间

- read-only：是否只读

### 14.4 知识要点

**声明式事务控制的配置要点**

- 平台事务管理器配置

- 事务通知的配置

- 事务aop织入的配置



## 15 基于注解的声明式事务控制

### 15.1 使用注解配置声明式事务控制

在需要进行事务管理的方法上添加@Transactional注解

事务的隔离级别、传播行为、超时时间、是否只读作为参数在注解里配置@Transactional(isolation = Isolation.READ_COMMITTED,propagation = Propagation.REQUIRED)

@Transactional可以使用在类上，也可以使用在方法上

@Transactional就相当于事务增强的配置，如下：

```xml
<!--事务增强配置-->
<tx:advice id="txAdvice" transaction-manager="transactionManager">
    <tx:attributes>
        <tx:method name="*"/>
    </tx:attributes>
</tx:advice>
```

@Transactional(isolation = Isolation.READ_COMMITTED,propagation = Propagation.REQUIRED)这个里面的参数相当于如下：

```xml
<!--事务的aop增强 只有配置了事务AOP织入，才能将通知和切点结合-->
<aop:config>
    
    <aop:pointcut id="myPointcut" expression="execution(* com.itheima.service.impl.*.*(..))"/>
    <!--学习aop的时候，使用的aop:aspect  但是这里不使用它，因为spring为事务的增强提供了aop:advisor  aop:advisor也叫作切面，一个通知的切面-->
    <!--通知要和切点结合起来才能增强，所以这里需要配置切点-->
    <aop:advisor advice-ref="txAdvice" pointcut-ref="myPointcut"></aop:advisor>
</aop:config>
```

@Transactional注解如果加在类上 ，那么这个类里面所有的方法都是用了事务。

如果方法和类上面都有@Transactional，那么以方法上为准。

编写 applicationContext.xml 配置文件：

```xml
<!--组件扫描-->
<context:component-scan base-package="com.itheima"/>

<!--引入外部properties文件-->
<context:property-placeholder location="classpath:jdbc.properties"/>

<!--数据源对象-->
<bean id="dataSource" class="com.mchange.v2.c3p0.ComboPooledDataSource">
    <property name="driverClass" value="com.mysql.jdbc.Driver"></property>
    <property name="jdbcUrl" value="jdbc:mysql:///test"></property>
    <property name="user" value="root"></property>
    <property name="password" value="root"></property>
</bean>

<!--jdbc模板对象-->
<bean id="jdbcTemplate" class="org.springframework.jdbc.core.JdbcTemplate">
    <property name="dataSource" ref="dataSource"/>
</bean>

<!--配置平台事务管理器  告诉spring框架dao层使用的技术-->
<bean id="transactionManager" class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
    <property name="dataSource" ref="dataSource"></property>
</bean>

<!--事务的注解驱动 transaction-manager的值为平台事务管理器的id  只有配置了事务的注解驱动，类和方法上的@Transactional才能生效-->
<tx:annotation-driven transaction-manager="transactionManager"></tx:annotation-driven>
```

### 15.2 注解配置声明式事务控制解析

①使用 @Transactional 在需要进行事务控制的类或是方法上修饰，注解可用的属性同 xml 配置方式，例如隔离级别、传播行为等。

②注解使用在类上，那么该类下的所有方法都使用同一套注解参数配置。

③使用在方法上，不同的方法可以采用不同的事务参数配置。

④Xml配置文件中要开启事务的注解驱动<tx:annotation-driven />

### 15.3 知识要点

**注解声明式事务控制的配置要点**

- 平台事务管理器配置（xml方式）

- 事务通知的配置（@Transactional注解配置）

- 事务注解驱动的配置 <tx:annotation-driven/>

