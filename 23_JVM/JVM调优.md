## JVM调优

- 常见jvm调优问题四部曲：

  - 你了解jvm调优吗？
    - 回答：了解
  - 为什么要进行jvm调优？
    - 提高系统的稳定性，防止cpu飙升，内存占用剧增，oom异常。
    - 降低延迟（提高运行效率）
    - 提高系统吞吐量（提高系统处理能力）
  - 你对jvm进行过调优吗？
    - 回答：**没有在生产环境实际操作过，一般是组长和运维做，因为只有组长和运维的才有生产环境的权限，我自己对这块有研究过，也协助组长做过一点。**
  - 那你说说如何进行jvm调优？（重难点）
    - 举一个场景来说明，如何进行调优是今天的讲解重点。

- 调优前说明：
  - **一般情况没有什么问题（比如：访问效率正常，内存，cpu占用也在理想范围内），我们是不会对jvm进行调优。**
  - 当出现一些问题（**访问速度慢**，**cpu负载飙升**，**内存在快速增加甚至超过了最大内存**，**抛出oom异常**），这个时候我们就需要考虑进行cpu使用情况和jvm的内存情况分析，找到瓶颈（问题）所在然后解决。

- 场景：
  - 场景1：服务器的监控工具（**zabbix**）报警，cpu飙升，使用率达到90多%甚至100%
    
    - java常见情况：**死锁**，**无限循环**，**内存回收（GC）较高**(解释：垃圾较多，GC也是需要占用CPU的)。
    
    ```
      CPU飙升，监控工具检测到了就会给运维发送报警邮件
    ```
  - 场景2：服务器的监控工具（zabbix）报警（80%），内存占用快速增加，甚至超过jvm最大内存导致oom异常( java.lang.OutOfMemoryError )
  - 注意：场景1和场景2也可以同时出现：比如无用内存较多的情况，GC就会占用大量的CPU。
  
- 调优过程：
  - 使用jvm调优命令和工具，分析cpu占用情况和jvm内存与线程情况，找到问题所在。

    - 分析cpu使用情况（场景1）
      - 使用top命令找到最占cpu的进程pid(比如8859)

      - 使用top -Hp  8859 获取该进程下的最占用cpu的线程id

        - ```
          top -Hp  8859 #得到最占cpu的线程的pid
          printf '%x\n' 线程PID #得到线程PID对应的16进制的ID 因为下面jstack命令中的线程id是16进制的。
          ```

      - 使用jstack命令（jvm自带的命令）**获取该进程下的所有线程的详细信息**

        - ```
          jstack pid(进程pid) #在线查看
          jstack pid(进程pid)>stack.dump #下载为一个文件，然后使用可视化工具查看
          ```

      - 从这些信息中找到刚才占用cpu最多的线程的信息，进行分析。

        - 情况1:如果是自己写的代码，则定位到具体代码查看是否逻辑有问题，比如死锁，无限循环等。
        - 情况2：如果该线程名中包含GC，那么表示该线程是垃圾回收线程，说明导致cpu飙升的原因是内存被快速消耗，需要花大量的CPU来完成GC。如果是该情况就需要分析jvm内存使用情况。

    - 分析内存使用情况（场景1，场景2）

      - 使用jmap命令**获取jvm内存使用情况**（堆内存占用大小，GC）

        - ```
          jmap -heap pid # 在线查看堆内存信息
          jmap -dump:format=b,file=xxx.dump pid #下载来一个文件，然后使用工具进行查看堆内存情况（常用）
          ```

      - 使用工具查看jvm内存情况（更直观）

        - 常用工具：JVisualVM，jdk自带的分析jvm内存和线程的可视化工具

      - 找到内存使用最高的对象所对应的代码。

        - 如果代码有问题就修改，比如：不断的读取大数据到内存。
        - 如果代码没问题（合理需求），则需要调整堆内存的大小参数。

    - 优化jvm常用参数（如果代码没有问题则进行参数调整）

      - 如果内存不够用，适当调大堆内存的初始值和堆内存的最大值。（常用）（重点回答）

        - **-Xmx和-Xms一般都是设置相等的**，避免在生产环境由于heap内存扩大或缩小**导致应用停顿，**也避免每次垃圾回收完成后JVM重新分配内存。

        - 不是越大越好，如果太大，会造成资源浪费，同时会导致full GC所需的时间越久，越大概率会导致服务器出现停止响应。

        - ```
          -Xms4096m:初始化堆内存大小（默认值，最大物理内存1/64）
          -Xmx4096m:最大堆内存大小（默认值，最大物理内存1/4）
          这个值一般根据以往经验或者根据上线前的压力测试来设置，一般大小的web项目设置为4个g。
          
          ```
    
java -Xms4096 -Xmx4096 C
          ```

      - 调整年轻代和老年代的内存占比（重点回答）
    
        - 一般设置为1：4
        
          - ```
        -XX:NewRatio=4 #表示年轻代比老年代1:4
            ```
    
  - 年轻代为啥比老年代小：
          
    - 在最坏情况下young gen里可能所有对象都还活着，而如果它们全部都要晋升到old gen的话，那old gen里的剩余空间必须能容纳下这些对象才行，这就需要old gen比young gen大。（否则young GC就无法进行，而必须做full GC才能应付了，这样性能就会大大下降）。 
          
      个人理解：
          
            ```
            新new的对象在年轻代，经过一次gc后没有被回收，就会转移到老年代，简单的说就是年轻代中没有被回收的对象都会被转移到老年代中。老年代就可以理解为一个容器，不断接收年轻代中转移的数据，既然要接收年轻代转移的数据，肯定不能小于年轻代。如果和年轻代一样大的话，那么年轻代就无法再转移到老年代。所以应该大于年轻代。如果两个设置一样，老年代满了之后，就会就行full GC(这非常消耗内存的)
      ```
          
      
      
      - 调整年轻代中Eden(E区)和Surivor(S区)
      
        - 一般设置为8：2
      ```
      
    - ```
            -XX:SurvivorRatio=8 #新生代 Eden 和 Survivor 比例为 8:2；
      ```
  
- 设置不同的垃圾回收器
      
        - ```
          –XX:+UseParNewGC：指定使用 ParNew + Serial Old垃圾回收器组合；  年轻代并行、老年代串行
          -XX:+UseParallelOldGC：指定使用 ParNew + ParNew Old 垃圾回收器组合；  年轻代并行、老年代并行
    -XX:+UseConcMarkSweepGC：指定使用 CMS + Serial Old 垃圾回收器组合；  年轻代CMS、老年代串行
          ```

      - 设置打印gc信息（设置）

        - 目的：可以通过日志查看gc信息,来分析使用的哪种垃圾回收器，和gc效率。也是查找问题的一种方案。
        
        - ```
    -XX:+PrintGC：开启打印 gc 信息；
          -XX:+PrintGCDetails：打印 gc 详细信息。
          ```
    ```
      
      - ![](E:\05_资料\javaEE黑马29期面试复习资料\jvm\jvm调优\Snipaste_2020-01-13_18-44-30.png)
      
    ```
          DefNew – 使用的垃圾收集器的名字. DefNew 这个名字代表的是: 单线程(single-threaded), 采用标记复制(mark-copy)算法的, 使整个JVM暂停运行(stop-the-world)的年轻代(Young generation) 垃圾收集器(garbage collector).
          
          如果发现一个GC回收的时间很长，我们可以设置并行的垃圾回收器（相当于多线程回收）
          ```
    
      - 设置内存溢出后dump当时的内存情况
      
        - ```
          -XX:+HeapDumpOnOutOfMemoryError
          ```
      
      - 注意：
      
      - 一般参数调整了之后会进行一些压力测试，看是否有问题。没有问题再在生产环境设置。
  
- 总结

  - 那你说说如何进行jvm调优？
    - 有一次服务器的监控工具（zabbix）报警，cpu飙升，使用率达到90多%，而且内存占用也非常大。当时几个组长就开始进行紧急处理，我虽然直接上手操作但是基本过程我还是记得的，而且下来也自己做了研究和整理。
    - 处理过程大概是这样的
      - 先使用top 命令找到最占用cpu的进程id和该进程中最占cpu的线程id
      - 使用jstack命令查看该线程的信息。
      - 我们发现这个线程是一个gc线程，然后就猜测可能是内存消耗太快导致gc占用cpu太高。
      - 然后我们就使用jmap命令讲jvm堆内存情况 dump下来
      - 使用一个jdk自动的工具JVisualVM的图形化工具对dump的堆内存文件进行分析
        - 我也参与了这块使用JVisualVM工具进行分析。
      - 发现有一个对象占用了很多内存，然后找到其对应的代码
      - 发现这块代码有问题：每次用户访问都会加载大量数据到内存中，访问量一多，gc的频率就会很高，从而导致cpu也飙升。
    - 解决方案：
      - 当时应急处理，先把堆内存调大一点，然后重启tomcat。重启后，cpu和内存使用全部降下来了，而且内存大了些，gc的操作也不会经常进行，cpu也比较稳定，但是这只是应急，不是长久。
      - 后续处理：
        - 负责这块代码的开发，把代码修改后测试通过后，当天晚上就更新到服务器上了。
          - 如果被问代码怎么修改的，可以回答，这块不是我负责，具体我不清楚，总之就是不能加载大量数据到内存中。

    - 如果被追问，可以调整哪些参数来优化jvm，你就说上面介绍的几种参数。