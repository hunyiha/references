# 分布式ID生成解决方案

## 1. 分布式ID生成解决方案

常见的方式。可以利用数据库也可以利用程序生成，一般来说全球唯一。



## 2. UUID

算法的核心思想是结合机器的网卡、当地时间、一个随记数来生成UUID。 

**优点**：

- 1）简单，代码方便。
- 2）生成ID性能非常好，基本不会有性能问题。
- 3）全球唯一，在数据迁移，系统数据合并，或者数据库变更等情况下，可以从容应对。

**缺点**：

- 1）没有排序，无法保证趋势递增。
- 2）UUID往往是使用字符串存储，查询的效率比较低。
- 3）存储空间比较大，如果是海量数据库，就需要考虑存储量的问题。
- 4）传输数据量大
- 5）不可读。



## 3. Redis

当使用数据库来生成 ID性能不够要求的时候，我们可以尝试使用Redis来生成ID。这主要依赖于Redis是单线程的，所以也可以用生成全局唯一的ID。可以用Redis的原子操作INCR和INCRBY来实现。

**优点**：

- 1）不依赖于数据库，灵活方便，且性能优于数据库。
- 2）数字ID天然排序，对分页或者需要排序的结果很有帮助。

**缺点**：

- 1）如果系统中没有Redis，还需要引入新的组件，增加系统复杂度。
- 2）需要编码和配置的工作量比较大。
- 3）网络传输造成性能下降。



## 3 开源算法snowflake

 snowflake是Twitter开源的分布式ID生成算法，结果是一个long型的ID。其核心思想是：使用41bit作为毫秒数，10bit作为机器的ID（5个bit是数据中心，5个bit的机器ID），12bit作为毫秒内的流水号（意味着每个节点在每毫秒可以产生 4096 个 ID），最后还有一个符号位，永远是0

![](E:\01_notes\09_分布式解决方案\01_分布式id\img\Snipaste_2020-02-28_10-24-58.png)



### 3.1 snowflake快速入门

#### 3.1.1 快速入门

（1）新建工程，将工具类IdWorker.java拷贝到工程中。

```java
package com.changgou.util;

import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.net.NetworkInterface;

/**
 * <p>名称：IdWorker.java</p>
 * <p>描述：分布式自增长ID</p>
 * <pre>
 *     Twitter的 Snowflake　JAVA实现方案
 * </pre>
 * 核心代码为其IdWorker这个类实现，其原理结构如下，我分别用一个0表示一位，用—分割开部分的作用：
 * 1||0---0000000000 0000000000 0000000000 0000000000 0 --- 00000 ---00000 ---000000000000
 * 在上面的字符串中，第一位为未使用（实际上也可作为long的符号位），接下来的41位为毫秒级时间，
 * 然后5位datacenter标识位，5位机器ID（并不算标识符，实际是为线程标识），
 * 然后12位该毫秒内的当前毫秒内的计数，加起来刚好64位，为一个Long型。
 * 这样的好处是，整体上按照时间自增排序，并且整个分布式系统内不会产生ID碰撞（由datacenter和机器ID作区分），
 * 并且效率较高，经测试，snowflake每秒能够产生26万ID左右，完全满足需要。
 * <p>
 * 64位ID (42(毫秒)+5(机器ID)+5(业务编码)+12(重复累加))
 *
 * @author Polim
 */
public class IdWorker {
    // 时间起始标记点，作为基准，一般取系统的最近时间（一旦确定不能变动）
    private final static long twepoch = 1288834974657L;
    // 机器标识位数
    private final static long workerIdBits = 5L;
    // 数据中心标识位数
    private final static long datacenterIdBits = 5L;
    // 机器ID最大值
    private final static long maxWorkerId = -1L ^ (-1L << workerIdBits);
    // 数据中心ID最大值
    private final static long maxDatacenterId = -1L ^ (-1L << datacenterIdBits);
    // 毫秒内自增位
    private final static long sequenceBits = 12L;
    // 机器ID偏左移12位
    private final static long workerIdShift = sequenceBits;
    // 数据中心ID左移17位
    private final static long datacenterIdShift = sequenceBits + workerIdBits;
    // 时间毫秒左移22位
    private final static long timestampLeftShift = sequenceBits + workerIdBits + datacenterIdBits;

    private final static long sequenceMask = -1L ^ (-1L << sequenceBits);
    /* 上次生产id时间戳 */
    private static long lastTimestamp = -1L;
    // 0，并发控制
    private long sequence = 0L;

    private final long workerId;
    // 数据标识id部分
    private final long datacenterId;

    public IdWorker(){
        this.datacenterId = getDatacenterId(maxDatacenterId);
        this.workerId = getMaxWorkerId(datacenterId, maxWorkerId);
    }
    /**
     * @param workerId
     *            工作机器ID
     * @param datacenterId
     *            序列号
     */
    public IdWorker(long workerId, long datacenterId) {
        if (workerId > maxWorkerId || workerId < 0) {
            throw new IllegalArgumentException(String.format("worker Id can't be greater than %d or less than 0", maxWorkerId));
        }
        if (datacenterId > maxDatacenterId || datacenterId < 0) {
            throw new IllegalArgumentException(String.format("datacenter Id can't be greater than %d or less than 0", maxDatacenterId));
        }
        this.workerId = workerId;
        this.datacenterId = datacenterId;
    }
    /**
     * 获取下一个ID
     *
     * @return
     */
    public synchronized long nextId() {
        long timestamp = timeGen();
        if (timestamp < lastTimestamp) {
            throw new RuntimeException(String.format("Clock moved backwards.  Refusing to generate id for %d milliseconds", lastTimestamp - timestamp));
        }

        if (lastTimestamp == timestamp) {
            // 当前毫秒内，则+1
            sequence = (sequence + 1) & sequenceMask;
            if (sequence == 0) {
                // 当前毫秒内计数满了，则等待下一秒
                timestamp = tilNextMillis(lastTimestamp);
            }
        } else {
            sequence = 0L;
        }
        lastTimestamp = timestamp;
        // ID偏移组合生成最终的ID，并返回ID
        long nextId = ((timestamp - twepoch) << timestampLeftShift)
                | (datacenterId << datacenterIdShift)
                | (workerId << workerIdShift) | sequence;

        return nextId;
    }

    private long tilNextMillis(final long lastTimestamp) {
        long timestamp = this.timeGen();
        while (timestamp <= lastTimestamp) {
            timestamp = this.timeGen();
        }
        return timestamp;
    }

    private long timeGen() {
        return System.currentTimeMillis();
    }

    /**
     * <p>
     * 获取 maxWorkerId
     * </p>
     */
    protected static long getMaxWorkerId(long datacenterId, long maxWorkerId) {
        StringBuffer mpid = new StringBuffer();
        mpid.append(datacenterId);
        String name = ManagementFactory.getRuntimeMXBean().getName();
        if (!name.isEmpty()) {
            /*
             * GET jvmPid
             */
            mpid.append(name.split("@")[0]);
        }
        /*
         * MAC + PID 的 hashcode 获取16个低位
         */
        return (mpid.toString().hashCode() & 0xffff) % (maxWorkerId + 1);
    }

    /**
     * <p>
     * 数据标识id部分
     * </p>
     */
    protected static long getDatacenterId(long maxDatacenterId) {
        long id = 0L;
        try {
            InetAddress ip = InetAddress.getLocalHost();
            NetworkInterface network = NetworkInterface.getByInetAddress(ip);
            if (network == null) {
                id = 1L;
            } else {
                byte[] mac = network.getHardwareAddress();
                id = ((0x000000FF & (long) mac[mac.length - 1])
                        | (0x0000FF00 & (((long) mac[mac.length - 2]) << 8))) >> 6;
                id = id % (maxDatacenterId + 1);
            }
        } catch (Exception e) {
            System.out.println(" getDatacenterId: " + e.getMessage());
        }
        return id;
    }
}
```



（ 2）编写代码

```java
 public static void main(String[] args) {

        IdWorker idWorker=new IdWorker(0,0);

        for(int i=0;i<10000;i++){
            long nextId = idWorker.nextId();
            System.out.println(nextId);
        }
    }
```



#### 3.1.2 在springboot项目中应用雪花算法

2) 在配置文件application.yml添加配置

```yaml
workerId: 0  
datacenterId: 0
```



3) 修改SpringBootApplication，增加代码

```java


@Value("${workerId}")  //读取配置文件的workerId
private Integer workerId;

@Value("${datacenterId}") //读取配置文件中的datacenterId
private Integer datacenterId;


@Bean  //将生成的idWorker放置到spring容器中
public IdWorker idWorker(){
    return new IdWorker(workerId,datacenterId);
}

```

完整的配置如下：

```java
@SpringBootApplication   //springboot的启动类标识
@EnableEurekaClient  //开启eureka
@MapperScan(basePackages = {"com.changgou.goods.dao"})  //通用mapper的包扫描，生成代理对象
public class GoodsApplication {

    @Value("${workerId}")
    private Integer workerId;

    @Value("${datacenterId}")
    private Integer datacenterId;

    public static void main(String[] args) {
        SpringApplication.run( GoodsApplication.class);
    }

    @Bean
    public IdWorker idWorker(){
        return new IdWorker(workerId,datacenterId);
    }
}
```



## 4. 数据库sequence

使用数据库的id自增策略，如MySQL的auto_increment。并且可以使用两台数据库分别设置不同步长，生成不重复ID的策略来实现高可用。

- **优点**：数据库生成的ID绝对有序，高可用实现方式简单。
- **缺点**：需要独立部署数据库实例，成本高，有性能瓶颈





## 5. 分布式id生成方式的比较

​             

| 方式             | 优点                         | 缺点                       |
| ---------------- | ---------------------------- | -------------------------- |
| UUID             | 本地生成，无中心，无性能瓶颈 | 无序，过长                 |
| MongoDB ObjectId | 本地生成，含时间戳，有序     | 过长                       |
| 数据库sequence   | 有序                         | 中心生成，独立部署数据库   |
| 雪花算法         | 有序，Long型                 | 中心生成，独立部署ID生成器 |

