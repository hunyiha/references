# 远程调用

## 1. okhttp

### 1.1 okhttp简介

OKHttp是Square公司辨析的一个网络请求框架，也是目前市面上使用最多的网络框架之一。OKHttp是基于HTTP协议封装的一套请求客户端，在请求底层支持连接同一个地址的链接共享同一个Socket。

OkHttp作为当前Android端最火热的网络请求框架之一，有很多的**优点**：

- 支持HTTP/2 协议，允许连接到同一个主机地址的所有请求共享Socket。可以有效管理网络连接以及提高连接复用率。
- 在HTTP/2协议不可用的情况下，通过连接池减少请求的延迟。
- GZip透明压缩减少传输的数据包大小。
- 缓存请求，避免同一个重复的网络请求。



### 1.2 入门使用

导入maven坐标：

```xml
<!--okhttp依赖-->
<dependency>
    <groupId>com.squareup.okhttp3</groupId>
    <artifactId>okhttp</artifactId>
    <version>3.9.0</version>
</dependency>
```



远程调用示例:

```java
//创建OkHttpClient对象
OkHttpClient okHttpClient = new OkHttpClient();

String url = "http://192.168.200.128/ad_update?position="+message;

Request request = new Request.Builder()
    		.url(url)   //请求链接
    		.build(); 	//创建Request对象

//Response response = client.newCall(request).execute();//获取Response对象
Call call = okHttpClient.newCall(request);

call.enqueue(new Callback() {
    @Override
    public void onFailure(Call call, IOException e) {
        //请求失败
        e.printStackTrace();
    }

    @Override
    public void onResponse(Call call, Response response) throws IOException {
        //请求成功
        System.out.println("请求成功:"+response.message());
    }
});
```



在上面OKHttpClient和Request创建好之后，就开始发起HTTP请求了。OkHttp中请求方式分为同步请求（*client.newCall(request).execute()* ）和异步请求（*client.newCall(request).enqueue()*）两种，其中同步请求和异步请求的区别就是同步请求会阻塞当前线程，一异步请求会放到线程池中执行。



### 1.3 参考文档

 okhttp原理分析： https://juejin.im/post/5d406ac85188255d2e32c892 



## 2. RestTemplate



### 2.2 SpringBoot中使用RestTemplate

在启动类中创建RestTemplate的Bean

```java
@SpringBootApplication
public class OAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(OAuthApplication.class,args);
    }

    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
}
```

```java
MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
body.add("grant_type","password");
body.add("username",username);
body.add("password",password);

//MultiValueMap是接口  LinkedMultiValueMap是它的一个实现类
MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
headers.add("Authorization",this.getHttpBasic(clientId,clientSecret));
HttpEntity<MultiValueMap<String,String>> requestEntity = new HttpEntity<>(body,headers);

restTemplate.setErrorHandler(new DefaultResponseErrorHandler(){
    @Override
    public void handleError(ClientHttpResponse response) throws IOException {
        if (response.getRawStatusCode()!=400 && response.getRawStatusCode()!=401){
            super.handleError(response);
        }
    }
});

/**
*	第一个参数：请求的url
*	第二个参数：请求方式
*	第三个参数：HttpEntity 封装请求参数
*   第四个参数：返回值的类型
*/
ResponseEntity<Map> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity, Map.class);
```



## 3 feign接口调用

微服务之间的调用使用feign接口调用的方式。

### 3.1导入依赖

```xml
<!--在服务的提供方中导入feign的依赖-->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-openfeign</artifactId>
    <scope>provided</scope>
</dependency>

<!--服务调用方需要依赖提供方的feign接口-->
<dependency>
    <groupId>com.changgou</groupId>
    <artifactId>changgou_service_user_api</artifactId>
    <version>1.0-SNAPSHOT</version>
</dependency>
```



### 3.2 首先定义feign接口

```java
@FeignClient(name = "user")  //@FeignClient表示这是一个feign客户端  name的值是服务的名称
public interface UserFeign {

    @GetMapping("/user/load/{username}")  //方法是controller中复制的，路径需要加上类路径
    public User findUserInfo(@PathVariable("username") String username);
}

```

### 3.3服务调用方开启feign调用

在服务调用方的启动类上开启feign调用。

```java
//开启feign的调用 参数basePackages的值是一个数组，是feign接口所在的包
@EnableFeignClients(basePackages = {"com.changgou.user.feign"}) 
```



```java
@SpringBootApplication
@EnableDiscoveryClient
@MapperScan(basePackages = "com.changgou.auth.dao")
@EnableFeignClients(basePackages = {"com.changgou.user.feign"}) //开启feign的调用
public class OAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(OAuthApplication.class,args);
    }

    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
}
```



### 3.4 注入调用

在需要调用的地方直接注入feign接口，直接调用即可。

