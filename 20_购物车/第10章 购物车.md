## 第10章 购物车

### 1 SpringSecurity权限控制

![1576310211655](C:\Users\admin\AppData\Roaming\Typora\typora-user-images\1576310211655.png)

用户认证：Who are you?

用户授权：What can you do?

微服务中的访问流程

```
1. 用户每次访问微服务的时候，先去oauth2.0服务登录，登录后再访问微服务网关，微服务网关将请求转发给其他微服务处理。

2.由于我们项目使用了微服务，任何用户都有可能使用任意微服务，此时我们需要控制相关权限，例如：普通用户角色不能使用用户的删除操作、商品的审核，只有管理员才可以使用,那么这个时候就需要使用到SpringSecurity的权限控制功能了。对登录的用户进行权限和角色的控制
```

因为JWT令牌里面是可以包含自定义数据的，基于这一点，我们就可以在用户获取令牌的时候，在令牌中添加相关的角色和权限的信息，在资源服务中就可以获取令牌中角色和权限的信息，在访问资源的时候就可以从令牌中获取角色和权限的信息，从而判断用户是否能够访问该方法，是否有权限去进行该操作，如果没有权限的话就不许与用户去进行这个操作，返回一个401，这就是所谓的权限控制，也就是用户授权

那么如何通过springsecurity进行用户权限的控制？

- 将用户角色和权限加载到JWT令牌中
- 使用@PreAuthorize注解进行控制

####1.1 角色权限加载

在changgou-user-oauth服务中，com.changgou.oauth.config.UserDetailsServiceImpl该类实现了加载用户相关信息，如下代码：

```java
//创建User对象 //根据用户名查询用户信息
String pwd = new BCryptPasswordEncoder().encode("itheima");
//创建User对象
String  permissions = "salesman,accountant,user";
UserJwt userDetails = new UserJwt(username,password,AuthorityUtils.commaSeparatedStringToAuthorityList(permissions));
return userDetails;
```

上述代码给登录用户定义了三个角色，分别为 salesman , accountant , user ，这一块我们目前使用的是硬编码方式将角色写死了。

JWT令牌的解析结果：

```json
{
    "scope":["app"],
    "name":null,
    "id":null,
    "exp":1576364877,
    "authorities":["accountant","user","salesman"],
    "jti":"723390cc-dc5c-4153-9ef6-0874495fda23",
    "client_id":"changgou",
    "username":"heima"
}
```

从上述JWT令牌的解析结果来看，当用户登录成功，在生成JWT令牌的时候已经将角色和权限的信息包含到了JWT令牌中，然后就可以通过SpringSecurity中的相关注解进行权限的控制。

####1.2 角色权限控制

@PreFilter 和 @PostFilter 。其中前两者可以用来在方法调用前或者调用后进行权限检查，后两者可以用来对集合类型的参数或者返回值进行过滤。在需要控制权限的方法上，我们可以添加@PreAuthorize 注解，用于方法执行前进行权限检查，校验用户当前角色是否能访问该方法。



在资源服务的配置类中开启SpringSecurity的权限注解

```java
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)//激活方法上的PreAuthorize注解 即开启springsecurity的权限注解
```

在需要进行权限控制的handle方法上使用@PreAuthorize注解进行

```java
@PreAuthorize("hasAnyAuthority('admin')")  //表示传过来的JWT令牌中必要要有一个admin的权限，这样才能操作   英 /ˈɔːθəraɪz/  批准，授予 v
```

如果没有admin这个权限的话就会报异常：

```java
org.springframework.security.access.AccessDeniedException: 不允许访问
```

####1.3 其它

```java
如果希望一个方法能被多个角色访问，配置: @PreAuthorize("hasAnyAuthority('admin','user')")
如果希望一个类都能被多个角色访问，在类上配置: @PreAuthorize("hasAnyAuthority('admin','user')")
```



### 2 购物车

购物车分为用户登录购物车和未登录购物车操作

国内知名电商京东用户登录和不登录都可以操作购物车，如果用户不登录，操作购物车可以将数据存储到Cookie，用户登录后购物车数据可以存储到Redis中，再将之前未登录加入的购物车合并到Redis中即可。

淘宝天猫则采用了另外一种实现方案，用户要想将商品加入购物车，必须先登录才能操作购物车。

我们今天实现的购物车是天猫解决方案，即用户必须先登录才能使用购物车功能。

####2.1 购物车业务分析
(1)需求分析

用户在商品详细页点击加入购物车，提交商品**SKU编号**和**购买数量**，添加到购物车。购物车展示页面如下：

![1576331382535](C:\Users\admin\AppData\Roaming\Typora\typora-user-images\1576331382535.png)

SPU = Standard Product Unit （标准产品单位）

SKU=stock keeping unit( 库存量单位)

(2)购物车实现思路

![1576331779733](C:\Users\admin\AppData\Roaming\Typora\typora-user-images\1576331779733.png)

我们实现的是用户登录后的购物车，用户将商品加入购物车的时候，直接将要加入购物车的详情存入到Redis即可。每次查看购物车的时候直接从Redis中获取。

(3)表结构分析
用户登录后将商品加入购物车，需要存储商品详情以及购买数量，购物车详情表如下：
changgou_order数据中tb_order_item表：

```mysql
CREATE TABLE `tb_order_item` (
    `id` varchar(20) COLLATE utf8_bin NOT NULL COMMENT 'ID',
    `category_id1` int(11) DEFAULT NULL COMMENT '1级分类',
    `category_id2` int(11) DEFAULT NULL COMMENT '2级分类',
    `category_id3` int(11) DEFAULT NULL COMMENT '3级分类',
    `spu_id` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT 'SPU_ID',
    `sku_id` bigint(20) NOT NULL COMMENT 'SKU_ID',
    `order_id` bigint(20) NOT NULL COMMENT '订单ID',
    `name` varchar(200) COLLATE utf8_bin DEFAULT NULL COMMENT '商品名称',
    `price` int(20) DEFAULT NULL COMMENT '单价',
    `num` int(10) DEFAULT NULL COMMENT '数量',
    `money` int(20) DEFAULT NULL COMMENT '总金额',
    `pay_money` int(11) DEFAULT NULL COMMENT '实付金额',
    `image` varchar(200) COLLATE utf8_bin DEFAULT NULL COMMENT '图片地址',
    `weight` int(11) DEFAULT NULL COMMENT '重量',
    `post_fee` int(11) DEFAULT NULL COMMENT '运费',
    `is_return` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '是否退货',
    PRIMARY KEY (`id`),
    KEY `item_id` (`sku_id`),
    KEY `order_id` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
```

一个订单中可能存在很多个商品，每一个商品对于订单来说都是一个订单项，每一个商品对于购物车来说就是一个购物项



### 2.2 添加购物车



![1576332462688](C:\Users\admin\AppData\Roaming\Typora\typora-user-images\1576332462688.png)

思路：

1.添加购物车前台提交的数据是商品的skuID以及数量，购物车是写在订单微服务中的，所以我们需要基于feign的形式去远程调用goods服务，根据skuId去查询商品的详细信息。

- 在商品服务中提供根据skuId查询商品详情的接口

- 让接口以feign的形式向外暴露

####2.2.1 获取sku数据
goods服务中定义根据id查询sku对象实现

```java
@RestController
@CrossOrigin
@RequestMapping("/sku")
public class SkuController {
    @Autowired
    private SkuService skuService;
    
    @GetMapping("/{id}")
    public Result<Sku> findById(@PathVariable("id") String id){
        Sku sku = skuService.findById(id);
        return new Result(true,StatusCode.OK,"查询成功",sku);
    }
}
```

####2.2.2 定义feign接口
goods-api工程中定义skuFeign接口,并定义查询方法

```java
/**
* 根据id查询sku信息
* @param id
* @return
*/
@GetMapping("/{id}")
public Result<Sku> findById(@PathVariable("id") String id);
```

skuFeign接口完整形式：

```java
@FeignClient(name="goods") //使用name指明操作的是哪一个服务
public interface SkuFeign {
    
    /**
    * 根据id查询sku信息
    * @param id
    * @return
    */
    @GetMapping("/sku/{id}")
    public Result<Sku> findById(@PathVariable("id") String id);
}
```

暴露的feign接口的书写规则：

1.首先导入feign的起步依赖,以及暴露的接口所需要的实体类POJO

```xml
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-openfeign</artifactId>
    <scope>provided</scope>
</dependency>
```

2.直接将需要暴露的方法签名以及上面的注解全部复制过来,注意添加类路径，feign接口的详细信息见skuFeign接口完整形式

```java
@GetMapping("/sku/{id}")
public Result<Sku> findById(@PathVariable("id") String id);
```

3.在需要调用这个feign接口的微服务中导入feign、feign接口依赖，在启动类上开启feign的远程调用

```java
@EnableFeignClients(basePackages = {"com.changgou.goods.feign"}) //开启feign的远程调用，通过basePackages属性指明需要调用的feign接口所在包
```

4.注入，然后完成调用

####2.2.3 订单服务添加依赖

```xml
<dependency>
    <groupId>com.changgou</groupId>
    <artifactId>changgou_service_goods_api</artifactId>
    <version>1.0-SNAPSHOT</version>
</dependency>
```

#### 2.2.4 订单服务启动类添加feign接口扫描

##### 2.2.5 订单服务新建CartController

```java
@RestController
@CrossOrigin
@RequestMapping("/cart")
public class CartController {
    @Autowired
    private CartService cartService;
    
    /**
    * 添加购物车
    * @param skuId
    * @param num
    * @return
    */
    @GetMapping("/add")
    public Result add(@RequestParam("skuId") String skuId, @RequestParam("num") Integer num){
        //暂时静态,后续动态获取
        String username = "itcast";
        cartService.add(skuId,num,username);
        return new Result(true, StatusCode.OK,"加入购物车成功");
    }
}

```

#### 2.2.6 订单服务添加cartService，实现添加购物车

添加购物车需要的参数：商品的skuId，添加的商品数量num, 已经该购物车是属于哪一个用户的，即username

购物车里面的所有信息是保存到redis中的

添加购物车的业务逻辑分析：

- 1.查询redis中的商品信息
- 2.如果当前商品在redis中存在，则之前就已经添加到购物车中，此时需要更新商品的数量以及价钱
- 3.如果当前商品在redis中不存在，则直接将商品添加到redis中
- 4.添加商品的时候需要基于feign去调用别的接口

需要操作redis,则需要在配置文件中配置redis的连接信息，注入redis的模板类，还需要明确信息(即购物车信息)在redis中是以什么形式储存的

购物车信息在redis中是通过hash的形式保存的，key   field  value,即key   map的形式,一个key就对应一个用户的购物车，一个key下面有若干个map,每一个map就对应购物车中的一个购物项

- hash的key:  自定义一个前缀(cart_)，然后加上用户名username   cart_username作为key
- map的key:skuId
- map的value:通过一个自定义类(这个类里面封装了商品的信息，即就是上面提到的订单项)   上文已经提到储存的表结构，changgou_order数据中tb_order_item表



### 2.4 购物车列表

![1576390987067](C:\Users\admin\AppData\Roaming\Typora\typora-user-images\1576390987067.png)

购物车列表查询的实现思路：

​	根据当前的登录人，在redis中获取即可

​	接口方法的返回值：不仅要返回商品的集合，还要返回商品的总数量，以及总价格，所以用map接收

​	map里面有购物车的列表数据，商品的总数，已经总价

service层代码实现：

```java
	/**
     * 购物车列表查询
     * @param username 登录的用户名
     * @return
     */
    @Override
    public Map list(String username) {
        List<OrderItem> orderItemList = redisTemplate.boundHashOps(CART + username).values();

        HashMap<String,Object> map = new HashMap<>();

        //将订单项列表存到map中
        map.put("itemList",orderItemList);

        //商品总数和商品总价
        Integer totalNum = 0;
        Integer totalPrice = 0;

        for (OrderItem orderItem : orderItemList) {
            totalNum += orderItem.getNum();
            totalPrice += orderItem.getPrice();
        }

        map.put("totalNum",totalNum);
        map.put("totalPrice",totalPrice);
        return map;
    }			
```

controller层代码实现：

```java
 @GetMapping("/list")
 public Map list(){
    String username = "itcast";  //暂时将username写死
    return cartService.list(username);
 }
```



### 3. 购物车渲染

![1576391549830](C:\Users\admin\AppData\Roaming\Typora\typora-user-images\1576391549830.png)

如上图所示,用户每次将商品加入购物车，或者点击购物车列表的时候，先经过订单购物车后端渲染服务，再通过feign调用购物车订单微服务来实现购物车的操作，例如：加入购物车、购物车列表。

当用户需要获取购物车列表的时候，会经过网关，网关将请求转发到订单购物车后端渲染服务，后端渲染服务会基于feign去调用订单微服务，来获取到购物车的相关数据，获取到购物车相关数据的时候又会基于thymeleaf完成购物车列表页的渲染，接着将渲染之后的页面返回给用户，完成结果的展示

### 3.1购物车渲染服务搭建

在changgou_web中搭建订单购物车微服务工程 changgou_web_order ，该工程主要实现购物车和订单的渲染操作。

(1) pom.xml依赖

```xml
<dependencies>
        <dependency>
            <groupId>com.changgou</groupId>
            <artifactId>changgou_service_order_api</artifactId>
            <version>1.0-SNAPSHOT</version>
        </dependency>

        <dependency>
            <groupId>com.changgou</groupId>
            <artifactId>changgou_common</artifactId>
            <version>1.0-SNAPSHOT</version>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-thymeleaf</artifactId>
        </dependency>
    </dependencies>
```

(2)application.yml配置

```yml
server:
  port: 9011
spring:
  application:
    name: order-web
  main:
    allow-bean-definition-overriding: true   #当遇到同样名字的时候，是否允许覆盖注册
  thymeleaf:
    cache: false
eureka:
  client:
    service-url:
      defaultZone: http://127.0.0.1:6868/eureka
  instance:
    prefer-ip-address: true
feign:
  hystrix:
    enabled: true
  client:
    config:
      default:   #配置全局的feign的调用超时时间  如果 有指定的服务配置 默认的配置不会生效
        connectTimeout: 60000 # 指定的是 消费者 连接服务提供者的连接超时时间 是否能连接  单位是毫秒
        readTimeout: 80000  # 指定的是调用服务提供者的 服务 的超时时间（）  单位是毫秒
#hystrix 配置
hystrix:
  command:
    default:
      execution:
        timeout:
          #如果enabled设置为false，则请求超时交给ribbon控制
          enabled: true
        isolation:
          strategy: SEMAPHORE
          thread:
            # 熔断器超时时间，默认：1000/毫秒
            timeoutInMilliseconds: 80000
#请求处理的超时时间
ribbon:
  ReadTimeout: 4000
  #请求连接的超时时间
  ConnectTimeout: 3000
```

(3)创建启动类

```java
@SpringBootApplication
@EnableEurekaClient   //声明它是一个Eureka的客户端
@EnableFeignClients(basePackages = {"com.changgou.order.feign"})  //开启feign的调用 指明feign接口所在的包
public class WebOrderApplication {
    public static void main(String[] args) {
        SpringApplication.run(WebOrderApplication.class, args);
    }
}
```

(4)静态资源拷贝

资源\成品页面\cart.html页面拷贝到工程中，如下图：

<img src="C:\Users\admin\AppData\Roaming\Typora\typora-user-images\1576419217268.png" alt="1576419217268" style="zoom:50%;" />



###3.2 购物车列表渲染
####3.2.1 Feign创建

在changgou_service_order_api中添加CartFeign接口，并在接口中创建添加购物车和查询购物车列表，代码如下：

```java
@FeignClient(name="order")  //FeignClient声明他是一个feignClient的接口  name指明它需要调用的服务名
public interface CartFeign {

    @GetMapping("/cart/addCart")
    public Result add(@RequestParam("skuId") String skuId, @RequestParam("num") Integer num);

    @GetMapping("/cart/list")
    public Map list();
}
```

#### 3.2.2 购物车渲染服务的controller

```java
@Controller
@RequestMapping("/wcart")
public class CartController {

    private CartFeign cartFeign;

    /**
     * 查询购物车，完成页面跳转
     * @param model
     * @return
     */
    @RequestMapping("/list")
    public String list(Model model){
        Map map = cartFeign.list();
        model.addAttribute("items",map);
        return "cart";
    }

    /**
     * 添加购物车购物车
     * @param skuId
     * @param num
     * @return
     */
    @RequestMapping("/add")
    @ResponseBody
    public Result<Map> add(String skuId, Integer num){
        cartFeign.add(skuId, num);

        //查询，更新
        Map map = cartFeign.list();

        return new Result(true, StatusCode.OK,"添加购物车成功",map);
    }
}
```

### 3.2.3 前端页面

(1)动态加载列表数据
我们可以先在网页下面添加一个js脚本，用于加载购物车数据，并用一个对象存储，代码如下：



很多服务在内部调用的时候都需要令牌，feign的拦截器不应该定义到某一个服务中，需要作为一个公共的资源

feign的拦截器存放的位置：谁发起feign的请求，feign的拦截器就放到谁身上

如果定义了feign的拦截器，那么在发起feign请求的时候就会进入feign的拦截器中，进行一系列的获取，多请求头进行了增强

微服务之间的认证就是在两个微服务之间也需要进行令牌的传递



传递过程中有JWT令牌，动态获取登录人的信息就是解析令牌，获取信息就好了



compilar +instull

