# 通用Mapper



## 1.通用mapper与SpringBoot集成

### 1.1SpringBoot中通用mapper的使用步骤

#### 1.1.1新建SpringBoot工程

```xml
<parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>2.2.1.RELEASE</version>
</parent>
```



#### 1.1.2导入依赖

```xml
<dependencies>
    <!--通用mapper起步依赖-->
    <dependency>
        <groupId>tk.mybatis</groupId>
        <artifactId>mapper-spring-boot-starter</artifactId>
        <version>2.0.4</version>
    </dependency>

    <!--web的启动器-->
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
    </dependency>

    <!--mysql连接-->
    <dependency>
        <groupId>mysql</groupId>
        <artifactId>mysql-connector-java</artifactId>
    </dependency>

    <!--mybatis分页插件-->
    <dependency>
        <groupId>com.github.pagehelper</groupId>
        <artifactId>pagehelper-spring-boot-starter</artifactId>
        <version>1.2.3</version>
    </dependency>
</dependencies>
```



#### 1.1.3 创建启动类

```java
@SpringBootApplication
@MapperScan(basePackages = "com.example.mapper")  //通用mapper的包扫描
public class TKApplication {
    public static void main(String[] args) {
        SpringApplication.run(TKApplication.class, args);
    }
}
```



#### 1.1.4添加配置文件

```yaml
server:
  #服务端口
  port: 80
spring:
  application:
    #服务名称
    name: goods
  #数据源的配置
  datasource:
    driver-class-name: com.mysql.jdbc.Driver
    url: jdbc:mysql://localhost:3306/changgou?useUnicode=true&characterEncoding=UTF-8&serverTimezone=UTC
    username: root
    password: root
```



#### 1.1.5编写POJO

返回结果实体类：

```java
package com.example.entity;

/**
 * 返回结果实体类
 */
public class Result<T> {

    private boolean flag;//是否成功
    private Integer code;//返回码
    private String message;//返回消息

    private T data;//返回数据

    //省略setter、getter和构造方法
}
```

实体类：

```java
/**
 * @program: tk_mapper->Brand
 * @description:
 * @author: hunyiha
 * @create: 2020-02-26 11:35
 **/

@Table(name="tb_brand")
public class Brand {

    @Id  
    private Integer id;
    private String name;
    private String image;
    private String letter;
    private String seq;

    @Transient
    private String username;
}
```

**注意**：

1. 表名默认使用类名,驼峰转下划线(只对大写字母进行处理),如`UserInfo`默认对应的表名为`user_info`
2. 表名可使用`@Table(name = "tableName")`进行指定,对不符合第一条默认规则的可以通过这种方式指定表名
3. 字段默认和`@Column`一样,都会作为表字段,表字段默认为Java对象的Field名字驼峰转下划线形式
4. 可以使用`@Column(name = "fieldName")`指定不符合第3条规则的字段名
5. 使用`@Transient`注解可以忽略字段,添加该注解的字段不会作为表字段使用.
6. 建议一定是有一个`@Id`注解作为主键的字段,可以有多个`@Id`注解的字段作为联合主键.
7. 如果是MySQL的自增字段，加上`@GeneratedValue(generator = "JDBC")`即可。如果是其他数据库，可以参考[官网文档](https://gitee.com/free/Mapper/blob/master/wiki/mapper3/3.Use.md#3主键策略仅用于insert方法)。



#### 1.1.6创建controller层

```java
/**
 * @program: tk_mapper->BrandController
 * @description: 品牌管理
 * @author: hunyiha
 * @create: 2020-02-26 11:40
 **/

@RestController
@RequestMapping("/brand")
public class BrandController {

    @Autowired
    private BrandService brandService;

    /**
     * 根据品牌id查询品牌数据
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Result findById(@PathVariable("id") Integer id){
        Brand brand = brandService.findById(id);
        return new Result(true,"200","查询成功",brand);
    }

    /**
     * 查询所有品牌
     * @return
     */
    @GetMapping("/all")
    public Result findAllBrand(){
        List<Brand> brandList = brandService.findAllBrand();
        return new Result(true,"200","查询成功",brandList);
    }

    /**
     * 添加品牌信息
     * @param brand
     * @return
     */
    @PutMapping("/add")
    public Result addBrand(@RequestBody Brand brand){
        brandService.addBrand(brand);
        return new Result(true,"200","添加成功");
    }

    /**
     * 根据id删除品牌
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public Result deleteById(@PathVariable("id") Integer id){
        brandService.deleteById(id);
        return new Result(true,"200","删除成功");
    }

    /**
     * 修改品牌信息
     * @param brand
     * @return
     */
    @PostMapping("/update")
    public Result updateBrand(@RequestBody Brand brand){
        brandService.updateBrand(brand);
        return new Result(true,"200","修改成功");
    }

    /**
     * 根据条件查询+分页查询
     * @param searchMap
     * @return
     */

    @GetMapping("search/{page}/{size}")
    public Result selectByConditions(Map<String,Object> searchMap,@PathVariable("page") Integer page,@PathVariable("size") Integer size){
        Page<Brand> brandList =  brandService.selectByConditions(searchMap,page,size);
        PageResult<Brand> pageResult = new PageResult<>(brandList.getTotal(),brandList.getResult());
        return new Result(true,"200","查询成功",pageResult);
    }

}

```



#### 1.1.7 创建service层

接口编写：

```java
public interface BrandService {
    Brand findById(Integer id);

    List<Brand> findAllBrand();

    void addBrand(Brand brand);

    void deleteById(Integer id);

    void updateBrand(Brand brand);

    Page<Brand> selectByConditions(Map<String, Object> searchMap, Integer page, Integer size);
}

```

实现类编写：

```java
/**
 * @program: tk_mapper->BrandServiceImpl
 * @description:
 * @author: hunyiha
 * @create: 2020-02-26 12:15
 **/

@Service
public class BrandServiceImpl implements BrandService {

    @Autowired
    private BrandMapper brandMapper;

    @Override
    public Brand findById(Integer id) {
        return brandMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<Brand> findAllBrand() {
        return brandMapper.selectAll();
    }

    @Override
    public void addBrand(Brand brand) {
        brandMapper.insertSelective(brand);
    }

    @Override
    public void deleteById(Integer id) {
        brandMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void updateBrand(Brand brand) {
        brandMapper.updateByPrimaryKeySelective(brand);
    }

    @Override
    public Page<Brand> selectByConditions(Map<String, Object> searchMap, Integer page, Integer size) {

        PageHelper.startPage(page,size);
        Example example = createExample(searchMap);
        Page<Brand> pageBrand = (Page<Brand>) brandMapper.selectByExample(example);
        return pageBrand;
    }


    private Example createExample(Map<String, Object> searchMap){

        Example example=new Example(Brand.class);

        Example.Criteria criteria = example.createCriteria();

        if(searchMap!=null){
            // 品牌名称
            if(searchMap.get("name")!=null && !"".equals(searchMap.get("name"))){
                criteria.andLike("name","%"+searchMap.get("name")+"%");
            }
            // 品牌图片地址
            if(searchMap.get("image")!=null && !"".equals(searchMap.get("image"))){
                criteria.andLike("image","%"+searchMap.get("image")+"%");
            }
            // 品牌的首字母
            if(searchMap.get("letter")!=null && !"".equals(searchMap.get("letter"))){
                criteria.andLike("letter","%"+searchMap.get("letter")+"%");
            }

            // 品牌id
            if(searchMap.get("id")!=null ){
                criteria.andEqualTo("id",searchMap.get("id"));
            }
            // 排序
            if(searchMap.get("seq")!=null ){
                criteria.andEqualTo("seq",searchMap.get("seq"));
            }

        }
        return example;
    }

}

```



#### 1.1.8创建mapper层

```java
public interface BrandMapper extends Mapper<Brand> {

}
```



## 2. 通用mapper自定义方法











### 1.2 常见问题

1. 使用通用mapper tk.mybatis时 实体类有字段,但是数据库没有,咋办？

```
在实体类的字段上标注@Transient
```



 https://blog.csdn.net/mashaokang1314/article/details/91867235?depth_1-utm_source=distribute.pc_relevant.none-task&utm_source=distribute.pc_relevant.none-task 



 https://blog.csdn.net/li627528647/article/details/77915661?depth_1-utm_source=distribute.pc_relevant.none-task&utm_source=distribute.pc_relevant.none-task 





文档参考：

 https://juejin.im/entry/5a3b482cf265da431281152b 