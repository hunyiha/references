# MySQL数据库

## MySQL数据库基础

### 数据库的基本概念

数据库的英文单词： DataBase 简称 ： DB

#### 什么是数据库

* 用于存储和管理数据的仓库。

#### 什么是MySQL服务器

- 一台计算机上安装了MySQL软件(即Mysql服务，服务就是没有界面的软件)，此时这台电脑就可以称为MySQL的服务器了，MySQL服务器=硬件+软件

#### 数据库的特点

- **持久化存储**数据的。其实数据库就是一个文件系统

- 方便**存储和管理数据**

- 使用了统一的方式操作数据库 -- **SQL**

### MySQL数据库软件

学一个软件就要学安装、卸载、配置

#### Mysql的安装

- 参见《MySQL基础.pdf》

#### Mysql的卸载

- 1.去mysql的安装目录找到my.ini文件，复制 datadir="..."
  - datadir的值是mysql的数据文件夹路径，是一个隐藏文件,所以先要复制，以免卸载后找不到
- 2.卸载MySQL
  - 和卸载普通软件卸载一致
- 3.删除C:/ProgramData目录下的MySQL文件夹
  - 删除的MySQL的数据文件，如果不删除，MySQL是没有卸载干净的，无法再次安装

#### MySQL的配置

上面安装的就是MySQL的服务，所谓服务就是没有界面的应用程序

##### MySQL服务的启动

- 1.手动启动

- 2.cmd--> services.msc 打开服务的窗口,然后启动服务

- 3.**管理员**打开cmd，使用windows的命令去启动服务

  - net  start mysql：启动mysql的服务(MySQL的服务名一定要是mysql才能这样)

  - net  stop mysql:   关闭mysql服务 

    使用上述命令的时候MySQL的服务名一定要是mysql，否者也无法启动，需要去查看MySQL的服务名

##### MySQL的登录

- 1.mysql -uroot -p密码(这是默认连接本地MySQL)
- 2.mysql -hip -uroot -p连接目标的密码
- 3.mysql --host=ip --user=root --password=连接目标的密码

##### MySQL退出

- 1.exit
- 2.quit

##### MySQL目录结构

MySQL的目录分为两部分，**安装目录**和**数据目录**

- 1.MySQL安装目录：basedir="D:/develop/MySQL/"

  - bin文件夹：二进制的可执行文件，在安装MySQL的时候我们将bin目录是配置到了环境变量中，所以我们在cmd下可以直接使用mysql命令

  - data目录：这是数据目录，里面放了一些日志文件、数据文件
  - include目录：里面存放了一些C语言的头信息
  - lib目录：MySQL运行需要的一些文件，相当于jar包
  - share目录：MySQL的一些错误的信息
  - my.ini：MySQL的配置文件
    -  datadir：MySQL的安装目录
    -  basedir:  MySQL的安装目录
    - default-storage-engine：默认存储引擎

- 2.MySQL数据目录：

  - 可以在my.ini配置文件中查找数据目录：datadir

MySQL中的概念和文件的对应关系

- **数据库**对应电脑上的**文件夹**
- **表**对应数据库文件夹里面的**文件**（.frm文件）
- **数据**对应**数据**



## SQL

### SQL的概念

SQL是Structured Query Language的简称，即结构化查询语言

其实就是定义了**操作所有关系型数据库的规则**。每一种数据库操作的方式存在不一样的地方，称为“**方言**”。

### SQL通用语法

- 1.SQL 语句可以单行或多行书写，以**分号结尾**。
- 2.可使用空格和缩进来增强语句的可读性。
- 3.MySQL 数据库的 SQL 语句**不区分大小写**，关键字建议使用大写。
- 4.三种注释
  - 单行注释:  --  注释内容(中间有一个空格) 或 **# 注释内容(mysql 特有)** 
  - 多行注释:  /* 注释 */

### SQL分类

DDL(Data Definition Language)数据定义语言
		用来定义数据库对象：**数据库，表，列**等。关键字：create, drop,alter 等

DML(Data Manipulation Language)数据操作语言
		用来对数据库中**表的数据**进行**增删改**。关键字：insert, delete, update 等

DQL(Data Query Language)数据查询语言
		用来**查询**数据库中**表的记录**(数据)。关键字：select, where 等

DCL(Data Control Language)数据控制语言(了解)
		用来定义**数据库**的访问权限和安全级别，及创建用户。关键字：GRANT， REVOKE 等

### DDL数据定义语言

#### 操作数据库

1.创建数据库: create

```sql
-- 创建数据库：
	  create database 数据库名称;
-- 创建数据库，判断不存在，再创建：
	  create database if not exists 数据库名称;
-- 创建数据库，并指定字符集
	  create database 数据库名称 character set 字符集名;
--练习:创建db4数据库，判断是否存在，并制定字符集为gbk
      create database if not exists db4 character set gbk;
```

2.查询数据库: show

```sql
-- 查询所有数据库的名称:
       show databases;
-- 查询某个数据库的字符集:查询某个数据库的创建语句
       show create database 数据库名称;
```

3.修改数据库

```sql
-- 修改数据库的字符集
	  alter database 数据库名称 character set 字符集名称;
```

4.删除数据库

```sql
-- 删除数据库
	  drop database 数据库名称;
-- 判断数据库存在，存在再删除
	  drop database if exists 数据库名称;
```

5.使用数据库

```sql
-- 查询当前正在使用的数据库名称
	  select database();
-- 使用数据库
	  user 数据库名称;   
```

#### 操作表     

1.创建表

```sql
-- 创建表语法   最后一列，不需要加逗号（,）
       create table 表名(
           列名1 数据类型1,
           列名2 数据类型2,
           ....
           列名n 数据类型n
       );

-- 复制表
	  create table 表名 like 被复制的表名;	 

-- 创建表的实例
	  create table student(
      	  id int,
          name varchar(32),
          age int ,
          score double(4,1),
          birthday date,
          insert_time timestamp
      );
```

数据库中的数据类型

```
1. int：整数类型
		age int,
2. double:小数类型
		score double(5,2)
3. date:日期，只包含年月日，yyyy-MM-dd
4. datetime:日期，包含年月日时分秒	 yyyy-MM-dd HH:mm:ss
5. timestamp:时间错类型	包含年月日时分秒	 yyyy-MM-dd HH:mm:ss	
		如果将来不给这个字段赋值，或赋值为null，则默认使用当前的系统时间，来自动赋值
6. varchar：字符串
		name varchar(20):姓名最大20个字符
		zhangsan 8个字符  张三 2个字符
```

2.查询表

```sql
-- 查询某个数据库中所有的表名称
	   show tables;
-- 查询表结构
	   desc 表名;
```

3.修改表

```sql
-- 修改表名
	    alter table 表名 rename to 新的表名;
-- 修改表的字符集
		alter table 表名 character set 字符集名称;
-- 添加一列
		alter table 表名 add 列名 数据类型;
-- 修改列名称 类型
		alter table 表 名 change 列名 新列别 新数据类型;
		alter table 表名 modify 列名 新数据类型;
-- 删除列
		alter table 表名 drop 列名;
```

4.删除表

```sql
-- 删除表
		drop table 表名;
-- 如果表存在就删除表
		drop table  if exists 表名 ;
```

### DML数据操作语言

1.添加数据

```sql
-- 添加数据语法
insert into 表名(列名1,列名2,...列名n) values(值1,值2,...值n);

/*
	1. 列名和值要一一对应。
	2. 如果表名后，不定义列名，则默认给所有列添加值
		  insert into 表名 values(值1,值2,...值n);
	3. 除了数字类型，其他类型需要使用引号(单双都可以)引起来
*/
```

2.删除数据

```sql
-- 删除数据语法
		delete from 表名 [where 条件]
/*
	1. 如果不加条件，则删除表中所有记录。
	2. 如果要删除所有记录
			1. delete from 表名; -- 不推荐使用。有多少条记录就会执行多少次删除操作
			2. TRUNCATE TABLE 表名; -- 推荐使用，效率更高 先删除表，然后再创建一张一样的表。
*/
```

3.修改数据

```sql
-- 修改数据语法
		update 表名 set 列名1 = 值1, 列名2 = 值2,... [where 条件];

/*
	1. 如果不加任何条件，则会将表中所有记录全部修改。
*/
```

### DQL数据查询语言

数据查询语句的语法		

```sql
-- 基础查询
	select id,name from 表名;

-- 语法
	select
			字段列表
	from
			表名列表
	where
			条件列表
	group by
			分组字段
	having
			分组之后的条件
	order by
			排序
	limit
			分页限定
```

#### 基础查询

```sql
-- 1.多个字段的查询
		select 字段名1，字段名2... from 表名；
		-- 注意：如果查询所有字段，则可以使用*来替代字段列表。
-- 2.去除重复结果集：特别说明，是查询出来以后的结果集是否一样，而不是原数据是否一样
		select distinct address from 表名
-- 3.计算列
		select math,english,math+english total from student;
		/*
			一般可以使用四则运算计算一些列的值。（一般只会进行数值型的计算）
		    ifnull(表达式1,表达式2)：null参与的运算，计算结果都为null
			     表达式1：哪个字段需要判断是否为null
			     如果该字段为null后的替换值。
		*/
-- 4.起别名：
		as： -- as也可以省略
```

#### 条件查询

```
    1. where子句后跟条件
	2. 运算符
		> 、< 、<= 、>= 、= 、<> ,!=
		BETWEEN...AND  
		IN( 集合) 
		LIKE：模糊查询
			占位符：
				 _:单个任意字符
			     %：多个任意字符
		IS NULL  
		and  或 &&
		or  或 || 
		not  或 !
```

基础和条件查询小练习

```sql
-- 数学分大于80分的学生
select * from student where math>80;

-- 查询 english 分数小于或等于 80 分的学生
select * from student where english <= 80;

-- 查询 age 等于 20 岁的学生
select * from student where age>20;

-- 查询 age 不等于 20 岁的学生
-- 写法1
select * from student where age != 20;

-- 写法2
select * from student where age>20 or age <20;

-- 写法3
select * from student where age <> 20;

-- 查询 age 大于 35 且性别为男的学生(两个条件同时满足)
select * from student where  age > 35 and sex = '男';

-- 查询 age 大于 35 或性别为男的学生(两个条件其中一个满足)
select * from student where age>35 or sex='男';

-- 查询 id 是 1 或 3 或 5 的学生 方法1
select * from student where id = 1 or id = 3 or id = 5;

-- 查询 id 是 1 或 3 或 5 的学生 方法2
select * from student where id in(1,3,5);

-- 查询 id 不是 1 或 3 或 5 的学生
select * from student where id not in(1,3,5);

-- 查询英语成绩为null
SELECT * FROM student WHERE english = NULL; -- 不对的。null值不能使用 = （!=） 判断

SELECT * FROM student WHERE english IS NULL;

-- 查询英语成绩不为null
SELECT * FROM student WHERE english  IS NOT NULL;

-- 查询年龄在30-40之间的学生
select * from student where age between 10 and 40;

-- 查询 english 成绩大于等于 75，且小于等于 90 的学生
select * from student where english between 75 and 90;

-- 查询姓马的学生
select * from student where name like '马%';

-- 查询姓名中包含'德'字的学生
select * from student where name like '%德%';

-- 查询姓马，且姓名有两个字的学生
select * from student where name like '马_';
```

#### 排序查询

```sql
-- 语法  order by 子句  排序可以加多个字段
		order by 排序字段1 排序方式1 ，  排序字段2 排序方式2...
		
-- 示例
		SELECT * FROM student3 ORDER BY math DESC;
		SELECT * FROM student3 ORDER BY math,english ASC;

/*
	排序方式：
		* ASC：升序，默认的。
		* DESC：降序
`	如果有多个排序条件，则当前边的条件值一样时，才会判断第二条件。
*/
```

#### 聚合函数

聚合函数：将**一列**数据作为一个整体，进行**纵向**的计算，聚合函数排除null

```sql
 聚合函数：将一列数据作为一个整体，进行纵向的计算。
	1. count：计算个数  排除null
		1. 一般选择非空的列：主键
		2. count(*)  公司中不使用这个，使用主键
	2. max：计算最大值
	3. min：计算最小值  排除null
	4. sum：计算和
	5. avg：计算平均值
	
 * 注意：聚合函数的计算，排除null值。
		解决方案：
			1. 选择不包含非空的列进行计算
			2. IFNULL函数
```

#### 分组查询

统计一个具有相同特征的某一类数据，将这些数据当做一个整体来看，看整体的一些信息

```mysql
-- 语法
	 group by 分组字段；

/*
	1. 分组之后查询的字段：分组字段、聚合函数
		2. where 和 having 的区别？
			1. where 在分组之前进行限定，如果不满足条件，则不参与分组。having在分组之后进行限定，如果不满足结果，则不会被查询出来
			2. where 后不可以跟聚合函数，having可以进行聚合函数的判断。
*/
```

分组查询的小练习

```mysql 
-- 按照性别分组。分别查询男、女同学的平均分

		SELECT sex , AVG(math) FROM student GROUP BY sex;
		
-- 按照性别分组。分别查询男、女同学的平均分,人数
		
		SELECT sex , AVG(math),COUNT(id) FROM student GROUP BY sex;
		
--  按照性别分组。分别查询男、女同学的平均分,人数 要求：分数低于70分的人，不参与分组
		SELECT sex , AVG(math),COUNT(id) FROM student WHERE math > 70 GROUP BY sex;
		
--  按照性别分组。分别查询男、女同学的平均分,人数 要求：分数低于70分的人，不参与分组,分组之后。人数要大于2个人
		SELECT sex , AVG(math),COUNT(id) FROM student WHERE math > 70 GROUP BY sex HAVING COUNT(id) > 2;
		
		SELECT sex , AVG(math),COUNT(id) 人数 FROM student WHERE math > 70 GROUP BY sex HAVING 人数 > 2;
```

#### 分页查询

```sql
-- 语法
	limit 开始的索引,每页查询的条数;
-- 公式
	开始的索引 = （当前的页码 - 1） * 每页显示的条数
-- limit 是一个MySQL"方言"
```



## 约束

对表中的数据进行限定，保证数据的正确性、有效性和完整性。	

### 约束分类

- 主键约束：primary key

- 非空约束：not null

- 唯一约束：unique

- 外键约束：foreign key

### 非空约束

not null，某一列的值不能为null

#### 创建表时添加约束

```sql
CREATE TABLE stu(
	id INT,
	NAME VARCHAR(20) NOT NULL -- name为非空
);
```

#### 创建表完后，添加非空约束

```sql
ALTER TABLE stu MODIFY NAME VARCHAR(20) NOT NULL;
```

#### 删除name的非空约束

```sql
ALTER TABLE stu MODIFY NAME VARCHAR(20);
```

### 唯一约束

unique，某一列的值不能重复,唯一约束可以有NULL值，null是没有数据不存在重复

#### 在创建表时，添加唯一约束

```sql
CREATE TABLE stu(
    id INT,
    phone_number VARCHAR(20) UNIQUE -- 手机号
);
```

#### 在表创建完后，添加唯一约束

```sql
ALTER TABLE stu MODIFY phone_number VARCHAR(20) UNIQUE;
```

#### 删除唯一约束

```sql
ALTER TABLE stu DROP INDEX phone_number;
```

### 主键约束

#### primary key的注意事项

1. 非空且唯一
2. 一张表只能有一个字段为主键
3. 主键就是表中记录的唯一标识

#### 在创建表时，添加主键约束

```sql
create table stu(
    id int primary key,-- 给id添加主键约束
    name varchar(20)
);
```

#### 创建完表后，添加主键

```sql
ALTER TABLE stu MODIFY id INT PRIMARY KEY;
```

#### 删除主键

```sql
-- 错误 alter table stu modify id int ;
	  ALTER TABLE stu DROP PRIMARY KEY;
-- 删除主键约束后会保留非空约束
```

#### 自动增长

```sql
-- 如果某一列是数值类型的，使用 auto_increment 可以来完成值得自动增长,如果是自增长,删除主键的时候需要先删除自增长
```

#### 在创建表时，添加主键约束，并且完成主键自增长

```sql
create table stu(
    id int primary key auto_increment,-- 给id添加主键约束
    name varchar(20)
);
```

#### 删除自动增长

```sql
ALTER TABLE stu MODIFY id INT;
```

#### 添加自动增长

```sql
ALTER TABLE stu MODIFY id INT AUTO_INCREMENT;
```

### 外键约束

foreign key,让表与表产生关系，从而保证数据的正确性

#### 在创建表时，可以添加外键

```sql
create table 表名(
    ....
    外键列
    constraint 外键名称 foreign key (外键列名称) references 主表名称(主表列名称)
);
```

####  创建表之后，添加外键

```sql
ALTER TABLE 表名 ADD CONSTRAINT 外键名称 FOREIGN KEY (外键字段名称) REFERENCES 主表名称(主表列名称);
```

#### 删除外键

```sql
ALTER TABLE 表名 DROP FOREIGN KEY 外键名称;
```

#### 添加级联操作

```sql
ALTER TABLE 表名 ADD CONSTRAINT 外键名称 FOREIGN KEY (外键字段名称) REFERENCES 主表名称(主表列名称) ON UPDATE CASCADE ON DELETE CASCADE  ;
```

#### 级联操作分类

- 级联更新：ON UPDATE CASCADE 

- 级联删除：ON DELETE CASCADE 

#### 级联操作的扩展

```
update 则是主键表中被参考字段的值更新，delete是指在主键表中删除一条记录.
on update和on delete后面可以跟的词语有四个,分别是:
	no action,set null,set default,cascade
no action:  
	表示不做任何操作，
set null: 
	表示在外键表中将相应字段设置为null
set default:  
	表示设置为默认值(restrict)
cascade: 
    表示级联操作，就是说，如果主键表中被参考字段更新，外键表中也更新，主键表中的
    记录被删除，外键表中改行也相应删除
```



## 数据库的设计

### 多表之间的关系分类

#### 一对一的关系

```
如：人和身份证 
分析：一个人只有一个身份证，一个身份证只能对应一个人
```

#### 一对多的关系

```
如：部门和员工
分析：一个部门有多个员工，一个员工只能对应一个部门
```

#### 多对多的关系

```
如：学生和课程
分析：一个学生可以选择很多门课程，一个课程也可以被很多学生选择
```

### 多表之间的关系实现

#### 一对多的关系实现

```
如：部门和员工
实现方式：在多的一方建立外键，指向一的一方的主键。
```

#### 多对多的关系实现

```
如：学生和课程
实现方式：多对多关系实现需要借助第三张中间表。中间表至少包含两个字段，这两个字段作为第三张表的外键，分别指向两张表的主键
```

#### 一对一的关系实现

```
如：人和身份证
实现方式：一对一关系实现，可以在任意一方添加唯一外键指向另一方的主键。实际开发中之间建立一张表
```

### 数据库设计的范式

设计数据库时，需要遵循的一些**规范**。要遵循后边的范式要求，必须先遵循前边的所有范式要求

设计关系数据库时，遵从不同的规范要求，设计出合理的关系型数据库，这些不同的规范要求被称为不同的范式，各种范式呈递次规范，越高的范式数据库冗余越小

目前关系数据库有六种范式：第一范式（1NF）、第二范式（2NF）、第三范式（3NF）、巴斯-科德范式（BCNF）、第四范式(4NF）和第五范式（5NF，又称完美范式）

每一种规范有不同的要求

#### 数据库设计的范式的分类

```
1. 第一范式（1NF）：每一列都是不可分割的原子数据项
2. 第二范式（2NF）：在1NF的基础上，非码属性必须完全依赖于码（在1NF基础上消除非主属性对主码的部分函数依赖）
	 几个概念：
     1. 函数依赖：A-->B,如果通过A属性(属性组)的值，可以确定唯一B属性的值。则称B依赖于A（sfz）
			例如：学号-->姓名。  （学号，课程名称） --> 分数
     2. 完全函数依赖：A-->B， 如果A是一个属性组，则B属性值得确定需要依赖于A属性组中所有的属性值。（zb）
			例如：（学号，课程名称） --> 分数
	3. 部分函数依赖：A-->B， 如果A是一个属性组，则B属性值得确定只需要依赖于A属性组中某一些值即可。
			例如：（学号，课程名称） -- > 姓名
	4. 传递函数依赖：A-->B, B -- >C . 如果通过A属性(属性组)的值，可以确定唯一B属性的值，在通过B属性（属性组）的值可以确定唯一C属性的值，
	       则称 C 传递函数依赖于A
					例如：学号-->系名，系名-->系主任
	5. 码：如果在一张表中，一个属性或属性组，被其他所有属性所完全依赖，则称这个属性(属性组)为该表的码
			例如：该表中码为：（学号，课程名称）
				 主属性：码属性组中的所有属性
				 非主属性：除过码属性组的属性
3. 第三范式（3NF）：在2NF基础上，任何非主属性不依赖于其它非主属性（在2NF基础上消除传递依赖）
```

### 数据库的备份和还原

命令行还原和备份

```sql
-- 备份
		mysqldump -u用户名 -p密码 数据库名称 > 保存的路径
-- 还原
		1. 登录数据库
		2. 创建数据库
		3. 使用数据库
		4. 执行文件。source 文件路径
```

可视化工具还原和备份



## 多表查询

### 多表查询的分类

 内连接查询

- 隐式内连接
- 显式内连接

外链接查询

- 左外连接
- 右外连接

子查询

### 隐式内连接

```
使用where条件消除无用数据
```

### 显式内连接

```sql
select 字段列表 from 表名1 [inner] join 表名2 on 条件
```

### 内连接查询注意事项

三个明确

- 明确从哪些表中查询数据
- 明确条件是什么
- 明确查询哪些字段

### 左外连接

```sql
select 字段列表 from 表1 left [outer] join 表2 on 条件；
```

查询的是左表所有数据以及其交集部分。

### 右外连接

```sql
select 字段列表 from 表1 right [outer] join 表2 on 条件；
```

查询的是右表所有数据以及其交集部分

### 子查询

查询中嵌套查询，称嵌套查询为子查询。即将一个sql的查询结果作为一张虚拟表去进行查询

#### 子查询的结果是单行单列

```
子查询可以作为条件，使用运算符去判断。 运算符： > >= < <= =
```

#### 子查询的结果是多行单列

```
子查询可以作为条件，使用运算符in来判断
```

#### 子查询的结果是多行多列的

```
 子查询可以作为一张虚拟表参与查询
```



## 事务

### 事务的概念

如果一个包含多个步骤的业务操作，被事务管理，那么这些操作要么同时成功，要么同时失败。

### 事务的操作

#### 开启事务

```sql
start transaction;
```

#### 回滚事务

```
rollback;
```

#### 提交事务

```sql
commit;
```

### MySQL数据库事务提交方式

MySQL数据库中事务默认自动提交

#### 事务提交的两种方式

自动提交

```
mysql就是自动提交的
一条DML(增删改)语句会自动提交一次事务。
```

手动提交

```
Oracle 数据库默认是手动提交事务
需要先开启事务，再提交
```

#### 查看事务的默认提交方式

```sql
SELECT @@autocommit; -- 1 代表自动提交  0 代表手动提交
```

#### 修改事务的默认提交方式

```sql
set @@autocommit = 0;
```

### 事务的四大特征

1. 原子性：是不可分割的最小操作单位，要么同时成功，要么同时失败。
2. 持久性：当事务提交或回滚后，数据库会持久化的保存数据。
3. 隔离性：多个事务之间。相互独立。
4. 一致性：事务操作前后，数据总量不变

### 事务的隔离级别

事务：如果一个包含多个步骤的业务操作，被事务管理，那么这些操作要么同时成功，要么同时失败。

事务的隔离级别：多个事务之间隔离的，相互独立的。但是如果**多个事务操作同一批数据**，则会引发一些问题，设置不同的隔离级别就可以解决这些问题。

#### 多个事务操作同一批数据会出现的问题

1. 脏读：一个事务，读取到另一个事务中没有提交的数据
2. 不可重复读(虚读)：在同一个事务中，两次读取到的数据不一样。
3. 幻读：一个事务操作(DML)数据表中所有记录，另一个事务添加了一条数据，则第一个事务查询不到自己的修改。

#### 事务的隔离级别

```
1. read uncommitted：读未提交
	  产生的问题：脏读、不可重复读、幻读
2. read committed：读已提交 （Oracle）
      产生的问题：不可重复读、幻读
3. repeatable read：可重复读 （MySQL默认）
      产生的问题：幻读
4. serializable：串行化
      可以解决所有的问题
```

 注意：隔离级别从小到大安全性越来越高，但是效率越来越低

##### 数据库查询隔离级别

```sql
 select @@tx_isolation;
```

##### 数据库设置隔离级别

```sql
set global transaction isolation level  级别字符串;
```



## DCL数据控制语言

### SQL分类

- DDL：操作数据库和表
- DML：增删改表中数据 
- DQL：查询表中数据
- DCL：管理用户，授权

### 管理用户

查询用户

```mysql
-- 1. 切换到mysql数据库
		USE myql;
-- 2. 查询user表
		SELECT * FROM USER;
			
-- 通配符： % 表示可以在任意主机使用用户登录数据库
```

添加用户

```mysql
CREATE USER '用户名'@'主机名' IDENTIFIED BY '密码';
```

删除用户

```mysql
DROP USER '用户名'@'主机名';
```

修改用户密码

```mysql
UPDATE USER SET PASSWORD = PASSWORD('新密码') WHERE USER = '用户名';
UPDATE USER SET PASSWORD = PASSWORD('abc') WHERE USER = 'lisi';
	
-- 一种简单的方式
SET PASSWORD FOR '用户名'@'主机名' = PASSWORD('新密码');
SET PASSWORD FOR 'root'@'localhost' = PASSWORD('123');
```

mysql中忘记了root用户的密码

```
1. cmd -- > net stop mysql 停止mysql服务
			需要管理员运行该cmd
2. 使用无验证方式启动mysql服务： mysqld --skip-grant-tables
3. 打开新的cmd窗口,直接输入mysql命令，敲回车。就可以登录成功
4. use mysql;
5. update user set password = password('你的新密码') where user = 'root';
6. 关闭两个窗口
7. 打开任务管理器，手动结束mysqld.exe 的进程
8. 启动mysql服务
9. 使用新密码登录。
```

### 权限管理

查询权限

```mysql
SHOW GRANTS FOR '用户名'@'主机名';
SHOW GRANTS FOR 'lisi'@'%';
```

授予权限

```mysql
-- 授予权限
		grant 权限列表 on 数据库名.表名 to '用户名'@'主机名';
-- 给张三用户授予所有权限，在任意数据库任意表上
		GRANT ALL ON *.* TO 'zhangsan'@'localhost';
```

撤销权限

```mysql
-- 撤销权限：
	    revoke 权限列表 on 数据库名.表名 from '用户名'@'主机名';
     	REVOKE UPDATE ON db3.`account` FROM 'lisi'@'%';
```



## JDBC

### JDBC的概念

Java DataBase Connectivity  Java 数据库连接， Java语言操作数据库

 JDBC本质：其实是官方（sun公司）定义的一套操作所有关系型数据库的规则，即接口。各个数据库厂商去实现这套接口，提供数据库驱动jar包。我们可以使用这套接口（JDBC）编程，真正执行的代码是驱动jar包中的实现类。

### JDBC使用步骤

```
1. 导入驱动jar包 mysql-connector-java-5.1.37-bin.jar
	1.复制mysql-connector-java-5.1.37-bin.jar到项目的libs目录下
    2.右键-->Add As Library(这个操作才是将jar包加入到工作空间中去)
2. 注册驱动
3. 获取数据库连接对象 Connection
4. 定义sql
5. 获取执行sql语句的对象 Statement
6. 执行sql，接受返回结果
7. 处理结果
8. 释放资源
```

代码示例

```java
//1.导包
//2.加载驱动
Class.forName("com.mysql.jdbc.Driver");  //将驱动加载进内存
//3.获取连接
Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/account", "root", "root");
//4.获取sql的执行对象
Statement stat = conn.createStatement();
//5，定义sql
String sql = "update account set money = 500 where id = 1";
//6.执行sql
int count = stat.executeUpdate(sql);
//7.处理结果
System.out.println(count);
//8.释放资源
stat.close();
conn.close();
```



### JDBC中的对象解析

#### DriverManager

驱动管理对象，是一个类

```java
//2.加载驱动
Class.forName("com.mysql.jdbc.Driver");  //将驱动加载进内存
```

使用Class.forName(类的全路径名)将类加载到内存。使用当前类的类加载器加载该类,返回与给定字符串名称的类或接口相关联的 Class对象。 

com.mysql.jdbc.Driver这个类里面有一个静态代码块，在加载类进内存的时候同时会执行静态代码块，静态代码块执行了注册驱动

```java
static {
    try {
        DriverManager.registerDriver(new Driver());  //创建com.mysql.jdbc.Driver对象，注册驱动
    } catch (SQLException var1) {
        throw new RuntimeException("Can't register driver!");
    }
}
```

mysql5之后的驱动jar包可以省略注册驱动的步骤

获取数据库连接的方法：

```java
static Connection getConnection(String url, String user, String password)
```

参数：

- url：指定连接的路径
  - 语法：jdbc:mysql://ip地址(域名):端口号/数据库名称
  - 例子：jdbc:mysql://localhost:3306/db3
  - 细节：如果连接的是本机mysql服务器，并且mysql服务默认端口是3306，则url可以简写为：jdbc:mysql:///数据库名称
- user：用户名
- password：密码 

### Connection对象

数据库连接对象，**接口**

#### Connection对象的功能

1.获取执行sql 的对象

| 方法                                             | 说明                    |
| ------------------------------------------------ | ----------------------- |
| Statement    createStatement()                   | 创建SQL的执行语句对象   |
| PreparedStatement   prepareStatement(String sql) | 获取SQL的预执行语句对象 |

2.管理事务

| 方法                              | 说明                                            |
| --------------------------------- | ----------------------------------------------- |
| setAutoCommit(boolean autoCommit) | 开启事务，调用该方法设置参数为false，即开启事务 |
| commit()                          | 提交事务                                        |
| rollback()                        | 回滚事务                                        |

### Statement对象

执行SQL的对象，**接口**

Statement类中的方法：

| 方法                               | 说明                                                         |
| ---------------------------------- | ------------------------------------------------------------ |
| boolean execute(String sql)        | 可以执行任意的sql ，了解                                     |
| int executeUpdate(String sql)      | 执行DML（insert、update、delete）语句、DDL(create，alter、drop)语句 |
| ResultSet executeQuery(String sql) | 执行DQL（select)语句                                         |

第二个方法的返回值：影响的行数，可以通过这个影响的行数判断DML语句是否执行成功 返回值>0的则执行成功，反之，则失败。

### ResultSet对象

结果集对象,封装查询结果，**接口**

ResultSet类中的方法

| 方法           | 说明                                                         |
| -------------- | ------------------------------------------------------------ |
| boolean next() | 游标向下移动一行，判断当前行是否是最后一行末尾(是否有数据)，如果是，则返回false，如果不是则返回true |
| getXxx(参数)   | 获取数据，例如：int getInt() ,String getString()。           |

方法二的参数

- int：代表列的编号,从1开始   如： getString(1)
- String：代表列名称。 如： getDouble("balance")

### PreparedStatement对象

执行sql的对象，**预编译**SQL对象，**接口**，继承Statement接口

### SQL注入

#### 出现SQL注入的原因

在拼接sql时，有一些sql的特殊关键字参与字符串的拼接。会造成安全性问题

1. 输入用户随便，输入密码：a' or 'a' = 'a
2. sql：select * from user where username = 'fhdsjkf' and password = 'a' or 'a' = 'a' 会查询出所有的数据

解决sql注入问题：使用PreparedStatement对象来解决

#### SQL注入的解决方案

使用PreparedStatement对象来解决

#### 预编译的SQL

Statement这个对象执行的是静态的SQL，它在执行的时候所有的SQL都是拼接好了的，静态SQL容易出现SQL注入的问题，而预编译SQL则不会

预编译SQL参数使用?作为占位符,在执行SQL的时候赋值就行

后期使用**PreparedStatement**对象来完成增删改查的所有操作

- 可以防止SQL注入
- 效率更高



## 数据库连接池

### 数据库连接池

其实就是一个**容器**(集合)，存放数据库连接的容器。当系统初始化好后，容器被创建，容器中会申请一些连接对象，当用户来访问数据库时，从容器中获取连接对象，用户访问完之后，会将连接对象归还给容器。

### 数据库连接池的优点

- 节约资源

- 用户访问高效

### 数据库连接池的实现

数据库连接池的标准接口：DataSource   javax.sql包下

转准接口中的方法：

| 方法            | 说明     |
| --------------- | -------- |
| getConnection() | 获取连接 |
| close()         | 归还连接 |

数据库连接池我们一般不去实现它，Sun公司也没去实现，只是提供了一个标准接口，由数据库厂商或者其它公司来实现

### 常见的数据库连接池实现技术

- C3P0  数据库连接池技术
- Druid 数据库连接池技术，由阿里巴巴提供实现

### C3P0数据库连接池技术

C3P0使用步骤
- 1.导入jar包 (两个)    c3p0-0.9.5.2.jar mchange-commons-java-0.2.12.jar (C3P0的依赖jar包）
  - 不要忘记导入数据库驱动jar包

- 2.定义配置文件：
  - 名称： c3p0.properties 或者 c3p0-config.xml，C3P0在执行的时候会在类路径下去查找这两个名字的配置文件，位置和名称不能改
  - 路径：直接将文件放在src目录下即可

- 3.创建核心对象 数据库连接池对象 ComboPooledDataSource（连接参数可以通过硬编码和配置文件两种方式，不推荐硬编码，因为维护麻烦）
- 4.获取连接： getConnection

C3P0的配置文件

```xml
  <default-config>
        <!--  连接参数 -->
        <property name="driverClass">com.mysql.jdbc.Driver</property>
        <property name="jdbcUrl">jdbc:mysql://localhost:3306/db4</property>
        <property name="user">root</property>
        <property name="password">root</property>

        <!-- 连接池参数 -->
        <!--初始化申请的连接数量-->
        <property name="initialPoolSize">5</property>
        <!--最大的连接数量-->
        <property name="maxPoolSize">10</property>
        <!--超时时间-->
        <property name="checkoutTimeout">3000</property>
  </default-config>

  <named-config name="otherc3p0"> 
        <!--  连接参数 -->
        <property name="driverClass">com.mysql.jdbc.Driver</property>
        <property name="jdbcUrl">jdbc:mysql://localhost:3306/db3</property>
        <property name="user">root</property>
        <property name="password">root</property>

        <!-- 连接池参数 -->
        <property name="initialPoolSize">5</property>
        <property name="maxPoolSize">8</property>
        <property name="checkoutTimeout">1000</property>
  </named-config>
```

初始化连接数：数据库连接池初始化的时候创建的连接数

最大连接数：当初始化的连接数满了之后，还能够继续申请（最大连接-初始化连接）个连接

超时时间：当获取连接的时候，最大等待的时间

上面配置了两个C3P0的连接参数，default-config表示默认的配置，当不想使用默认配置的时候可以new ComboPooledDataSource("otherc3p0")，即通过制定的配置创建数据库连接池

### Druid数据库连接池实现技术

使用步骤

- 1.导入jar包 druid-1.0.9.jar
- 2.定义配置文件
  - 是properties形式的
  - 可以叫任意名称，可以放在任意目录下

- 3.加载配置文件。Properties
- 4.获取数据库连接池对象：通过工厂来来获取  DruidDataSourceFactory
- 5.获取连接：getConnection

### Spring JDBC

Spring框架对JDBC的简单封装。提供了一个JDBCTemplate对象简化JDBC的开发

#### 使用步骤

- 1.导入jar包
- 2.创建JdbcTemplate对象。依赖于数据源DataSource
  - JdbcTemplate template = new JdbcTemplate(ds)

- 3.调用JdbcTemplate的方法来完成CRUD的操作

#### JdbcTemplate对象中的方法

| 方法             | 说明                                                         |
| ---------------- | ------------------------------------------------------------ |
| update()         | 执行DML语句。增、删、改语句。SQL编写时候？代替参数，执行update()的时候传递sql和参数的值，可变参 |
| queryForMap()    | 查询结果将结果集封装为map集合，将列名作为key，将值作为value 将这条记录封装为一个map集合。查询结果只能是1，0获其它都会报错 |
| queryForList()   | 查询结果将结果集封装为list集合。将每一条记录封装为一个Map集合，再将Map集合装载到List集合中 |
| query()          | 查询结果，将结果封装为JavaBean对象                           |
| queryForObject() | 查询结果，将结果封装为对象。一般用于聚合函数的查询           |

query()方法的参数sql、RowMapper(这是一个接口，我们可以直接去实现，也可以使用它的实现类BeanPropertyRowMapper

自己实现RowMapper

```java
@Test
public void test6(){
    String sql = "select * from emp";
    List<Emp> list = template.query(sql, new RowMapper<Emp>() {

        @Override
        public Emp mapRow(ResultSet rs, int i) throws SQLException {
            Emp emp = new Emp();
            int id = rs.getInt("id");
            String ename = rs.getString("ename");
            int job_id = rs.getInt("job_id");
            int mgr = rs.getInt("mgr");
            Date joindate = rs.getDate("joindate");
            double salary = rs.getDouble("salary");
            double bonus = rs.getDouble("bonus");
            int dept_id = rs.getInt("dept_id");

            emp.setId(id);
            emp.setEname(ename);
            emp.setJob_id(job_id);
            emp.setMgr(mgr);
            emp.setJoindate(joindate);
            emp.setSalary(salary);
            emp.setBonus(bonus);
            emp.setDept_id(dept_id);

            return emp;
        }
    });
    for (Emp emp : list) {
        System.out.println(emp);
    }
}
```

使用实现类BeanPropertyRowMapper实现类  new BeanPropertyMapper<类>（类.class）

```java
/**
	查询所有记录，将其封装为Emp对象的List集合
*/

@Test
public void test6_2(){
    String sql = "select * from emp";
    List<Emp> list = template.query(sql, new BeanPropertyRowMapper<Emp>(Emp.class));
    for (Emp emp : list) {
        System.out.println(emp);
    }
}
```

query()方法

```java
/**
	 7. 查询总记录数
*/
			
@Test
public void test7(){
    String sql = "select count(id) from emp";
    Long total = template.queryForObject(sql, Long.class);
    System.out.println(total);
}
```

