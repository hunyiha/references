# SpringBoot框架

## 1.Spring Boot概述

### 1.1 什么是Spring Boot

Spring Boot是Spring项目中的一个子工程，与我们所熟知的Spring-framework 同属于spring的产品

一般把Spring Boot称为搭建程序的 **脚手架** 或者说是**便捷搭建基于Spring的工程的脚手架**。其最主要作用就是帮助开发人员快速的构建庞大的spring项目，并且尽可能的**减少一切xml配置**，做到开箱即用，迅速上手，让开发人员关注业务而非配置。



### 1.2 为什么要学习Spring Boot

java一直被人诟病的一点就是臃肿、麻烦。当我们还在辛苦的搭建项目时，可能Python程序员已经把功能写好了，究其原因注意是两点：

- 复杂的配置
  	项目各种配置其实是开发时的损耗， 因为在思考 Spring 特性配置和解决业务问题之间需要进行思维切换，所以写配置挤占了写应用程序逻辑的时间。

- 一个是混乱的依赖管理
  项目的依赖管理也是件吃力不讨好的事情。决定项目里要用哪些库就已经够让人头痛的了，你还要知道这些库的哪个版本和其他库不会有冲突，这难题实在太棘手。并且，依赖管理也是一种损耗，添加依赖不是写应用程序代码。一旦选错了依赖的版本，随之而来的不兼容问题毫无疑问会是生产力杀手。

而Spring Boot让这一切成为过去！我们可以使用Spring Boot创建java应用，并使用java –jar 启动它，就能得到一个生产级别的web工程。



### 1.3 Spring Boot的特点

Spring Boot 主要特点是：

- 创建独立的Spring应用，为所有 Spring 的开发者提供一个非常快速的、广泛接受的入门体验
- 直接嵌入应用服务器，如tomcat、jetty、undertow等；不需要去部署war包
- 提供固定的启动器依赖去简化组件配置；实现开箱即用（启动器starter-其实就是Spring Boot提供的一个jar包），通过自己设置参数（.properties或.yml的配置文件），即可快速使用。
- 自动地配置Spring和其它有需要的第三方依赖
- 提供了一些大型项目中常见的非功能性特性，如内嵌服务器、安全、指标，健康检测、外部化配置等
- 绝对没有代码生成，也无需 XML 配置



## 2. 快速入门

我们就来利用Spring Boot搭建一个web工程

### 2.1. 创建工程

新建一个maven jar工程

### 2.2. 添加依赖

前面说传统开发的问题之一就是依赖管理混乱，怎么这里我们还需要管理依赖呢？难道Spring Boot不帮我们管理吗？别着急，现在我们的项目与Spring Boot还没有什么关联。Spring Boot提供了一个名为spring-boot-starter-parent的工程，里面已经对各种常用依赖（并非全部）的版本进行了管理，我们的项目需要以这个项目为父工程，这样我们就不用操心依赖的版本问题了，需要什么依赖，直接引入坐标即可！

#### 2.2.1. 添加父工程坐标

```xml
<parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>2.2.1.RELEASE</version>
</parent>
```

#### 2.2.2. 添加web启动器

为了让Spring Boot帮我们完成各种自动配置，我们必须引入Spring Boot提供的自动配置依赖，我们称为 **启动器** 。因为我们是web项目，这里我们引入web启动器，在  pom.xml 文件中加入如下依赖：

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
</dependency>
```

需要注意的是，我们并没有在这里指定版本信息。因为Spring Boot的父工程已经对版本进行了管理了。这个时候，我们会发现项目中多出了大量的依赖。那些依赖都是Spring Boot根据 spring-boot-starter-web 这个依赖自动引入的，而且所有的版本都已经管理好，不会出现冲突。

#### 2.2.3. 管理jdk版本

如果我们想要修改Spring Boot项目的jdk版本，只需要简单的添加以下属性即可，如果没有需求，则不添加。同样的在  pom.xml 文件中添加如下：

```xml
<!--Spring Boot项目的jdk版本-->
<properties>
    <java.version>1.8</java.version>
</properties>
```

#### 2.2.4. 完整pom文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    
    <!--springboot提供的父工程，所有的springboot项目都以这个作为父工程-->
    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.2.1.RELEASE</version>
        <relativePath/> 
    </parent>
    
    <groupId>com.example</groupId>
    <artifactId>springboot</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <name>springboot</name>
    <description>Demo project for Spring Boot</description>

    <!--Spring Boot项目的jdk版本-->
    <properties>
        <java.version>1.8</java.version>
    </properties>

    <dependencies>
        
        <!--web启动器-->
        <!--启动器不加版本号的原因是父工程里面已经定义好了，我们只需要指定父工程的版本，springboot会给我们进行管理-->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        
    </dependencies>
</project>

```

### 2.3. 启动类

Spring Boot项目通过main函数即可启动，我们需要创建一个启动类：

```java
package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * spring boot工程都有一个启动引导类，这是工程的入口类
 * 并在引导类上添加@SpringBootApplication这个组合注解标注这是启动类
 * @SpringBootApplication
 *      1.这个组合注解里面有扫描注解，会扫描当前包及其子包下的spring注解
 *      2.也是一个配置注解,使用了之后就是一个配置类
 */
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}

```

### 2.4. 编写controller

接下来，就可以像以前那样开发SpringMVC的项目了！

```java
package com.itheima.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/*
     @RestController是一个组合注解
     @RestController=@Controller+@ResponseBody
     @ResponseBody表示方法的返回值以字符串的形式返回，页面不进行跳转
 */

@RestController
public class HelloControler {

    @GetMapping("hello")
    public String hello(){
        return "hello springboot";
    }
}

```

### 2.5. 启动测试

接下来，运行main函数，查看控制台：

并且可以看到监听的端口信息：



## 3.java配置应用

在入门案例中，我们没有任何的配置，就可以实现一个SpringMVC的项目了，快速、高效！
但是有同学会有疑问，如果没有任何的xml，那么我们如果要配置一个Bean该怎么办？比如我们要配置一个数据库连接池，以前会这么配置：

```properties
jdbc.driverClassName=com.mysql.jdbc.Driver
jdbc.url=jdbc:mysql://127.0.0.1:3306/springboot_test
jdbc.username=root
jdbc.password=root
```

```xml
<!--数据源  Druid数据库连接池 jdbc.properties抽取配置-->
<bean id="dataSource" class="com.alibaba.druid.pool.DruidDataSource" destroy-method="close">
    <property name="username" value="${jdbc.username}" />
    <property name="password" value="${jdbc.password}" />
    <property name="driverClassName" value="${jdbc.driver}" />
    <property name="url" value="${jdbc.url}" />
</bean>
```

### 3.1. Spring配置历史

事实上，在Spring3.0开始，Spring官方就已经开始推荐使用java配置来代替传统的xml配置了，我们不妨来回顾一下Spring的历史：

- Spring1.0时代

  在此时因为jdk1.5刚刚出来，注解开发并未盛行，因此一切Spring配置都是xml格式，想象一下所有的bean都用xml配置，细思极恐啊，心疼那个时候的程序员2秒

- Spring2.0时代

  Spring引入了注解开发，但是因为并不完善，因此并未完全替代xml，此时的程序员往往是把xml与注解进行结合，貌似我们之前都是这种方式。

- Spring3.0及以后

  3.0以后Spring的注解已经非常完善了，因此Spring推荐大家使用完全的java配置来代替以前的xml，不过似乎在国内并未推广盛行。然后当Spring Boot来临，人们才慢慢认识到java配置的优雅。

### 3.2. 尝试java配置

java配置主要靠java类和一些注解，比较常用的注解有：

- @Configuration ：声明一个类作为配置类，代替xml文件

- @Bean ：声明在方法上，将方法的返回值加入Bean容器，代替 <bean> 标签

- @Value ：属性注入

- @PropertySource ：指定外部属性文件

我们接下来用java配置来尝试实现连接池配置：



1. 在 pom.xml 文件中添加Druid连接池依赖如下：

   ```xml 
   <dependency>
       <groupId>com.alibaba</groupId>
       <artifactId>druid</artifactId>
       <version>1.1.6</version>
   </dependency>
   ```

   2.使用数据操作工具创建数据库  springboot_test

   

   3.然后在项目中创建 heima-springboot\src\main\resources\jdbc.properties 文件，内容如下

   ```properties
   jdbc.driverClassName=com.mysql.jdbc.Driver
   jdbc.url=jdbc:mysql://127.0.0.1:3306/springboot_test
   jdbc.username=root
   jdbc.password=root
   ```

   4.编写 JdbcConfig.java 如下

   ```java
   
   @Configuration  //声明我们 JdbcConfig 是一个配置类
   @PropertySource("classpath:jdbc.properties")  //指定属性文件的路径是: classpath:jdbc.properties
   public class JdbcConfig {
   
       @Value("${jdbc.driverClassName}")  //为属性注入值
       private String driverClassName;
   
       @Value("${jdbc.url}")
       private String url;
   
       @Value("${jdbc.username}")
       private String username;
   
       @Value("${jdbc.password}")
       private String password;
   
       @Bean //将 dataSource() 方法声明为一个注册Bean的方法，Spring会自动调用该方法，将方法的返回值加入Spring容器中。
       public DataSource getDataSource(){
           DruidDataSource dataSource = new DruidDataSource();
   
           dataSource.setDriverClassName(driverClassName);
           dataSource.setUrl(url);
           dataSource.setUsername(username);
           dataSource.setPassword(password);
   
           return dataSource;
       }
   }
   
   ```

   - @Configuration ：声明我们 JdbcConfig 是一个配置类

   - @PropertySource ：指定属性文件的路径是: classpath:jdbc.properties

   - @Value: 为属性注入值

   - @Bean: 将 dataSource() 方法声明为一个注册Bean的方法，Spring会自动调用该方法，将方法的返回值加入Spring容器中。

   ​        然后我们就可以在任意位置通过 @Autowired 注入DataSource了！

   

   5.在 HelloController 中注入DataSource进行测试，改造代码如下：

   ```java
   @RestController
   public class HelloController {
   
       @Autowired
       private DataSource dataSource;
   
       @GetMapping("hello")
       public String hello(){
           System.out.println(dataSource);
           return "hello springboot";
       }
   }
   ```

   

### 3.3. Spring Boot的属性注入

属性文件的名称有变化，默认的文件名必须是：application.properties或application.yml

在上面的案例中，我们实验了java配置方式。不过属性注入使用的是@Value注解。这种方式虽然可行，但是不够强大，因为它只能注入基本类型值。

在Spring Boot中，提供了一种新的属性注入方式，支持各种java基本数据类型及复杂类型的注入。



1)新建一个类JdbcProperties.java 用于进行属性注入：

```java

/*
    在类上通过@ConfigurationProperties注解声明当前类为属性读取类
    prefix = "jdbc" 读取属性文件中，前缀为jdbc的值
 */
@ConfigurationProperties(prefix = "jdbc")  
public class JdbcProperties {
    private String driverClassName;
    private String url;
    private String username;
    private String password;

    public String getDriverClassName() {  
        return driverClassName;
    }

    public void setDriverClassName(String driverClassName) {
        this.driverClassName = driverClassName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
```

- 在类上通过@ConfigurationProperties注解声明当前类为属性读取类

- prefix="jdbc" 读取属性文件中，前缀为jdbc的值。

- 在类上定义各个属性，名称必须与属性文件中 jdbc. 后面部分一致

- 需要注意的是，这里我们并没有指定属性文件的地址，所以我们需要把jdbc.properties名称改为application.properties，这是Spring Boot默认读取的属性文件名：

application.properties文件的内容：

```properties
jdbc.driverClassName=com.mysql.jdbc.Driver
jdbc.url=jdbc:mysql://127.0.0.1:3306/springboot_test
jdbc.username=root
jdbc.password=root
```

【注意】如果出现如下提示，项目也可以运行；

![](E:\01_notes\06_springboot\img\异常.png)

如果要去掉上述的提示，则可以在 pom.xml 文件中添加如下依赖：

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-configuration-processor</artifactId>
    <!--不传递依赖-->
    <optional>true</optional>
</dependency>
```



2)将 JdbcConfig 类原来全部注释掉或删除，修改为如下内容：

```java
@Configuration
@EnableConfigurationProperties(JdbcProperties.class)  //来声明要使用 JdbcProperties 这个类的对象
public class JdbcConfig {

    @Bean
    public DataSource dataSource(JdbcProperties jdbcProperties){
        DruidDataSource druidDataSource = new DruidDataSource();

        druidDataSource.setUrl(jdbcProperties.getUrl());
        druidDataSource.setDriverClassName(jdbcProperties.getDriverClassName());
        druidDataSource.setUsername(jdbcProperties.getUsername());
        druidDataSource.setPassword(jdbcProperties.getPassword());

        return druidDataSource;
    }
}

```

- 通过 @EnableConfigurationProperties(JdbcProperties.class) 来声明要使用 JdbcProperties 这个类的对象

- 然后要使用配置的话；可以通过以下方式注入JdbcProperties：

  - @Autowired注入

    ```java
    @Autowired
    private  JdbcProperties jdbcProperties;
    ```

  - 构造函数注入

    ```java
    private  JdbcProperties jdbcProperties;
    
    public JdbcConfig(JdbcProperties jdbcProperties) {
        this.jdbcProperties = jdbcProperties;
    }
    ```

  - 声明有@Bean的方法参数注入

    ```java
    @Bean
    public DataSource dataSource(JdbcProperties jdbcProperties){
        DruidDataSource druidDataSource = new DruidDataSource();
    
        druidDataSource.setUrl(jdbcProperties.getUrl());
        druidDataSource.setDriverClassName(jdbcProperties.getDriverClassName());
        druidDataSource.setUsername(jdbcProperties.getUsername());
        druidDataSource.setPassword(jdbcProperties.getPassword());
    
        return druidDataSource;
    }
    ```

    上面的例子使用了最后一种。

@ConfigurationProperties和@Value的对比

```
Relaxed binding：松散绑定
	不严格要求属性文件中的属性名与成员变量名一致。支持驼峰，中划线，下划线等等转换，甚至支持对象引导。比如：user.friend.name：代表的是user对象中的friend属性中的name属	性，显然friend也是对象。@value注解就难以完成这样的注入方式。
meta-data support：元数据支持，帮助IDE生成属性提示（写开源框架会用到）。
```

### 3.4. 更优雅的注入

事实上，如果一段属性只有一个Bean需要使用，我们无需将其注入到一个类（JdbcProperties，将该类上的所有注解去掉）中。而是直接在需要的地方声明即可；再次修改 JdbcConfig 类为如下代码：

```java
@Configuration
public class JdbcConfig {

    @Bean
    @ConfigurationProperties(prefix = "jdbc")  // 声明要注入的属性前缀，Spring Boot会自动把相关属性通过set方法注入到DataSource中
    public DataSource dataSource(){
        return  new DruidDataSource();
    }
}

```

我们直接把 @ConfigurationProperties(prefix = "jdbc") 声明在需要使用的 @Bean 的方法上，然后Spring Boot就会自动调用这个Bean（此处是DataSource）的set方法，然后完成注入。使用的前提是：**该类必须有对应属性的set方法**！



### 3.5. Yaml配置文件

配置文件除了可以使用application.properties类型，还可以使用后缀名为：.yml或者.yaml的类型，也就是：application.yml或者application.yaml

Yaml文件的基本格式：

```yaml
jdbc:
  driverClassName: com.mysql.jdbc.Driver
  url: jdbc:mysql://127.0.0.1:3306/springboot_test
  username: root
  password: root
```

yaml和properties如果两个配置文件都有，会把两个文件的配置合并，如果有重复属性，以properties中的为准。



### 3.6. 多个Yaml配置文件

当一个项目中有多个yml配置文件的时候，可以以application-**.yml命名；在application.yml中配置项目使用激活这些配置文件即可。

```yaml
#激活配置文件;需要指定其它的配置文件名称,只需要写application-后面的名字就好
spring:
  profiles:
    active: a,b
```



## 4. 自动配置原理

使用Spring Boot之后，一个整合了SpringMVC的WEB工程开发，变的无比简单，那些繁杂的配置都消失不见了，这是如何做到的？

一切魔力的开始，都是从我们的main函数来的，所以我们再次来看下启动类：

```java
@SpringBootApplication
public class SpringbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootApplication.class, args);
    }

}
```

我们发现特别的地方有两个：

- 注解：@SpringBootApplication

- run方法：SpringApplication.run()



### 4.1. 了解@SpringBootApplication

- @SpringBootConfiguration
- @EnableAutoConfiguration
- @ComponentScan

#### 4.1.1 @SpringBootConfiguration

通过这段我们可以看出，在这个注解上面，又有一个 @Configuration 注解。通过上面的注释阅读我们知道：这个注解的作用就是声明当前类是一个配置类，然后Spring会自动扫描到添加了 @Configuration 的类，并且读取其中的配置信息。而 @SpringBootConfiguration 是来声明当前类是SpringBoot应用的配置类，项目中只能有一个。所以一般我们无需自己添加。



#### 4.1.2 @EnableAutoConfiguration

总结，Spring Boot内部对大量的第三方库或Spring内部库进行了默认配置，这些配置是否生效，取决于我们是否引入了对应库所需的依赖，如果有那么默认配置就会生效。所以，我们使用SpringBoot构建一个项目，只需要引入所需框架的依赖，配置就可以交给SpringBoot处理了。除非你不希望使用SpringBoot的默认配置，它也提供了自定义配置的入口。



#### 4.1.3  @ComponentScan

配置组件扫描的指令。提供了类似与 <context:component-scan> 标签的作用通过basePackageClasses或者basePackages属性来指定要扫描的包。如果没有指定这些属性，那么将从声明这个注解的类所在的包开始，扫描包及子包

而我们的@SpringBootApplication注解声明的类就是main函数所在的启动类，因此扫描的包是该类所在包及其子包。因此，一般启动类会放在一个比较前的包目录中。



### 4.2 默认配置原理

#### 4.2.1. spring.factories



### 4.3. 总结

SpringBoot为我们提供了默认配置，而默认配置生效的步骤：

- @EnableAutoConfiguration注解会去寻找 META-INF/spring.factories 文件，读取其中以EnableAutoConfiguration 为key的所有类的名称，这些类就是提前写好的自动配置类
- 这些类都声明了 @Configuration 注解，并且通过 @Bean 注解提前配置了我们所需要的一切实例
- 但是，这些配置不一定生效，因为有 @ConditionalOn 注解，满足一定条件才会生效。比如条件之一： 是一些
  相关的类要存在
- 类要存在，我们只需要引入了相关依赖（启动器），依赖有了条件成立，自动配置生效。
- 如果我们自己配置了相关Bean，那么会覆盖默认的自动配置的Bean
- 我们还可以通过配置application.yml文件，来覆盖自动配置中的属性

**1）启动器**

所以，我们如果不想配置，只需要引入依赖即可，而依赖版本我们也不用操心，因为只要引入了SpringBoot提供的stater（启动器），就会自动管理依赖及版本了。

因此，玩SpringBoot的第一件事情，就是找启动器，SpringBoot提供了大量的默认启动器

**2）全局配置**

另外，SpringBoot的默认配置，都会读取默认属性，而这些属性可以通过自定义 application.properties 文件来进行覆盖。这样虽然使用的还是默认配置，但是配置中的值改成了我们自定义的。

因此，玩SpringBoot的第二件事情，就是通过 application.properties 来覆盖默认属性值，形成自定义配置。我们需要知道SpringBoot的默认属性key，非常多，可以再idea中自动提示

属性文件支持两种格式，application.properties和application.yml

yml的语法实例：

```yaml
jdbc:
  driverClassName: com.mysql.jdbc.Driver
  url: jdbc:mysql://127.0.0.1:3306/springboot_test
  username: root
  password: root
```

如果properties和yml文件都存在，如果有重叠属性，默认以Properties优先。遇到需要修改的组件的配置项流程为：

<img src="E:\01_notes\06_springboot\img\yam和properties.png" style="zoom:67%;" />



## 5 Spring Boot实践

### 5.1. Lombok

我们编写pojo时，经常需要编写构造函数和getter、setter方法，属性多的时候，就非常浪费时间，使用lombok插件可以解决这个问题：

在IDEA中安装lombok插件；不安装插件在IDEA中使用lombok的注解虽然编译能通过，但是源码会报错。所以为了让IDEA更好的辨别lombok注解则才安装插件。

需要在maven工程中的 pom.xml 文件引入依赖：

```
<dependency>
  <groupId>org.projectlombok</groupId>
  <artifactId>lombok</artifactId>
</dependency>
```

然后可以在Bean上使用：

- @Data ：自动提供getter和setter、hashCode、equals、toString等方法

- @Getter：自动提供getter方法

- @Setter：自动提供setter方法

- @Slf4j：自动在bean中提供log变量，其实用的是slf4j的日志功能。

例如；在javabean上加@Data，那么就可以省去getter和setter等方法的编写，lombok插件会自动生成。



### 5.2. 整合SpringMVC

虽然默认配置已经可以使用SpringMVC了，不过我们有时候需要进行自定义配置。

可以在 application.yml 文件中配置日志级别控制：

```yaml
#日志记录级别
logging:
  level:
    com.itheima: debug
    org.springframework: info
```

#### 5.2.1. 修改端口

查看SpringBoot的全局属性可知，端口通过以下方式配置：

修改  application.yml 配置文件，添加如下配置：

```yaml
#tomcat端口
server:
  port: 80
```

#### 5.2.2. 访问静态资源

现在，我们的项目是一个jar工程，那么就没有webapp，我们的静态资源该放哪里呢？

回顾我们在上面看的源码，有一个叫做ResourceProperties的类，里面就定义了静态资源的默认查找路径：

默认的静态资源路径为：

- classpath:/META-INF/resources/
- classpath:/resources/
- classpath:/static/
- classpath:/public

只要静态资源放在这些目录中任何一个，SpringMVC都会帮我们处理。

我们习惯会把静态资源放在 classpath:/static/ 目录下。我们创建目录  static ，并且从 资料文件夹中复制itcast.gif 和 test.js 如下：



### 5.2.3. 添加拦截器

拦截器也是我们经常需要使用的，在SpringBoot中该如何配置呢？

拦截器不是一个普通属性，而是一个类，所以就要用到java配置方式了。在SpringBoot官方文档中有这么一段说明：

```
如果你想要保持Spring Boot 的一些默认MVC特征，同时又想自定义一些MVC配置（包括：拦截器，格式化器,视图控制器、消息转换器 等等），你应该让一个类实现 WebMvcConfigurer ，并且添加 @Configuration 注解，但是千万不要加 @EnableWebMvc 注解。如果你想要自定义 HandlerMapping 、 HandlerAdapter 、
ExceptionResolver 等组件，你可以创建一个 WebMvcRegistrationsAdapter 实例 来提供以上组件。

如果你想要完全自定义SpringMVC，不保留SpringBoot提供的一切特征，你可以自己定义类并且添加@Configuration 注解和 @EnableWebMvc 注解
```

总结：通过实现 WebMvcConfigurer 并添加 @Configuration 注解来实现自定义部分SpringMvc配置。

1. 编写自定义拦截器：

```java
package com.itheima.springboot.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/*
    编写拦截器
 */

@Slf4j
public class MyInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.debug("这是拦截器的preHandle方法");
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
                           ModelAndView modelAndView) throws Exception {
        log.debug("这是拦截器的postHandle方法");

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        log.debug("这是拦截器的afterCompletion方法");

    }
}

```

2.定义配置类 MvcConfig.java ，用于注册拦截器，内容如下

```java
@Configuration
public class MvcConfig implements WebMvcConfigurer {
    
    //注册拦截器到系统，让spring去管理
    @Bean
    public MyInterceptor myInterceptor(){
        return new MyInterceptor();
    }

    //重写方法，将拦截器添加到拦截器链
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //通过registry添加myInterceptor拦截器，并设置拦截器路径为 /*
        registry.addInterceptor(myInterceptor()).addPathPatterns("/*");
    }
}
```



### 5.3. 整合jdbc和事务

spring中的jdbc连接和事务是配置中的重要一环，在SpringBoot中该如何处理呢？

答案是不需要处理，我们只要找到SpringBoot提供的启动器即可，在 pom.xml 文件中添加如下依赖：

```xml
 <!--jdbc启动器-->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-jdbc</artifactId>
</dependency>
```

当然，不要忘了数据库驱动，SpringBoot并不知道我们用的什么数据库，这里我们选择MySQL；同样的在 pom.xml文件中添加如下依赖：

```xml
<!--springboot并不知道我们使用的是什么数据库-->
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <version>5.1.46</version>
</dependency>
```

至于事务，SpringBoot中通过注解来控制。就是我们熟知的 @Transactional 使用的时候设置在对应的类或方法上即可。



### 5.4. 整合连接池

其实，在刚才引入jdbc启动器的时候，SpringBoot已经自动帮我们引入了一个连接池：

![](E:\01_notes\06_springboot\img\springboot的默认连接池.png)

HikariCP应该是目前速度最快的连接池了，我们看看它与c3p0的对比：

<img src="E:\01_notes\06_springboot\img\连接池对比.png" style="zoom: 50%;" />

因此，我们只需要指定连接池参数即可；打开 application.yml 添加修改配置如下：

```yaml
#配置hikari,springboor默认使用的数据库连接池参数
spring:
  datasource:
    driver-class-name: com.mysql.jdbc.Driver
    url: jdbc:mysql://127.0.0.1:3306/springboot_test
    username: root
    password: root
```



### 5.5. 整合mybatis

#### 5.5.1. mybatis

1.SpringBoot官方并没有提供Mybatis的启动器，不过Mybatis官网自己实现了。在项目的 pom.xml 文件中加入如下依赖：

```xml
<!--mybatis启动器-->
<dependency>
    <groupId>org.mybatis.spring.boot</groupId>
    <artifactId>mybatis-spring-boot-starter</artifactId>
    <version>2.0.1</version>
</dependency>
```

2.配置 application.yml ，常用配置如下：

```yaml
mybatis:
  # 实体类别名包路径
  type-aliases-package: com.itheima.pojo
  # 映射文件路径
  # mapper-locations: classpath:mappers/*.xml
  configuration:
    log-impl: org.apache.ibatis.logging.stdout.StdOutImpl
```

3.配置Mapper扫描

需要注意，这里没有配置mapper接口扫描包，因此我们需要给每一个Mapper接口添加 @Mapper 注解，才能被识别。

```java
@Mapper
public interface UserMapper {
}
```

或者，我们也可以不加注解，而是在启动类上添加扫描包注解(推荐)：

```java
@SpringBootApplication
@MapperScan("com.itheima.springboot.mapper")  //@MapperScan扫描mybatis所有的业务mapper接口 mapper官方的
public class SpringbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootApplication.class, args);
    }

}
```

以下代码示例中，我们将采用@MapperScan扫描方式进行。



#### 5.5.2. 通用mapper

1.通用Mapper的作者也为自己的插件编写了启动器，我们直接引入即可。在项目的 pom.xml 文件中加入如下依赖：

```xml
<!--通用mapper-->
<dependency>
    <groupId>tk.mybatis</groupId>
    <artifactId>mapper-spring-boot-starter</artifactId>
    <version>2.1.5</version>
</dependency>
```

注意：一旦引入了通用Mapper的启动器，会覆盖Mybatis官方启动器的功能，因此**需要移除对官方Mybatis启动器的依赖**。



2.编写UserMapper

无需任何配置就可以使用了。如果有特殊需要，可以到通用mapper官网查看：https://github.com/abel533/Mapper/wiki/3.config

编写UserMapper.java 如下：

```java
public interface UserMapper extends Mapper<User>{

}
```

3.把启动类上的@MapperScan注解修改为**通用mapper中自带的**：

```java
package com.itheima.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@MapperScan扫描mybatis所有的业务mapper接口 mapper官方的
//@MapperScan("com.itheima.springboot.mapper")
//通用mapper的注解扫描
@tk.mybatis.spring.annotation.MapperScan("com.itheima.springboot.mapper")
public class SpringbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootApplication.class, args);
    }

}

```



4.在User实体类上加JPA注解

修改实体类User.java 如下：

```java
package com.itheima.springboot.pojo;

import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Table(name="tb_user")  //使用通用mapper时候，数据库中查询到的数据封装到哪一个实体类中
public class User {

    @Id //主键
    @KeySql(useGeneratedKeys = true) //开启主键自动回填
    private Long id;

    //user_name --> userName
    //如果不满足驼峰命名，可以使用@Column("abc)完成实体类和数据库字段的映射
    private String userName;

    private String password;

    private String name;

    private Integer age;

    private Integer sex;

    private Date birthday;

    private String note;

    private Date created;

    private Date updated;
}

```

5.对 UserService 的代码进行简单改造

```java
package com.itheima.springboot.service;

import com.itheima.springboot.mapper.UserMapper;
import com.itheima.springboot.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserService {

    @Autowired
    private UserMapper userMapper;

    //添加用户
    @Transactional
    public void add(User user){
        userMapper.insertSelective(user);
    }

    //查询用户
    public User queryById(int id){
        User user = userMapper.selectByPrimaryKey(id);
        return user;
    }
}

```

5.6. 启动测试

将 HelloController 进行简单改造：

```java
package com.itheima.springboot.controller;

import com.itheima.springboot.pojo.User;
import com.itheima.springboot.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.sql.DataSource;

@RestController
public class HelloController {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private UserService userService;

    @GetMapping("hello")
    public String hello(){
        System.out.println(dataSource);
        return "hello springboot";
    }

    @GetMapping("/user/{id}")
    public User queryById(@PathVariable int id){
        return userService.queryById(id);
    }
}

```



### 5.7. Junit测试

1.在springboot项目中如果要使用Junit进行单元测试，则需要添加如下的依赖：

```xml
<!--单元测试-->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-test</artifactId>
</dependency>
```

2.在测试包下编写测试类

在测试类上面必须要添加  @SpringBootTest 注解。

```java
package com.itheima.springboot.service;

import com.itheima.springboot.pojo.User;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest //springboot项目测试都需要加SpringBootTest注解
class UserServiceTest {

    @Autowired
    private UserService userService;

    @Test
    void add() {
        User user = new User();
        user.setId(0L);
        user.setUserName("root1");
        user.setPassword("root");
        user.setName("ceshi");
        user.setAge(33);
        user.setSex(1);
        user.setBirthday(new Date());
        user.setNote("ceshi");
        user.setCreated(new Date());
        user.setUpdated(new Date());
        userService.add(user);
        System.out.println("添加User：" + user);
    }

    @Test
    void queryById() {
        User user = userService.queryById(9);
        System.out.println(user);
    }
}
```

### 5.8. 整合Redis

在 pom.xml 文件中添加如下依赖:

```xml
<!--redis的启动器-->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-redis</artifactId>
</dependency>
```

配置 application.yml 文件:

```yaml
spring:
  #配置redis的参数
  redis:
    host: localhost
    port: 6379
```

api使用如下：

```java
@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisTest {

    @Autowired
    private RedisTemplate redisTemplate;
    
    @Test
    public void test(){
        
        //string 字符串
        //redisTemplate.opsForValue().set("str", "heima");
        redisTemplate.boundValueOps("str").set("heima");
        System.out.println("str = " + redisTemplate.opsForValue().get("str"));

        //hash 散列
        redisTemplate.boundHashOps("h_key").put("name", "heima");
        redisTemplate.boundHashOps("h_key").put("age", 13);
        
        //获取所有域
        Set set = redisTemplate.boundHashOps("h_key").keys();
        System.out.println(" hash散列的所有域：" + set);
        
        //获取所有值
        List list = redisTemplate.boundHashOps("h_key").values();
        System.out.println(" hash散列的所有域的值：" + list);

        //list 列表
        redisTemplate.boundListOps("l_key").leftPush("c");
        redisTemplate.boundListOps("l_key").leftPush("b");
        redisTemplate.boundListOps("l_key").leftPush("a");
        
        //获取全部元素
        list = redisTemplate.boundListOps("l_key").range(0, -1);
        System.out.println(" list列表中的所有元素：" + list);

        // set 集合
        redisTemplate.boundSetOps("s_key").add("a", "b", "c");
        set = redisTemplate.boundSetOps("s_key").members();
        System.out.println(" set集合中的所有元素：" + set);

        // sorted set 有序集合
        redisTemplate.boundZSetOps("z_key").add("a", 30);
        redisTemplate.boundZSetOps("z_key").add("b", 20);
        redisTemplate.boundZSetOps("z_key").add("c", 10);
        set = redisTemplate.boundZSetOps("z_key").range(0, -1);
        System.out.println(" zset有序集合中的所有元素：" + set);
    }
}

```



## 6.Spring Boot项目部署

### 6.1. 项目打包

1.添加项目的pom.xml插件；在pom.xml要显式的加入插件spring-boot-maven-plugin，否则无法产生 jar 清单文件，导致打出来的 jar 无法使用命令运行；

```xml
<build>
    <plugins>
        <plugin>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-maven-plugin</artifactId>
        </plugin>
    </plugins>
</build>
```

2.使用maven的命令package打包；

<img src="E:\01_notes\06_springboot\img\打包.png" style="zoom:50%;" />

之后在项目下的 target 目录中将有如下jar包：

<img src="E:\01_notes\06_springboot\img\jar包.png" style="zoom:50%;" />

<img src="E:\01_notes\06_springboot\img\解压.png" style="zoom:50%;" />

【注意】在查看打出的 jar 的时候，将发现 jar 包里面包含 jar 包；这样的包称为 fatJar（肥包）

### 6.2. 运行

运行打出来的包；使用命令： java –jar 包全名 或者写一个 bat 文件，里面包含 java –jar 包全名；这样就可以双击启动应用。

如执行上述打出来的jar的命令为：

```
java -jar heima-springboot-1.0-SNAPSHOT.jar 
```

测试则可使用浏览器访问：http://localhost/user/8



## 7.插件安装

### 7.1. 安装插件

打开IDEA的设置界面（按  ctrl+alt+S ）之后在插件选项中搜索SpringBoot，安装  JBLSpringBootAppGen

### 7.2. 应用插件

在IDEA中任意一个maven项目或src目录上 右击，选择  JBLSpringBootAppGen 即可。

在如下的界面中输入 启动引导类的名称并根据需要勾选是否要生成application.yml配置文件。

